package modelTest;

import static org.junit.Assert.*;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import model.BoatCard;

import org.junit.Test;

public class TestBoatCard {

	@Test
	public void testBoatCard() {
		BoatCard card= new BoatCard("Test", Enumerations.Color.RED);
		assertTrue(card.getId()=="Test"&& card.getColor()==Enumerations.Color.RED);
	}

}
