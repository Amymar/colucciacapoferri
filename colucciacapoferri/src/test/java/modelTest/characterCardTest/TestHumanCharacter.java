package modelTest.characterCardTest;

import static org.junit.Assert.*;
import it.polimi.ingsw.colucciacapoferri.model.boxes.Box;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoxSingleton;
import it.polimi.ingsw.colucciacapoferri.model.character_card.HumanCharacter;
import it.polimi.ingsw.colucciacapoferri.model.character_card.InterfaceCharacterCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestHumanCharacter {

	@Test
	public void testHumanCharacter() {
		BoxSingleton box= new Box();
		InterfaceCharacterCard character =new HumanCharacter("Test", box);
		assertTrue(character!=null && character.getName().equals("Test")&& character.getPosition()==box && character.getObjects().isEmpty() && character.getOldRounds().isEmpty());
	}

	@Test
	public void testIsEaten() {
		BoxSingleton box= new Box();
		InterfaceCharacterCard character =new HumanCharacter("Test", box);
		assertTrue(!character.isEaten());
	}
		
	@Test
	public void testSetEaten() {
		BoxSingleton box= new Box();
		InterfaceCharacterCard character =new HumanCharacter("Test", box);
		character.setEaten(true);
		assertFalse(character.isEaten());
	}

	@Test
	public void testGetThis() {
		Map<String, InterfaceCharacterCard> characters=new TreeMap<String, InterfaceCharacterCard> ();
		assertTrue(characters.isEmpty());
		BoxSingleton box= new Box();
		InterfaceCharacterCard card=new HumanCharacter();
		card.getThis("Test",box, characters);
		assertTrue(characters.containsKey("Test"));
		card.getThis("Test",box, characters);
		assertTrue(characters.size()==1);
	}

}
