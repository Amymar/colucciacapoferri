package modelTest;

import static org.junit.Assert.*;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;
import it.polimi.ingsw.colucciacapoferri.model.GameBoard;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoxSingleton;

import org.junit.Test;

import model.*;

public class TestBoard {
	
	@Test
	public void boardTest(){
		GameBoard map= new GameBoard();
		nextBoxTest(map);
//		safetyBoxTest(map);

	
	}

	private void nextBoxTest(GameBoard map){
		for (BoxSingleton box: map.getSectors().values()){
			for(BoxSingleton next: box.getNearbyBoxes().values()){
				assertTrue(next.getNearbyBoxes().containsKey(box.getId()));
			}
		}
	}

//	private void safetyBoxTest(GameBoard map){
//		String value;
//		for (BoxSingleton box: map.getSectors().values()){
//			value= box.getType().toString();
//			//TODO capire problema con JRE 1.7
//			switch (value){
//				case "SAFE" : assertTrue(idInSafe(box.getId()));
//					break;
//				case "BOAT": assertTrue( idInBoat(box.getId()));
//					break;
//				case "HUMANS": assertTrue(box.getId()=="XU");
//					break;
//				case "ALIENS": assertTrue(box.getId()=="XA");
//					break;
//				default : assertTrue(Enumerations.Ground.DANGER==box.getType());
//					break;
//			}
//		}
//	}

	private boolean idInSafe(String id){
		for(SafeBoxCheck name: Enumerations.SafeBoxCheck.values()){
			if(name.toString()==id){
				return true;
			}
		}
		return false;
	}
	private boolean idInBoat(String id){
		for(BoatBoxCheck name: Enumerations.BoatBoxCheck.values()){
			if(name.toString()==id){
				return true;
			}
		}
		return false;
	}

}
