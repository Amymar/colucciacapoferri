package modelTest;

import static org.junit.Assert.*;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoatBox;
import it.polimi.ingsw.colucciacapoferri.model.boxes.Box;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoxSingleton;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestBox {

	@Test
	public void testBox() {
		Box box=new Box();
		assertTrue(box!=null);
	}

	@Test
	public void testBoxStringGround() {
		Box box=new Box ("Test", Enumerations.Ground.DANGER);
		assertTrue(box.getId()=="Test"&& box.getType()==Enumerations.Ground.DANGER);
	}

	@Test
	public void testGetThis() {
		Map<String, BoxSingleton> boxes=new TreeMap<String, BoxSingleton> ();
		assertTrue(boxes.isEmpty());
		BoxSingleton box=new Box();
		box.getThis("Test",Enumerations.Ground.ALIENS, boxes);
		assertTrue(boxes.containsKey("Test"));
		box.getThis("Test",Enumerations.Ground.ALIENS, boxes);
		assertTrue(boxes.size()==1);;
	}

}
