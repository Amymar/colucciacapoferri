package modelTest;

import static org.junit.Assert.*;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import model.ObjectCard;

import org.junit.Test;

public class TestObjectCard {

	@Test
	public void testObjectCard() {
		ObjectCard card = new ObjectCard(1,Enumerations.ObjectCardType.ADRENALINE);
		assertTrue(card!=null);
		assertTrue(card.getId()==1 && card.getType()==Enumerations.ObjectCardType.ADRENALINE);
	}

}
