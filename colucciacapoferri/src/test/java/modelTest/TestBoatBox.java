package modelTest;

import static org.junit.Assert.*;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoatBox;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoxSingleton;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestBoatBox {

	@Test
	public void testBoatBox() {
		BoatBox box=new BoatBox ("Test", Enumerations.Ground.BOAT, true);
		assertTrue(box.getId()=="Test"&& box.getType()==Enumerations.Ground.BOAT&& box.isOpen()==true);
	}

	@Test
	public void testGetThis() {
		Map<String, BoxSingleton> boxes=new TreeMap<String, BoxSingleton> ();
		assertTrue(boxes.isEmpty());
		BoxSingleton box=new BoatBox();
		box.getThis("Test",Enumerations.Ground.BOAT, boxes);
		assertTrue(boxes.containsKey("Test"));
		box.getThis("Test",Enumerations.Ground.BOAT, boxes);
		assertTrue(boxes.size()==1);
		
	}

}
