package modelTest.sectorCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.FakeFraughtSectorCard;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.InterfaceCardSector;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestFakeFraughtSectorCard {

	
		@Test
		public void testFakeFraughtSectorCard() {
			InterfaceCardSector card = new FakeFraughtSectorCard();
			assertTrue(card.getId()==null&&card.getType()==Enumerations.SectorCardType.FAKE&& card.isObject()==true);
		}

		@Test
		public void testFakeFraughtSectorCardString() {
			String id="Test";
			InterfaceCardSector card= new FakeFraughtSectorCard(id);
			assertTrue(card.getId()==id&&card.getType()==Enumerations.SectorCardType.FAKE&& card.isObject()==true);
		}

		@Test
		public void testGetThis() {
			Map<String, InterfaceCardSector> cards=new TreeMap<String, InterfaceCardSector> ();
			assertTrue(cards.isEmpty());
			InterfaceCardSector card=new FakeFraughtSectorCard();
			card.getThis("Test", cards);
			assertTrue(cards.containsKey("Test"));
			card.getThis("Test", cards);
			assertTrue(cards.size()==1);
		}

}
