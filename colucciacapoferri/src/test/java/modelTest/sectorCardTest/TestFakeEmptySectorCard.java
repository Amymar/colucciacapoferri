package modelTest.sectorCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.FakeEmptySectorCard;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.InterfaceCardSector;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestFakeEmptySectorCard {

	@Test
	public void testFakeEmptySectorCard() {
		InterfaceCardSector card = new FakeEmptySectorCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.SectorCardType.FAKE&& card.isObject()==false);
	}

	@Test
	public void testFakeEmptySectorCardString() {
		String id="Test";
		InterfaceCardSector card= new FakeEmptySectorCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.SectorCardType.FAKE&& card.isObject()==false);
	}

	@Test
	public void testGetThis() {
		Map<String, InterfaceCardSector> cards=new TreeMap<String, InterfaceCardSector> ();
		assertTrue(cards.isEmpty());
		InterfaceCardSector card=new FakeEmptySectorCard();
		card.getThis("Test", cards);
		assertTrue(cards.containsKey("Test"));
		card.getThis("Test", cards);
		assertTrue(cards.size()==1);
	}


}
