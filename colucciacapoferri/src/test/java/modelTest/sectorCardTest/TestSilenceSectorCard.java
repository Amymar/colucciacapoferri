package modelTest.sectorCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.InterfaceCardSector;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.SilenceSectorCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestSilenceSectorCard {

	@Test
	public void testSilenceSectorCard() {
		InterfaceCardSector card = new SilenceSectorCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.SectorCardType.SILENCE&& card.isObject()==false);
	}

	@Test
	public void testSilenceSectorCardString() {
		String id="Test";
		InterfaceCardSector card= new SilenceSectorCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.SectorCardType.SILENCE&& card.isObject()==false);
	}

	@Test
	public void testGetThis() {
		Map<String, InterfaceCardSector> cards=new TreeMap<String, InterfaceCardSector> ();
		assertTrue(cards.isEmpty());
		InterfaceCardSector card=new SilenceSectorCard();
		card.getThis("Test", cards);
		assertTrue(cards.containsKey("Test"));
		card.getThis("Test", cards);
		assertTrue(cards.size()==1);
	}

}
