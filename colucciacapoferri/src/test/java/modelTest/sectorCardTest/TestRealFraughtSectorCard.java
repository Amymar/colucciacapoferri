package modelTest.sectorCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.InterfaceCardSector;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.RealFraughtSectorCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestRealFraughtSectorCard {

	@Test
	public void testRealFraughtSectorCard() {
		InterfaceCardSector card = new RealFraughtSectorCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.SectorCardType.REAL&& card.isObject()==true);
	}

	@Test
	public void testRealFraughtSectorCardString() {
		String id="Test";
		InterfaceCardSector card= new RealFraughtSectorCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.SectorCardType.REAL&& card.isObject()==true);
	}

	@Test
	public void testGetThis() {
		Map<String, InterfaceCardSector> cards=new TreeMap<String, InterfaceCardSector> ();
		assertTrue(cards.isEmpty());
		InterfaceCardSector card=new RealFraughtSectorCard();
		card.getThis("Test", cards);
		assertTrue(cards.containsKey("Test"));
		card.getThis("Test", cards);
		assertTrue(cards.size()==1);
	}


}
