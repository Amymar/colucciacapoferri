package modelTest.sectorCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.InterfaceCardSector;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.RealEmptySectorCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestRealEmptySectorCard {

	@Test
	public void testRealEmptySectorCard() {
		InterfaceCardSector card = new RealEmptySectorCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.SectorCardType.REAL&& card.isObject()==false);
	}

	@Test
	public void testRealEmptySectorCardString() {
		String id="Test";
		InterfaceCardSector card= new RealEmptySectorCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.SectorCardType.REAL&& card.isObject()==false);
	}

	@Test
	public void testGetThis() {
		Map<String, InterfaceCardSector> cards=new TreeMap<String, InterfaceCardSector> ();
		assertTrue(cards.isEmpty());
		InterfaceCardSector card=new RealEmptySectorCard();
		card.getThis("Test", cards);
		assertTrue(cards.containsKey("Test"));
		card.getThis("Test", cards);
		assertTrue(cards.size()==1);
	}


}
