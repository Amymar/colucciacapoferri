package modelTest.objectCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.SedativesObjectCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestSedativesObjectCard {

	@Test
	public void testSedativesObjectCard() {
		InterfaceObjectCard card= new SedativesObjectCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.ObjectCardType.SEDATIVES);
	}

	@Test
	public void testSedativesObjectCardString() {
		String id="Test";
		InterfaceObjectCard card= new SedativesObjectCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.ObjectCardType.SEDATIVES);
	}

	@Test
	public void testGetThis() {
		Map<String, InterfaceObjectCard> cards=new TreeMap<String, InterfaceObjectCard> ();
		assertTrue(cards.isEmpty());
		InterfaceObjectCard card=new SedativesObjectCard();
		card.getThis("Test", cards);
		assertTrue(cards.containsKey("Test"));
		card.getThis("Test", cards);
		assertTrue(cards.size()==1);
	}

}
