package modelTest.objectCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.LightObjectCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestLightObjectCard {

	@Test
	public void testLightObjectCard() {
		InterfaceObjectCard card= new LightObjectCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.ObjectCardType.LIGHT);
	}

	@Test
	public void testLightObjectCardString() {
		String id="Test";
		InterfaceObjectCard card= new LightObjectCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.ObjectCardType.LIGHT);
	}

	@Test
	public void testGetThis() {
			Map<String, InterfaceObjectCard> cards=new TreeMap<String, InterfaceObjectCard> ();
			assertTrue(cards.isEmpty());
			InterfaceObjectCard card=new LightObjectCard();
			card.getThis("Test", cards);
			assertTrue(cards.containsKey("Test"));
			card.getThis("Test", cards);
			assertTrue(cards.size()==1);
	}


}
