package modelTest.objectCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.object.AdrenalineObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestAdrenalineObjectCard {

	@Test
	public void testAdrenalineObjectCard() {
		InterfaceObjectCard card= new AdrenalineObjectCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.ObjectCardType.ADRENALINE);
	}

	@Test
	public void testAdrenalineObjectCardString() {
		String id="Test";
		InterfaceObjectCard card= new AdrenalineObjectCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.ObjectCardType.ADRENALINE);
	}

	@Test
	public void testGetThis() {
		Map<String, InterfaceObjectCard> cards=new TreeMap<String, InterfaceObjectCard> ();
		assertTrue(cards.isEmpty());
		InterfaceObjectCard card=new AdrenalineObjectCard();
		card.getThis("Test", cards);
		assertTrue(cards.containsKey("Test"));
		card.getThis("Test", cards);
		assertTrue(cards.size()==1);
	}

}
