package modelTest.objectCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.object.AttackObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestAttackObjectCard {

	@Test
	public void testAttackObjectCard() {
		InterfaceObjectCard card= new AttackObjectCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.ObjectCardType.ATTACK);
	}

	@Test
	public void testAttackObjectCardString() {
		String id="Test";
		InterfaceObjectCard card= new AttackObjectCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.ObjectCardType.ATTACK);
	}

	@Test
	public void testGetThis() {
			Map<String, InterfaceObjectCard> cards=new TreeMap<String, InterfaceObjectCard> ();
			assertTrue(cards.isEmpty());
			InterfaceObjectCard card=new AttackObjectCard();
			card.getThis("Test", cards);
			assertTrue(cards.containsKey("Test"));
			card.getThis("Test", cards);
			assertTrue(cards.size()==1);
	}


}
