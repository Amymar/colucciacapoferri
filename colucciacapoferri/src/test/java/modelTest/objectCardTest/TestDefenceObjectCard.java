package modelTest.objectCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.object.DefenceObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestDefenceObjectCard {

	@Test
	public void testDefenceObjectCard() {
		InterfaceObjectCard card= new DefenceObjectCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.ObjectCardType.DEFENCE);
	}

	@Test
	public void testDefenceObjectCardString() {
		String id="Test";
		InterfaceObjectCard card= new DefenceObjectCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.ObjectCardType.DEFENCE);
	}

	@Test
	public void testGetThis() {
			Map<String, InterfaceObjectCard> cards=new TreeMap<String, InterfaceObjectCard> ();
			assertTrue(cards.isEmpty());
			InterfaceObjectCard card=new DefenceObjectCard();
			card.getThis("Test", cards);
			assertTrue(cards.containsKey("Test"));
			card.getThis("Test", cards);
			assertTrue(cards.size()==1);
	}

}
