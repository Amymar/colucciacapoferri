package modelTest.objectCardTest;

import static org.junit.Assert.assertTrue;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.TeleportObjectCard;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

public class TestTeleportObjectCard {

	@Test
	public void testTeleportObjectCard() {
		InterfaceObjectCard card= new TeleportObjectCard();
		assertTrue(card.getId()==null&&card.getType()==Enumerations.ObjectCardType.TELEPORT);
	}

	@Test
	public void testTeleportObjectCardString() {
		String id="Test";
		InterfaceObjectCard card= new TeleportObjectCard(id);
		assertTrue(card.getId()==id&&card.getType()==Enumerations.ObjectCardType.TELEPORT);
	}

	@Test
	public void testGetThis() {
			Map<String, InterfaceObjectCard> cards=new TreeMap<String, InterfaceObjectCard> ();
			assertTrue(cards.isEmpty());
			InterfaceObjectCard card=new TeleportObjectCard();
			card.getThis("Test", cards);
			assertTrue(cards.containsKey("Test"));
			card.getThis("Test", cards);
			assertTrue(cards.size()==1);
	}

}
