package modelTest;

import static org.junit.Assert.*;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.model.boxes.Box;
import model.CharacterCard;

import org.junit.Test;

public class TestPgCard {

	@Test
	public void testPgCard() {
		Box box= new Box();
		CharacterCard pg= new CharacterCard("Test", Enumerations.Race.ALIEN, box);
		assertTrue(pg!=null);
		assertTrue(pg.getName()=="Test"&& pg.getRace()==Enumerations.Race.ALIEN && pg.getPosition()==box);
		
	}

}
