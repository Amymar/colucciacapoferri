package modelTest;

import static org.junit.Assert.*;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import model.CardSector;

import org.junit.Test;

public class TestSectorCard {

	@Test
	public void testSectorCard() {
		CardSector card = new CardSector ("Test", Enumerations.SectorCardType.REAL,true);
		assertTrue(card!=null);
		assertTrue(card.getId()=="Test" && card.getType()== Enumerations.SectorCardType.REAL && card.isObject()== true);
		
	}

}
