package view;

import java.awt.BorderLayout;
import java.awt.Color;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class StartClient {
	

	public static void main(String[] args) {
		Settings settings=new Settings();
		JFrame mainFrame = new JFrame("Escape From the Aliens in Outer Space");
		mainFrame.setBackground(Color.CYAN);
		JPanel background =new BackgroudJPanel("/Users/Marilena/Desktop/Pictures/main.jpg");
		mainFrame.setContentPane(background);
		
		startFrame(mainFrame,settings);
		
		mainFrame.setVisible(true);
		mainFrame.setSize(1024, 768);
		mainFrame.setLocation(150, 0);
		mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mainFrame.getContentPane().repaint();
		
	}
	
	public static JButton mainButton(String name, int vert, int hor, int size, Color color){
		JButton button =new JButton(name);
		button.setBackground(Color.BLACK);
		FontStyle style=new FontStyle();
		button.setFont(style.newFontStyle("A Lolita Scorned", size, color));
		button.setSize(new Dimension(vert, hor));
		button.setBorder(BorderFactory.createLineBorder(color, 1));
		button.setVisible(true);
		return button;
	}
	
	public static void startFrame(JFrame mainFrame, Settings settings){
		JPanel buttonsPanel =new JPanel();
		mainFrame.add(buttonsPanel);		
		buttonsPanel.setOpaque(false);
		buttonsPanel.setLayout(new BorderLayout());
		ActionListnerJButton listner =new ActionListnerJButton(mainFrame, settings);
		JButton newGame = mainButton("New Game", 30,40,60, new Color(204,204,0));
		buttonsPanel.add(newGame, BorderLayout.NORTH);
		JPanel southPanel=new JPanel();
		southPanel.setOpaque(false);
		JButton options = mainButton("Options", 0, 0, 34, Color.WHITE);
		southPanel.add(options);
		options.addActionListener(listner);
		JButton credits = mainButton("Credits", 0, 0, 34, Color.WHITE);
		southPanel.add(credits);
		buttonsPanel.add(southPanel, BorderLayout.SOUTH);
		
	}
}
