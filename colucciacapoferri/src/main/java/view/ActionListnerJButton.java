package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ActionListnerJButton implements ActionListener {
	JFrame frame;
	Settings settings;
	
	public ActionListnerJButton (JFrame frame, Settings settings){
		this.frame=frame;
		this.settings=settings;
	}

	public void actionPerformed(ActionEvent ae) {
		String action = ae.getActionCommand();
		if(action.equals("Options")){
			System.out.println("op press");
			getFrame().getContentPane().removeAll();
			
			options();
			
			
			
			
		}
	}
	
	

	private  void options( ){
		getFrame().setLayout(new GridLayout(4, 1));
		
		JLabel mainLabel =new JLabel ("Options");
		FontStyle style=new FontStyle();
		mainLabel.setFont(style.newFontStyle("A Lolita Scorned", 84, Color.WHITE));
		getFrame().add(mainLabel);
		
		JLabel connection=new JLabel("Connection type");
		connection.setFont(style.newFontStyle("A Lolita Scorned", 14, Color.WHITE));
		getFrame().add(connection);
		
		JPanel connectionPanel=new JPanel();
		connectionPanel.setOpaque(false);
		
		getFrame().add(connectionPanel);
		getFrame().pack();
		getFrame().setSize(1024, 768);
		getFrame().setLocation(0, 0);
		
//		getFrame().repaint();
		return;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

}
