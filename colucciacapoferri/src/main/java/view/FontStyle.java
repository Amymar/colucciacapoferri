package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

public class FontStyle {
	Map <TextAttribute, Object> attributes=new HashMap <TextAttribute, Object>();
	
	public Font newFontStyle(String name, int size, Color color){
		attributes.put(TextAttribute.FAMILY, name);
		attributes.put(TextAttribute.SIZE, size);
		attributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_REGULAR);
		attributes.put(TextAttribute.POSTURE, TextAttribute.POSTURE_REGULAR);
		attributes.put(TextAttribute.UNDERLINE, -1);
		attributes.put(TextAttribute.KERNING, 0);
		attributes.put(TextAttribute.FOREGROUND, color);
		attributes.put(TextAttribute.BACKGROUND, Color.BLACK);
		attributes.put(TextAttribute.WIDTH,TextAttribute.WIDTH_REGULAR);
		attributes.put(TextAttribute.TRACKING, TextAttribute.TRACKING_TIGHT);
		
		Font font=Font.getFont(attributes);
		Font newfont=font.deriveFont(attributes);
		return newfont;
		
	}
}
