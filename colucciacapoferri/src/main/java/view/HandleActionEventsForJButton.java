package view;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class HandleActionEventsForJButton extends JPanel implements ActionListener {


	private static final long serialVersionUID = 1L;

	public HandleActionEventsForJButton() {

        // set flow layout for the frame
        this.setLayout(new BorderLayout());

        JButton newGame = mainButton("New Game", 30,40,60, new Color(204,204,0));
        JButton options = mainButton("Options", 0, 0, 34, Color.WHITE);
        JButton credits= mainButton("Credits", 0, 0,34, Color.WHITE);

        //set action listeners for buttons
        newGame.addActionListener(this);
        options.addActionListener(this);
        credits.addActionListener(this);

        //add buttons to the frame
        add(newGame, BorderLayout.NORTH);
        add(options, BorderLayout.SOUTH);
        add(credits, BorderLayout.SOUTH);
    }
 
    public void actionPerformed(ActionEvent ae) {
        String action = ae.getActionCommand();
        if (action.equals("Yes")) {
            System.out.println("Yes Button pressed!");
        }
        else if (action.equals("No")) {
            System.out.println("No Button pressed!");
        }
    }
    
    public static JButton mainButton(String name, int vert, int hor, int size, Color color){
		JButton button =new JButton(name);
		button.setBackground(Color.BLACK);
		FontStyle style=new FontStyle();
		button.setFont(style.newFontStyle("A Lolita Scorned", size, color));
		button.setSize(new Dimension(vert, hor));
		button.setBorder(BorderFactory.createLineBorder(color, 1));
		button.setVisible(true);
		return button;
	}
 
    private static void createAndShowGUI() {
 
  //Create and set up the window.
    	JFrame mainFrame = new JFrame("Escape From the Aliens in Outer Space");
		mainFrame.setSize(1024, 768);
		mainFrame.setLocation(0, 0);
		JPanel background =new BackgroudJPanel("/Users/Marilena/Desktop/Pictures/main.jpg");
		JPanel buttonsPanel =new HandleActionEventsForJButton();
		buttonsPanel.setOpaque(false);
		background.add(buttonsPanel);		
		
  

  //Display the window.

//		mainFrame.pack();

		mainFrame.setVisible(true);

		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {

  //Schedule a job for the event-dispatching thread:

  //creating and showing this application's GUI.

  javax.swing.SwingUtilities.invokeLater(new Runnable() {

public void run() {

    createAndShowGUI();

}

  });
    }
 
}

