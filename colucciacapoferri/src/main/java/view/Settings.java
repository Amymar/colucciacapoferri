package view;

public class Settings {
	boolean socket=true;
	boolean rmi=false;
	boolean cli=false;
	boolean gui= true;
	
	public boolean isSocket() {
		return socket;
	}
	public void setSocket(boolean socket) {
		this.socket = socket;
		this.rmi=!socket;
	}
	public boolean isRmi() {
		return rmi;
	}
	public void setRmi(boolean rmi) {
		this.rmi = rmi;
		this.socket=!rmi;
	}
	public boolean isCli() {
		return cli;
	}
	public void setCli(boolean cli) {
		this.cli = cli;
		this.gui=!cli;
	}
	public boolean isGui() {
		return gui;
	}
	public void setGui(boolean gui) {
		this.gui = gui;
		this.cli=!gui;
	}
	
	

}
