package it.polimi.ingsw.colucciacapoferri.model.object;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.ObjectCardType;

import java.util.Map;

public class AdrenalineObjectCard implements InterfaceObjectCard{
	private String id;
	private static ObjectCardType type = ObjectCardType.ADRENALINE;
	
	public AdrenalineObjectCard(){
		
	}
	public AdrenalineObjectCard(String id){
		this.id=id;
	}

	public InterfaceObjectCard getThis(String id,Map<String, InterfaceObjectCard> map) {
		if(map.containsKey(id)){
			return map.get(id);
		}else{
			InterfaceObjectCard card = new AdrenalineObjectCard(id);
			map.put(id, card);
			return card;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id=id;
	}

	public ObjectCardType getType() {
		return type;
	}

	public void setType(ObjectCardType type) {
		AdrenalineObjectCard.type=type;
	}

}
