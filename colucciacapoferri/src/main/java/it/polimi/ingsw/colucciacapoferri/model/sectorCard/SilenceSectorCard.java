package it.polimi.ingsw.colucciacapoferri.model.sectorCard;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.SectorCardType;

import java.util.Map;

public class SilenceSectorCard implements InterfaceCardSector {

	private String id;
	private static SectorCardType type = SectorCardType.SILENCE;
	private static boolean object=false;
	
	public SilenceSectorCard(){
		
	}
	
	public SilenceSectorCard(String id) {
		this.setId(id);
	}

	public InterfaceCardSector getThis(String id, Map <String, InterfaceCardSector> map ) {
		if(map.containsKey(id)){
			return map.get(id);
		}else{
			InterfaceCardSector card = new SilenceSectorCard(id);
			map.put(id, card);
			return card;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public  SectorCardType getType() {
		return type;
	}

	public  void setType(SectorCardType type) {
		SilenceSectorCard.type = type;
	}

	public  boolean isObject() {
		return object;
	}

	public  void setObject(boolean object) {
		SilenceSectorCard.object = object;
	}

}
