package it.polimi.ingsw.colucciacapoferri.controller.client;

import it.polimi.ingsw.colucciacapoferri.controller.ConnectionToClient;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Logger;

public class ListenerClientSide implements Runnable {
	
	Socket socket;
	Logger log;
	
	public ListenerClientSide(Socket socket, Logger log){
		this.socket=socket;
		this.log=log;
	}

	public void run() {
		try {
			System.out.println("Inserisci il tuo nome");
			Scanner in = new Scanner(System.in);
			String line= in.next();
			ConnectionToClient connection= new ConnectionToClient(getSocket());
			connection.getOut().println(line);
		
			while (line!="end"){
				line=connection.getIn().readLine();
				String[] correct= line.split("#");
				int i=0;
				for( i=0; i<correct.length-1;i++){
					System.out.println(correct[i]);
				}
				if(correct[i].equals("£")){
					String j=in.nextLine();
					if(j.equals("\n")){
						System.out.println("entro");
						j=in.nextLine();
					}
					connection.getOut().println(j);;
				} 
				
				if(correct[i].equals("$")){
					int j=in.nextInt();			
					connection.getOut().println(j);;
				}
			}
			
			in.close();
		} catch (IOException e) {
				getLog().warning(e.toString());
		}
		
	}
		
		

	

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public Logger getLog() {
		return log;
	}

	public void setLog(Logger log) {
		this.log = log;
	}

	
}
