package it.polimi.ingsw.colucciacapoferri.controller;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoatBox;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoxSingleton;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.InterfaceCardSector;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import model.BoatCard;

public class Round implements Runnable {
	private GamingData data;
	private int roundNumber;
	private Player player;
	private BoxSingleton box;
	private boolean cardCheck=true;
	
	public Round (int number, Player pg, GamingData data){
		this.roundNumber=number;
		this.player=pg;
		this.data=data;
	}
	
	public void run(){
		getPlayer().getConnection().getOut().println(" # E' il"+(getData().getGame().getRound()+1)+"° round di "+getPlayer().getNickName()+"# #");

		
		if (getPlayer().getCharacter().getRace().equals(Race.HUMAN)){
			String ask=startItem();
			int i=0;
			int j=0;
			for(InterfaceObjectCard ob:getPlayer().getCharacter().getObjects()){
				if(!ask.equals("") && ask.equals(ob.getType().toString())&&i<1){
					getData().getObjectCards().add(getPlayer().getCharacter().getObjects().get(j));
					getPlayer().getCharacter().getObjects().remove(ob);
				}
				j++;
			}
			movement();
		}else if(getPlayer().getCharacter().getRace().equals(Race.ALIEN)){
			alienMovement();
		}
		if (isCardCheck()){
			sounds();
		}
		String end=actionOrItem();
		int i=0;
		for(InterfaceObjectCard ob:getPlayer().getCharacter().getObjects()){
			if(!end.equals("") && end.equals(ob.getType().toString())&&i<1){
				getPlayer().getCharacter().getObjects().remove(ob);
			}
		}
		getPlayer().getCharacter().getOldRounds().add(this);
	}
	
	private String startItem(){
		List <String> list=new ArrayList<String>();
		list.add(" ");
		boolean sem=true;
		while (sem && !getPlayer().getCharacter().getObjects().isEmpty()){
			System.out.println("ci sono");
			getPlayer().getConnection().getOut().println("Vuoi utilizzare un oggetto?# Digita il numero della tua scelta#1)si#2)no#$#");
			String ask="";
				try {
					ask = getPlayer().getConnection().getIn().readLine();
				} catch (IOException e) {
					Server.getLogfile().warning(e.toString());
				}
				int ch=Integer.parseInt(ask);
				
			if(ch==1){
				int i=1;
				String line="Quale oggetto?# ";
				
				for(InterfaceObjectCard ob: getPlayer().getCharacter().getObjects()){
					line+=i+") "+ob.getType().toString()+"#";
					i++;
					list.add(ob.getType().toString());
					
				}
				line+="$#";
				getPlayer().getConnection().getOut().println(line);
				try {
					ask=getPlayer().getConnection().getIn().readLine();
					ch=Integer.parseInt(ask);
				} catch (IOException e) {
					Server.getLogfile().warning(e.toString());
				}
				sem=false;
				return useItemStartRound(list.get(ch));
				
			}else{
				sem=false;
			}
		}	
		return "";
	}
	
	private void alienMovement(){
		int move = Constants.ALIENMOVEMENT;
		if (getPlayer().getCharacter().isEaten()){
			move++;
		}
		
		while(move>0){
			movement();
			move--;
			if(move<Constants.ALIENMOVEMENT && move>0){
				getPlayer().getConnection().getOut().println("Vuoi muoverti ancora?#Digita il numero della tua scelta#1)si#2)no#$#");
				try {
					String ask=getPlayer().getConnection().getIn().readLine();
					int ch=Integer.parseInt(ask);
					if (ch==2){
						move=0;
					}
				} catch (IOException e) {
					Server.getLogfile().warning(e.toString());	
				}
			}
		}
	}
	
	
	private void movement(){
		int muove = 0;
		String line ="";
		boolean sem=true;
		List<String> var = new ArrayList<String>();
		var.add(line);
		while (sem){
			line="Dove vuoi spostarti?# Digita il numero della tua scelta#";
			int i=1;
			
			for (String key: getData().getGameBoard().getSectors().get(getPlayer().getCharacter().getPosition().getId()).getNearbyBoxes().keySet()){
				line+=i+")" +key+"#";
				i++;
				var.add(key);
			}
			line+="$#";
			getPlayer().getConnection().getOut().println(line);
			try {
				line=getPlayer().getConnection().getIn().readLine();
				
				muove=Integer.parseInt(line);
			
				for(String key: getData().getGameBoard().getSectors().get(getPlayer().getCharacter().getPosition().getId()).getNearbyBoxes().keySet()){
					if (var.get(muove).equals(key)){
						sem=false;
						getPlayer().getCharacter().setPosition(getData().getGameBoard().getSectors().get(key));
						setBox(getData().getGameBoard().getSectors().get(key));
					}
				}
			}catch (SocketException e1){
				Server.getLogfile().warning(e1.toString());
				getData().getGame().getInGamePlayer().remove(getPlayer());
				sem=false;
			} catch (IOException e) {
				Server.getLogfile().warning(e.toString());
			}
			
		}
	}
	
	private void sounds (){
		if (getData().getSectorCards().isEmpty()){
			getData().creatingSectorCards();
		}
		switch (getPlayer().getCharacter().getPosition().getType()){
		case DANGER: 
			InterfaceCardSector sound=getData().getSectorCards().remove(0);
			executeSectorCard(sound);
 			break;
		case BOAT: 
			if(getPlayer().getCharacter().getRace()==Race.HUMAN){
				BoatCard card=getData().getBoatCards().remove(0);
				checkHumanVictory(card);				
			}
			break;
		default:
			getPlayer().getConnection().getOut().println("Settore sicuro# #");
			break;
		}
	}
	
	private void executeSectorCard (InterfaceCardSector sound){
		String line;
		switch (sound.getType()){
		case REAL: 
			if(sound.isObject()&& getPlayer().getCharacter().getObjects().size()<4){
				InterfaceObjectCard card=getData().getObjectCards().remove(0);
				getPlayer().getCharacter().getObjects().add(card);
				getPlayer().getConnection().getOut().println("Hai trovato un oggetto : carta "+card.getType().toString()+"# #");
			}
			getPlayer().getConnection().getOut().println("Hai fatto rumore e tutti ti hanno sentito# #");
			for(Player gamer: getData().getGame().getInGamePlayer()){
				line="Avete sentito un rumore nel settore "+getPlayer().getCharacter().getPosition().getId()+"# #";
				gamer.getConnection().getOut().println(line);
			}
			break;
		case FAKE:
			if(sound.isObject() && getPlayer().getCharacter().getObjects().size()<4){
				InterfaceObjectCard card=getData().getObjectCards().remove(0);
				getPlayer().getCharacter().getObjects().add(card);
				getPlayer().getConnection().getOut().println("Hai trovato un oggetto : carta "+card.getType().toString()+"# #");
			}
			fakeCard();
			break;
		case SILENCE: 
			getPlayer().getConnection().getOut().println("Sei come un ninja spaziale#NON HAI FATTO RUMORE# #");

			break;
		}
	}
	
	private void fakeCard(){
		String line;
		boolean sem=true;
		String fake="";
		try {
			while (sem){
				getPlayer().getConnection().getOut().println("Confondi i tuoi avversari# Fai rumore in un settore a tua scelta# Quale?#£#");
				fake=getPlayer().getConnection().getIn().readLine();
				while (fake.equals("")||fake.equals(" ")|| fake.equals("\n")){
					getPlayer().getConnection().getOut().println(" #£#");
					fake=getPlayer().getConnection().getIn().readLine();
				}
				
				for(String key: getData().getGameBoard().getSectors().keySet()){
					if(fake.equals(key)){
						sem=false;
					}
				}
				if(sem==true){
					getPlayer().getConnection().getOut().println("Settore non esistente# #");
				}				 
			}
			for(Player gamer: getData().getGame().getInGamePlayer()){
				line="Avete sentito un rumore nel settore "+fake+"# #";
				gamer.getConnection().getOut().println(line);
			}
		}catch (IOException e) {
				Server.getLogfile().warning(e.toString());
		}
	}
	
	private void checkHumanVictory(BoatCard boat){
		BoatBox boatBox=(BoatBox) getData().getGameBoard().getSectors().get( getPlayer().getCharacter().getPosition());
		switch(boat.getColor()){
		case RED: boatBox.setOpen(false);
			break;
		case GREEN: 
			if(boatBox.isOpen()){
				getPlayer().setVictory(true);
				getData().getGame().getInGamePlayer().remove(getPlayer());
				for(Player gamer: getData().getGame().getInGamePlayer()){
					String line=getPlayer().getNickName()+"ha vinto!!! ";
					gamer.getConnection().getOut().println(line);
				}
			}//TODO completare con avvisi giocatori
		}
	}
	
	private String actionOrItem(){
		String ask="";
		int ch=0;
		switch(getPlayer().getCharacter().getRace()){
		case ALIEN:
			getPlayer().getConnection().getOut().println("Vuoi attaccare?#Digita il numero della tua scelta#1)si#2)no#$#");
				try {
					ask=getPlayer().getConnection().getIn().readLine();
					ch=Integer.parseInt(ask);
			} catch (IOException e) {
				Server.getLogfile().warning(e.toString());
			}
			if (ch==1){
				attack();
			}
			break;
		case HUMAN: 
			return itemUse();
		}
		return "";
	}
	
	private String itemUse(){
		String ask="";
		int ch=0;
		boolean check=true;
		List<String> var = new ArrayList<String>();
		var.add(ask);
		while (check && !getPlayer().getCharacter().getObjects().isEmpty()){
			getPlayer().getConnection().getOut().println("Vuoi utilizzare un oggetto?# Digita il numero della tua scelta#1)si#2)no#$#");
			try {
				ask=getPlayer().getConnection().getIn().readLine();
				ch=Integer.parseInt(ask);
			} catch (IOException e) {
				Server.getLogfile().warning(e.toString());
			}
			if(ch==1){
				check=false;
				return object(var);
				
			}else if (ch==2){
				check=false;
				return "";
			}
		}
		return "";
	}
	
	private String object(List <String>var){
		int i=1;
		int ch=0;
		String ask="";
		String line="Quale oggetto?# ";
		
		for(InterfaceObjectCard ob: getPlayer().getCharacter().getObjects()){
			line+=i+") "+ob.getType().toString()+"#";
			i++;
			var.add(ob.getType().toString());
		}
		line+="$#";
		getPlayer().getConnection().getOut().println(line);
		try {
			ask=getPlayer().getConnection().getIn().readLine();
			ch=Integer.parseInt(ask);
		} catch (IOException e) {
			Server.getLogfile().warning(e.toString());
		}
		String end=useItemEndRound(var.get(ch));
		return end;
	}
	
	private String useItemStartRound (String item){
		for(ObjectCardType ob: ObjectCardType.values()){
			if(item.equals(ob.toString())){
				switch (ob){
				case ADRENALINE: adrenaline();
					return item;
				case ATTACK: attack();
					return item;
				case LIGHT: light();
					return item;
				case SEDATIVES: sedatives();
					return item;
				case TELEPORT: teleport();
					return item;
				default:getPlayer().getConnection().getOut().println("Oggetto non utilizzabile# #");
					return startItem();
					
		
				}
			}
		}
		return "";
	}
	
	private String useItemEndRound (String item){
		for(ObjectCardType ob: ObjectCardType.values()){
			if(item.equals(ob.toString())){
				switch (ob){
				case ATTACK: attack();
					return item;
				case LIGHT: light();
					return item;
				case TELEPORT: teleport();
					return item;
				default:getPlayer().getConnection().getOut().println("Oggetto non utilizzabile# #");
					return itemUse();
					
		
				}
			}
		}
		return "";
	}
	
	private void adrenaline(){
		movement();		
	}
	
	private void attack(){
		List <String > list=new ArrayList<String>();
		String line;
		boolean defence=false;
		for(Player gamer :getData().getGame().getInGamePlayer()){
			gamer.getConnection().getOut().println(getPlayer().getNickName()+" attacca in "+getPlayer().getCharacter().getPosition().getId()+"# #");
			for(InterfaceObjectCard ob: gamer.getCharacter().getObjects()){
				if(ob.getType()==ObjectCardType.DEFENCE){
					defence=true;
				}
			}
			if (gamer.getCharacter().getPosition()==getPlayer().getCharacter().getPosition() && gamer!=getPlayer()&&
					( gamer.getCharacter().getRace()==Race.ALIEN ||defence==false)){
					gamer.setDeath(true);
					gamer.getConnection().getOut().println("Sei Morto# #");
					list.add(gamer.getNickName());
			}
		}
		if(!list.isEmpty()){
			line=" Sono morti: #";
			for(String str:list){
				line+=str+"#";
			}
			line+=" #";
			for(Player gamer :getData().getGame().getInGamePlayer()){
				gamer.getConnection().getOut().println(line);
			}
		}
		
	}
	
	private void light(){
		getPlayer().getConnection().getOut().println("In quale settore devo far luce?# #");
		String line="";
		try {
			line = getPlayer().getConnection().getIn().readLine();
			while (line.equals("")||line.equals(" ")|| line.equals("\n")){
				getPlayer().getConnection().getOut().println(" #£#");
				line=getPlayer().getConnection().getIn().readLine();
			}
		} catch (IOException e) {
			Server.getLogfile().warning(e.toString());

		}
		Map <String, String> map=new TreeMap<String , String>();
		for( BoxSingleton aBox : getData().getGameBoard().getSectors().get(line).getNearbyBoxes().values()){
			for(Player gamer :getData().getGame().getInGamePlayer()){
				if(gamer.getCharacter().getPosition()==aBox || gamer.getCharacter().getPosition()==getData().getGameBoard().getSectors().get(line)){
					map.put(gamer.getNickName(),gamer.getCharacter().getPosition().getId());
				}
			}
		}
		for(Player gamer :getData().getGame().getInGamePlayer()){
			gamer.getConnection().getOut().println("E luce fu!!# "+map+"# #");
		}
	}
	
	private void sedatives(){
		setCardCheck(false);
	}
	
	private void teleport(){
		getPlayer().getCharacter().setPosition(getData().getGameBoard().getSectors().get("XU"));
	}

	public int getRoundNumber() {
		return roundNumber;
	}

	public void setRoundNumber(int roundNumber) {
		this.roundNumber = roundNumber;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public GamingData getData() {
		return data;
	}

	public void setData(GamingData data) {
		this.data = data;
	}

	public BoxSingleton getBox() {
		return box;
	}

	public void setBox(BoxSingleton box) {
		this.box = box;
	}

	public boolean isCardCheck() {
		return cardCheck;
	}

	public void setCardCheck(boolean cardCheck) {
		this.cardCheck = cardCheck;
	}

}
