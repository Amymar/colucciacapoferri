package it.polimi.ingsw.colucciacapoferri.model.boxes;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;

import java.util.Map;
import java.util.TreeMap;

public class Box implements BoxSingleton{
	private String id ;
	private Ground type;
	private Map <String, BoxSingleton> nearbyBoxes;
	
	public Box(){
		
	}
	
	public Box(String nome, Ground tipo){
		this.id=nome;
		this.type=tipo;
		this.nearbyBoxes=new TreeMap <String, BoxSingleton> ();
	}
	public BoxSingleton getThis(String idUnivoco, Ground tipo, Map <String, BoxSingleton> caselle){
		if(caselle.containsKey(idUnivoco)){
			return caselle.get(idUnivoco);
		}else{
			Box box = new Box(idUnivoco,tipo);
			caselle.put(idUnivoco, box);
			return box;
		}
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Ground getType() {
		return type;
	}

	public void setType(Ground type) {
		this.type = type;
	}

	public Map<String, BoxSingleton> getNearbyBoxes() {
		return nearbyBoxes;
	}

	public void setNearbyBoxes(Map<String, BoxSingleton> nearbyBoxes) {
		this.nearbyBoxes = nearbyBoxes;
	}

	
	

}
