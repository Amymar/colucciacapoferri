package it.polimi.ingsw.colucciacapoferri.model.character_card;

import it.polimi.ingsw.colucciacapoferri.controller.Round;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.Race;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoxSingleton;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AlienCharacter implements InterfaceCharacterCard {
	/**
	 * distinguish each character
	 */
	private String name;
	/**
	 * character race 
	 */
	private static Race race= Race.ALIEN;
	/**
	 * current position of the character on the game board 
	 */
	private BoxSingleton position;
	/**
	 * 
	 */
	List <Round> oldRounds;
	List<InterfaceObjectCard> objects;
	boolean eaten;
	

	public AlienCharacter(String name, BoxSingleton box){
		this.name=name;
		this.position=box;
		this.oldRounds= new ArrayList <Round>();
		this.objects= new ArrayList <InterfaceObjectCard>();
		this.eaten=false;
	}
	
	public AlienCharacter(){
		
	}
	
	public InterfaceCharacterCard getThis(String name, BoxSingleton box,Map<String, InterfaceCharacterCard> map) {
		if(map.containsKey(name)){
			return map.get(name);
		}else{
			InterfaceCharacterCard card = new AlienCharacter(name,box );
			map.put(name, card);
			return card;
		}
		
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Race getRace() {
		return race;
	}


	public void setRace(Race race) {
		AlienCharacter.race = race;
	}


	public BoxSingleton getPosition() {
		return position;
	}


	public void setPosition(BoxSingleton position) {
		this.position = position;
	}


	public List<Round> getOldRounds() {
		return oldRounds;
	}


	public void setOldRounds(List<Round> oldRounds) {
		this.oldRounds = oldRounds;
	}


	public List<InterfaceObjectCard> getObjects() {
		return objects;
	}


	public void setObjects(List<InterfaceObjectCard> objects) {
		this.objects = objects;
	}

	public boolean isEaten() {		
		return eaten;
	}

	public void setEaten(boolean eaten) {
		this.eaten=eaten;
	}

}
