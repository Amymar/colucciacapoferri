package it.polimi.ingsw.colucciacapoferri.controller;

public class Enumerations {
	
	public enum Humans {DOMINONI, NIGULOTTI, PORPORA, BRENDON}
	public enum Aliens {CECCARELLA, MARTANA, GALBANI, LANDON};
	public enum Race {ALIEN, HUMAN};
	public enum Ground {SAFE, DANGER ,HUMANS, ALIENS, BOAT};
	public enum Color {RED, GREEN};
	public enum SectorCardType {REAL, FAKE, SILENCE};
	public enum ObjectCardType {ATTACK, TELEPORT, ADRENALINE, SEDATIVES, LIGHT, DEFENCE};
	public enum SafeBoxCheck{A04,A05,A06,A09,A10,A11,A12,A13,B05,C01,C14,D10,D14,E02,E12,F01,F10,G07,G12,G14,H01,H02,H03,H07,H14,I01,I09,I14,K02,K05,K09,K11,L02,L04,L09,L11,L14,M02,M05,M09,M11,N03,N14,O05,O09,O14,P01,P03,P04,P12,Q01,Q04,Q06,Q11,Q14,R01,R04,R06,R07,R08,R12,T07,T08,T14,U01,U12,V01,V08,W03,W04,W05,W06,W10,W11,W12};
	public enum BoatBoxCheck{Z01,Z02,Z03,Z04};
	
	
	
	
}