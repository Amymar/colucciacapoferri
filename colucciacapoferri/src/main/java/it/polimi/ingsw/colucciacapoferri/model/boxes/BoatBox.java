package it.polimi.ingsw.colucciacapoferri.model.boxes;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;

import java.util.Map;

public class BoatBox extends Box implements BoxSingleton{
	private boolean open;
	
	public BoatBox(){
		
	}
	
	public BoatBox(String nome, Ground tipo, boolean open){
		super (nome,tipo);
		this.open=open;
	}
	
	public BoxSingleton getThis(String idUnivoco, Ground tipo, Map <String, BoxSingleton> caselle){
		if(caselle.containsKey(idUnivoco)){
			return caselle.get(idUnivoco);
		}else{
			BoxSingleton casella = new BoatBox(idUnivoco,tipo,true);
			caselle.put(idUnivoco, casella);
			return casella;
		}
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}
	
}
