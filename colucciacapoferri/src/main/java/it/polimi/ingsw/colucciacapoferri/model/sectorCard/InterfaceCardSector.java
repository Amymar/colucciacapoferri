package it.polimi.ingsw.colucciacapoferri.model.sectorCard;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.SectorCardType;

import java.util.Map;

public interface InterfaceCardSector {
	
	
	public InterfaceCardSector getThis(String id, Map <String, InterfaceCardSector> map);
	public String getId() ;	
	public void setId(String id) ;
	public SectorCardType getType();
	public void setType(SectorCardType type);
	public boolean isObject() ;	
	public void setObject(boolean object);
	
}
