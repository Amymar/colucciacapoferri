package it.polimi.ingsw.colucciacapoferri.controller;

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class Game implements Runnable {
	private Map <String, Player> players= new TreeMap <String, Player>();
	private List<Player> inGamePlayer = new LinkedList <Player> ();
	private long gameId;
	private int round;
	private GamingData data;
	
	public Game (){
		gameId= new Date().getTime();
	}
	
	//TODO add map games
	public Game getGame(Game p){
		if (p==null){
			p=new Game();
		}
		return p;
	}

	public void run(){
		Server.getLogfile().info("game "+getGameId()+" is up");
		this.data= new GamingData(this);
		setRound(0);
		assignsPg();
		roundsManager();

	}

	private void assignsPg(){
		Map <String, Player> copia= new TreeMap <String, Player>();
		copia.putAll(getPlayers());
		Random randomGiocatore = new Random();
		Random randomUmani= new Random();
		Random randomAlieni= new Random();
		List<String> keysGiocatore = new ArrayList<String>(players.keySet());
		List<String> keysUmani = new ArrayList<String>(getData().getHumans().keySet());
		List<String> keysAlieni = new ArrayList<String>(getData().getAliens().keySet());
		String randomKeyGiocatore;
		String randomKeyUmani;
		String randomKeyAlieni;

		for(int i=0, j=1; i<players.size()&&!copia.isEmpty() ; i++,j++){
			if(j%2!=0){
				randomKeyGiocatore=keysGiocatore.get( randomGiocatore.nextInt(keysGiocatore.size()) );
				randomKeyAlieni= keysAlieni.get( randomAlieni.nextInt(keysAlieni.size()) );
				getPlayers().get(randomKeyGiocatore).setCharacter(getData().getAliens().get(randomKeyAlieni));
				keysGiocatore.remove(randomKeyGiocatore);
				keysAlieni.remove(randomKeyAlieni);
				copia.remove(randomKeyGiocatore);	
			}else if (j%2==0){
				randomKeyGiocatore=keysGiocatore.get( randomGiocatore.nextInt(keysGiocatore.size()) );
				randomKeyUmani= keysUmani.get( randomUmani.nextInt(keysUmani.size()) );
				getPlayers().get(randomKeyGiocatore).setCharacter(getData().getHumans().get(randomKeyUmani));
				keysGiocatore.remove(randomKeyGiocatore);
				keysUmani.remove(randomKeyUmani);
				copia.remove(randomKeyGiocatore); 
			}
		}
		for(Player player: getPlayers().values()){
			String line="Il tuo personaggio è "+player.getCharacter().getName()+" "+player.getCharacter().getRace().toString()+"# premi un qualsiasi tasto seguito da invio per continuare#";
			player.getConnection().getOut().println(line);
//			try {
//				line=player.getConnection().getIn().readLine();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
		}
	}
	
	private void checkVictory(){
		for(Player player: getInGamePlayer()){
			if (getInGamePlayer().size()>Constants.MINPLAYERS && player.isVictory()){
				player.getConnection().getOut().println("HAI VINTO# speriamo di rivederti presto# premi un qualsiasi tasto seguito da invio per chiudere#");
				player.getConnection().getOut().println("end");
			}else if((getInGamePlayer().size()>Constants.MINPLAYERS && !player.isVictory())|| player.isDeath()){
				player.getConnection().getOut().println("HAI PERSO# speriamo di rivederti presto# premi un qualsiasi tasto seguito da invio per chiudere#");
				player.getConnection().getOut().println("end");
			}else if (!player.isDeath()){
				player.getConnection().getOut().println("HAI VINTO per abbandono# speriamo di rivederti presto# premi un qualsiasi tasto seguito da invio per chiudere#");
				player.getConnection().getOut().println("end");
			}
		}
	}

	private void roundsManager() {
		while (getRound()<Constants.NMAXROUNDS && getInGamePlayer().size()>Constants.MINPLAYERS){
			for( Player player: getInGamePlayer()){
				if(!player.isDeath() && !player.isVictory()){
					Round round =new Round(getRound(), player, getData());
					Thread roundThread=new Thread(round);
					roundThread.start();
					try {
						roundThread.join();
					} catch (InterruptedException e) {
						Server.getLogfile().warning(e.toString());
					}
				}
			}
			setRound(getRound()+1);
		}
		if(getRound()>=Constants.NMAXROUNDS){
			checkState();
		}
		checkVictory();
	}
	
	private void checkState(){
		for(Player gamer: getPlayers().values()){
			if(gamer.isDeath()){
				gamer.setVictory(false);
			}
		}
	}

	public Map<String, Player> getPlayers() {
		return players;
	}

	public void setPlayers(Map<String, Player> players) {
		this.players = players;
	}

	public List<Player> getInGamePlayer() {
		return inGamePlayer;
	}

	public void setInGamePlayer(List<Player> inGamePlayer) {
		this.inGamePlayer = inGamePlayer;
	}

	public long getGameId() {
		return gameId;
	}

	public void setGameId(long gameId) {
		this.gameId = gameId;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public GamingData getData() {
		return data;
	}

	public void setData(GamingData data) {
		this.data = data;
	}
	
	
}