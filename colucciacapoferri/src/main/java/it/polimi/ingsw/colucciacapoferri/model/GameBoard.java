package it.polimi.ingsw.colucciacapoferri.model;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoatBox;
import it.polimi.ingsw.colucciacapoferri.model.boxes.Box;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoxSingleton;

import java.util.Map;
import java.util.TreeMap;


public class GameBoard {

	private Map<String, BoxSingleton> sectors;

	
	public GameBoard (){
		
		this.sectors=new TreeMap <String, BoxSingleton>();
		
		//Scialuppe
		BoxSingleton scialuppa= new BoatBox();
		scialuppa.getThis("Z01", Ground.BOAT, sectors);
		scialuppa.getThis("Z02", Ground.BOAT, sectors);
		scialuppa.getThis("Z03", Ground.BOAT, sectors);
		scialuppa.getThis("Z04", Ground.BOAT, sectors);
		
		
		//basi
		BoxSingleton base =new Box();
		base.getThis ("XU", Ground.HUMANS, getSectors());
		base.getThis ("XA", Ground.ALIENS, getSectors());


		BoxSingleton box = new Box();
		
		//caselle A
		box.getThis ("A02", Ground.DANGER, getSectors());
		box.getThis ("A03", Ground.DANGER, getSectors());
		box.getThis ("A04", Ground.SAFE, getSectors());
		box.getThis ("A05", Ground.SAFE, getSectors());
		box.getThis ("A06", Ground.SAFE, getSectors());
		box.getThis ("A09", Ground.SAFE, getSectors());
		box.getThis ("A10", Ground.SAFE, getSectors());
		box.getThis ("A11", Ground.SAFE, getSectors());
		box.getThis ("A12", Ground.SAFE, getSectors());
		box.getThis("A13", Ground.SAFE, getSectors());
		box.getThis ("A14", Ground.DANGER, getSectors());
		
		//caselle B
		box.getThis ("B01", Ground.DANGER, getSectors());
		box.getThis ("B03", Ground.DANGER, getSectors());
		box.getThis ("B04", Ground.DANGER, getSectors());
		box.getThis ("B05", Ground.SAFE, getSectors());
		box.getThis ("B06", Ground.DANGER, getSectors());
		box.getThis ("B08", Ground.DANGER, getSectors());
		box.getThis ("B09", Ground.DANGER, getSectors());
		box.getThis ("B10", Ground.SAFE, getSectors());
		box.getThis ("B11", Ground.DANGER, getSectors());
		box.getThis ("B12", Ground.DANGER, getSectors());
		box.getThis ("B14", Ground.DANGER, getSectors());
		
		//caselle C
		box.getThis ("C01", Ground.SAFE, getSectors());
		box.getThis ("C02", Ground.DANGER, getSectors());
		box.getThis ("C03", Ground.DANGER, getSectors());
		box.getThis ("C04", Ground.DANGER, getSectors());
		box.getThis ("C05", Ground.DANGER, getSectors());
		box.getThis ("C06", Ground.DANGER, getSectors());
		box.getThis ("C07", Ground.DANGER, getSectors());
		box.getThis ("C08", Ground.DANGER, getSectors());
		box.getThis ("C09", Ground.DANGER, getSectors());
		box.getThis ("C10", Ground.DANGER, getSectors());
		box.getThis ("C11", Ground.DANGER, getSectors());
		box.getThis ("C12", Ground.DANGER, getSectors());
		box.getThis ("C13", Ground.DANGER, getSectors());
		box.getThis ("C14", Ground.SAFE, getSectors());
		
		//caselle D
		box.getThis ("D02", Ground.DANGER, getSectors());
		box.getThis ("D03", Ground.DANGER, getSectors());
		box.getThis ("D05", Ground.DANGER, getSectors());
		box.getThis ("D08", Ground.DANGER, getSectors());
		box.getThis ("D09", Ground.DANGER, getSectors());
		box.getThis ("D10", Ground.SAFE, getSectors());
		box.getThis ("D11", Ground.DANGER, getSectors());
		box.getThis ("D12", Ground.DANGER, getSectors());
		box.getThis ("D13", Ground.DANGER, getSectors());
		box.getThis ("D14", Ground.SAFE, getSectors());
		
		//caselle E
		box.getThis ("E02", Ground.SAFE, getSectors());
		box.getThis ("E03", Ground.DANGER, getSectors());
		box.getThis ("E04", Ground.DANGER, getSectors());
		box.getThis ("E05", Ground.DANGER, getSectors());
		box.getThis ("E06", Ground.DANGER, getSectors());
		box.getThis ("E08", Ground.DANGER, getSectors());
		box.getThis ("E09", Ground.DANGER, getSectors());
		box.getThis ("E10", Ground.DANGER, getSectors());
		box.getThis ("E11", Ground.DANGER, getSectors());
		box.getThis ("E12", Ground.SAFE, getSectors());
		box.getThis ("E13", Ground.DANGER, getSectors());
		
		//caselle F
		box.getThis ("F01", Ground.SAFE, getSectors());
		box.getThis ("F02", Ground.DANGER, getSectors());
		box.getThis ("F03", Ground.DANGER, getSectors());
		box.getThis ("F04", Ground.DANGER, getSectors());
		box.getThis ("F05", Ground.DANGER, getSectors());
		box.getThis ("F06", Ground.DANGER, getSectors());
		box.getThis ("F07", Ground.DANGER, getSectors());
		box.getThis ("F08", Ground.DANGER, getSectors());
		box.getThis ("F09", Ground.DANGER, getSectors());
		box.getThis ("F10", Ground.SAFE, getSectors());
		box.getThis ("F11", Ground.DANGER, getSectors());
		
		//caselle G
		box.getThis ("G01", Ground.DANGER, getSectors());
		box.getThis ("G02", Ground.DANGER, getSectors());
		box.getThis ("G03", Ground.DANGER, getSectors());
		box.getThis ("G04", Ground.DANGER, getSectors());
		box.getThis ("G05", Ground.DANGER, getSectors());
		box.getThis ("G06", Ground.DANGER, getSectors());
		box.getThis ("G07", Ground.SAFE, getSectors());
		box.getThis ("G08", Ground.DANGER, getSectors());
		box.getThis ("G09", Ground.DANGER, getSectors());
		box.getThis ("G10", Ground.DANGER, getSectors());
		box.getThis ("G11", Ground.DANGER, getSectors());
		box.getThis ("G12", Ground.SAFE, getSectors());
		box.getThis ("G13", Ground.DANGER, getSectors());
		box.getThis ("G14", Ground.SAFE, getSectors());
		
		//caselle H
		box.getThis ("H01", Ground.SAFE, getSectors());
		box.getThis ("H02", Ground.SAFE, getSectors());
		box.getThis ("H03", Ground.SAFE, getSectors());
		box.getThis ("H04", Ground.DANGER, getSectors());
		box.getThis ("H06", Ground.DANGER, getSectors());
		box.getThis ("H07", Ground.SAFE, getSectors());
		box.getThis ("H08", Ground.DANGER, getSectors());
		box.getThis ("H09", Ground.DANGER, getSectors());
		box.getThis ("H12", Ground.DANGER, getSectors());
		box.getThis ("H13", Ground.DANGER, getSectors());
		box.getThis ("H14", Ground.SAFE, getSectors());
		
		//caselle I
		box.getThis ("I01", Ground.SAFE, getSectors());
		box.getThis ("I02", Ground.DANGER, getSectors());
		box.getThis ("I03", Ground.DANGER, getSectors());
		box.getThis ("I04", Ground.DANGER, getSectors());
		box.getThis ("I05", Ground.DANGER, getSectors());
		box.getThis ("I07", Ground.DANGER, getSectors());
		box.getThis ("I08", Ground.DANGER, getSectors());
		box.getThis ("I09", Ground.SAFE, getSectors());
		box.getThis ("I10", Ground.DANGER, getSectors());
		box.getThis ("I11", Ground.DANGER, getSectors());
		box.getThis ("I13", Ground.DANGER, getSectors());
		box.getThis ("I14", Ground.SAFE, getSectors());
		
		//caselle J
		box.getThis ("J01", Ground.SAFE, getSectors());
		box.getThis ("J02", Ground.DANGER, getSectors());
		box.getThis ("J03", Ground.DANGER, getSectors());
		box.getThis ("J04", Ground.DANGER, getSectors());
		box.getThis ("J05", Ground.DANGER, getSectors());
		box.getThis ("J06", Ground.DANGER, getSectors());
		box.getThis ("J08", Ground.DANGER, getSectors());
		box.getThis ("J09", Ground.DANGER, getSectors());
		box.getThis ("J10", Ground.DANGER, getSectors());
		box.getThis ("J11", Ground.DANGER, getSectors());
		box.getThis ("J13", Ground.DANGER, getSectors());
		box.getThis ("J14", Ground.SAFE, getSectors());

		// caselle K
		box.getThis ("K01", Ground.DANGER, getSectors());
		box.getThis ("K02", Ground.SAFE, getSectors());
		box.getThis ("K03", Ground.DANGER, getSectors());
		box.getThis ("K04", Ground.DANGER, getSectors());
		box.getThis ("K05", Ground.SAFE, getSectors());
		box.getThis ("K06", Ground.DANGER, getSectors());
		box.getThis ("K08", Ground.DANGER, getSectors());
		box.getThis ("K09", Ground.SAFE, getSectors());
		box.getThis ("K10", Ground.DANGER, getSectors());
		box.getThis ("K11", Ground.SAFE, getSectors());
		box.getThis ("K12", Ground.DANGER, getSectors());
		box.getThis ("K14", Ground.DANGER, getSectors());
		
		//casselle L
		box.getThis ("L01", Ground.DANGER, getSectors());
		box.getThis ("L02", Ground.SAFE, getSectors());
		box.getThis ("L03", Ground.DANGER, getSectors());
		box.getThis ("L04", Ground.SAFE, getSectors());
		box.getThis ("L05", Ground.DANGER, getSectors());
		box.getThis ("L09", Ground.SAFE, getSectors());
		box.getThis ("L10", Ground.DANGER, getSectors());
		box.getThis ("L11", Ground.SAFE, getSectors());
		box.getThis ("L12", Ground.DANGER, getSectors());
		box.getThis ("L13", Ground.DANGER, getSectors());
		box.getThis ("L14", Ground.SAFE, getSectors());
		
		//caselle M
		box.getThis ("M01", Ground.DANGER, getSectors());
		box.getThis ("M02", Ground.SAFE, getSectors());
		box.getThis ("M03", Ground.DANGER, getSectors());
		box.getThis ("M04", Ground.DANGER, getSectors());
		box.getThis ("M05", Ground.SAFE, getSectors());
		box.getThis ("M06", Ground.DANGER, getSectors());
		box.getThis ("M08", Ground.DANGER, getSectors());
		box.getThis ("M09", Ground.SAFE, getSectors());
		box.getThis ("M10", Ground.DANGER, getSectors());
		box.getThis ("M11", Ground.SAFE, getSectors());
		box.getThis ("M12", Ground.DANGER, getSectors());
		box.getThis ("M13", Ground.DANGER, getSectors());
		box.getThis ("M14", Ground.DANGER, getSectors());
		
		//caselle N
		box.getThis ("N01", Ground.DANGER, getSectors());
		box.getThis ("N02", Ground.DANGER, getSectors());
		box.getThis ("N03", Ground.SAFE, getSectors());
		box.getThis ("N04", Ground.DANGER, getSectors());
		box.getThis ("N05", Ground.DANGER, getSectors());
		box.getThis ("N06", Ground.DANGER, getSectors());		
		box.getThis ("N08", Ground.DANGER, getSectors());
		box.getThis("N09", Ground.DANGER, getSectors());
		box.getThis ("N10", Ground.DANGER, getSectors());
		box.getThis ("N11", Ground.DANGER, getSectors());
		box.getThis ("N12", Ground.DANGER, getSectors());
		box.getThis ("N13", Ground.DANGER, getSectors());
		box.getThis ("N14", Ground.SAFE, getSectors());
		
		//caselle O
		box.getThis ("O02", Ground.DANGER, getSectors());
		box.getThis ("O05", Ground.SAFE, getSectors());
		box.getThis ("O06", Ground.DANGER, getSectors());
		box.getThis ("O07", Ground.DANGER, getSectors());
		box.getThis ("O08", Ground.DANGER, getSectors());
		box.getThis ("O09", Ground.SAFE, getSectors());
		box.getThis ("O10", Ground.DANGER, getSectors());
		box.getThis ("O11", Ground.DANGER, getSectors());
		box.getThis ("O12", Ground.DANGER, getSectors());
		box.getThis ("O13", Ground.DANGER, getSectors());
		box.getThis ("O14", Ground.SAFE, getSectors());
		
		//caselle P
		box.getThis ("P01", Ground.SAFE, getSectors());
		box.getThis ("P02", Ground.DANGER, getSectors());
		box.getThis ("P03", Ground.SAFE, getSectors());
		box.getThis ("P04", Ground.SAFE, getSectors());
		box.getThis ("P05", Ground.DANGER, getSectors());
		box.getThis ("P06", Ground.DANGER, getSectors());
		box.getThis ("P07", Ground.DANGER, getSectors());
		box.getThis ("P08", Ground.DANGER, getSectors());
		box.getThis ("P09", Ground.DANGER, getSectors());
		box.getThis ("P10", Ground.DANGER, getSectors());
		box.getThis ("P11", Ground.DANGER, getSectors());
		box.getThis ("P12", Ground.SAFE, getSectors());
		box.getThis ("P13", Ground.DANGER, getSectors());
		box.getThis ("P14", Ground.DANGER, getSectors());
		
		//caselle Q
		box.getThis("Q01",Ground.SAFE,getSectors());
		box.getThis("Q02",Ground.DANGER,getSectors());
		box.getThis("Q03",Ground.DANGER,getSectors());
		box.getThis("Q04",Ground.SAFE,getSectors());
		box.getThis("Q05",Ground.DANGER,getSectors());
		box.getThis("Q06",Ground.SAFE,getSectors());
		box.getThis("Q07",Ground.DANGER,getSectors());
		box.getThis("Q08",Ground.DANGER,getSectors());
		box.getThis("Q09",Ground.DANGER,getSectors());
		box.getThis("Q10",Ground.DANGER,getSectors());
		box.getThis("Q11",Ground.SAFE,getSectors());
		box.getThis("Q12",Ground.DANGER,getSectors());
		box.getThis("Q13",Ground.DANGER,getSectors());
		box.getThis("Q14",Ground.SAFE,getSectors());
		
		//caselle R
		box.getThis("R01",Ground.SAFE,getSectors());
		box.getThis("R02",Ground.DANGER,getSectors());
		box.getThis("R03",Ground.DANGER,getSectors());
		box.getThis("R04",Ground.SAFE,getSectors());
		box.getThis("R05",Ground.DANGER,getSectors());
		box.getThis("R06",Ground.SAFE,getSectors());
		box.getThis("R07",Ground.SAFE,getSectors());
		box.getThis("R08",Ground.SAFE,getSectors());
		box.getThis("R09",Ground.DANGER,getSectors());
		box.getThis("R12",Ground.SAFE,getSectors());
		box.getThis("R13",Ground.DANGER,getSectors());
		
		//caselle S
		box.getThis("S02",Ground.DANGER,getSectors());
		box.getThis("S04",Ground.DANGER,getSectors());
		box.getThis("S05",Ground.DANGER,getSectors());
		box.getThis("S06",Ground.DANGER,getSectors());
		box.getThis("S07",Ground.DANGER,getSectors());
		box.getThis("S08",Ground.DANGER,getSectors());
		box.getThis("S09",Ground.DANGER,getSectors());
		box.getThis("S12",Ground.DANGER,getSectors());
		box.getThis("S13",Ground.DANGER,getSectors());
		
		//caselle T
		box.getThis("T02",Ground.DANGER,getSectors());
		box.getThis("T05",Ground.DANGER,getSectors());
		box.getThis("T06",Ground.DANGER,getSectors());
		box.getThis("T07",Ground.SAFE,getSectors());
		box.getThis("T08",Ground.SAFE,getSectors());
		box.getThis("T11",Ground.DANGER,getSectors());
		box.getThis("T12",Ground.DANGER,getSectors());
		box.getThis("T13",Ground.DANGER,getSectors());
		box.getThis("T14",Ground.SAFE,getSectors());
		
		//caselle U
		box.getThis("U01",Ground.SAFE,getSectors());
		box.getThis("U02",Ground.DANGER,getSectors());
		box.getThis("U03",Ground.DANGER,getSectors());
		box.getThis("U04",Ground.DANGER,getSectors());
		box.getThis("U05",Ground.SAFE,getSectors());
		box.getThis("U06",Ground.DANGER,getSectors());
		box.getThis("U07",Ground.DANGER,getSectors());
		box.getThis("U08",Ground.DANGER,getSectors());
		box.getThis("U09",Ground.DANGER,getSectors());
		box.getThis("U10",Ground.DANGER,getSectors());
		box.getThis("U11",Ground.DANGER,getSectors());
		box.getThis("U12",Ground.SAFE,getSectors());
		box.getThis("U13",Ground.DANGER,getSectors());
		box.getThis("U14",Ground.DANGER,getSectors());
		
		//caselle V
		box.getThis("V01",Ground.SAFE,getSectors());
		box.getThis("V03",Ground.DANGER,getSectors());
		box.getThis("V04",Ground.DANGER,getSectors());
		box.getThis("V05",Ground.DANGER,getSectors());
		box.getThis("V06",Ground.DANGER,getSectors());
		box.getThis("V08",Ground.SAFE,getSectors());
		box.getThis("V09",Ground.DANGER,getSectors());
		box.getThis("V10",Ground.DANGER,getSectors());
		box.getThis("V11",Ground.DANGER,getSectors());
		box.getThis("V12",Ground.DANGER,getSectors());
		box.getThis("V14",Ground.DANGER,getSectors());
		
		//caselle W
		box.getThis("W02",Ground.DANGER,getSectors());
		box.getThis("W03",Ground.SAFE,getSectors());
		box.getThis("W04",Ground.SAFE,getSectors());
		box.getThis("W05",Ground.SAFE,getSectors());
		box.getThis("W06",Ground.SAFE,getSectors());
		box.getThis("W09",Ground.DANGER,getSectors());
		box.getThis("W10",Ground.SAFE,getSectors());
		box.getThis("W11",Ground.SAFE,getSectors());
		box.getThis("W12",Ground.SAFE,getSectors());
		box.getThis("W13",Ground.DANGER,getSectors());
		box.getThis("W14",Ground.DANGER,getSectors());


		
	
		//adiacenti basi
		this.getSectors().get("XU").getNearbyBoxes().put("K08",this.getSectors().get("K08"));
		this.getSectors().get("XU").getNearbyBoxes().put("K09", this.getSectors().get("K09"));
		this.getSectors().get("XU").getNearbyBoxes().put("L09", this.getSectors().get("L09"));
		this.getSectors().get("XU").getNearbyBoxes().put("M08", this.getSectors().get("M08"));
		this.getSectors().get("XU").getNearbyBoxes().put("M09", this.getSectors().get("M09"));
		
		this.getSectors().get("XA").getNearbyBoxes().put("K06", this.getSectors().get("K06"));
		this.getSectors().get("XA").getNearbyBoxes().put("L05", this.getSectors().get("L05"));
		this.getSectors().get("XA").getNearbyBoxes().put("M06", this.getSectors().get("M06"));
		
		//adiacenti scialuppe
		this.getSectors().get("Z01").getNearbyBoxes().put("A02", this.getSectors().get("A02"));
		this.getSectors().get("Z01").getNearbyBoxes().put("A03", this.getSectors().get("A03"));
		this.getSectors().get("Z01").getNearbyBoxes().put("B01", this.getSectors().get("B01"));
		this.getSectors().get("Z01").getNearbyBoxes().put("B03", this.getSectors().get("B03"));
		this.getSectors().get("Z01").getNearbyBoxes().put("C02",this.getSectors().get("C02"));
		this.getSectors().get("Z01").getNearbyBoxes().put("C03", this.getSectors().get("C03"));
		
		this.getSectors().get("Z02").getNearbyBoxes().put("U02", this.getSectors().get("U02"));
		this.getSectors().get("Z02").getNearbyBoxes().put("U03", this.getSectors().get("U03"));
		this.getSectors().get("Z02").getNearbyBoxes().put("V01", this.getSectors().get("V01"));
		this.getSectors().get("Z02").getNearbyBoxes().put("V03", this.getSectors().get("V03"));
		this.getSectors().get("Z02").getNearbyBoxes().put("W02", this.getSectors().get("W02"));
		this.getSectors().get("Z02").getNearbyBoxes().put("W03", this.getSectors().get("W03"));
		
		this.getSectors().get("Z03").getNearbyBoxes().put("U13",this.getSectors().get("U13"));
		this.getSectors().get("Z03").getNearbyBoxes().put("U14",this.getSectors().get("U14"));
		this.getSectors().get("Z03").getNearbyBoxes().put("V12",this.getSectors().get("V12"));
		this.getSectors().get("Z03").getNearbyBoxes().put("V14",this.getSectors().get("V14"));
		this.getSectors().get("Z03").getNearbyBoxes().put("W13",this.getSectors().get("W13"));
		this.getSectors().get("Z03").getNearbyBoxes().put("W14",this.getSectors().get("W14"));
		
		this.getSectors().get("Z04").getNearbyBoxes().put("A13",this.getSectors().get("A13"));
		this.getSectors().get("Z04").getNearbyBoxes().put("A14",this.getSectors().get("A14"));
		this.getSectors().get("Z04").getNearbyBoxes().put("B12",this.getSectors().get("B12"));
		this.getSectors().get("Z04").getNearbyBoxes().put("B14",this.getSectors().get("B14"));
		this.getSectors().get("Z04").getNearbyBoxes().put("C13",this.getSectors().get("C13"));
		this.getSectors().get("Z04").getNearbyBoxes().put("C14",this.getSectors().get("C14"));


		
		//adiacenti A
		this.getSectors().get("A02").getNearbyBoxes().put("B01",this.getSectors().get("B01"));
		this.getSectors().get("A02").getNearbyBoxes().put("Z01",this.getSectors().get("Z01"));
		this.getSectors().get("A02").getNearbyBoxes().put("A03",this.getSectors().get("A03"));
		
		this.getSectors().get("A03").getNearbyBoxes().put("Z01",this.getSectors().get("Z01"));
		this.getSectors().get("A03").getNearbyBoxes().put("B03",this.getSectors().get("B03"));
		this.getSectors().get("A03").getNearbyBoxes().put("A04",this.getSectors().get("A04"));
		this.getSectors().get("A03").getNearbyBoxes().put("A02",this.getSectors().get("A02"));
		
		this.getSectors().get("A04").getNearbyBoxes().put("B03", this.getSectors().get("B03"));
		this.getSectors().get("A04").getNearbyBoxes().put("B04", this.getSectors().get("B04"));
		this.getSectors().get("A04").getNearbyBoxes().put("A05", this.getSectors().get("A05"));
		this.getSectors().get("A04").getNearbyBoxes().put("A03",this.getSectors().get("A03"));
		
		this.getSectors().get("A05").getNearbyBoxes().put("A06", this.getSectors().get("A06"));
		this.getSectors().get("A05").getNearbyBoxes().put("B04", this.getSectors().get("B04"));
		this.getSectors().get("A05").getNearbyBoxes().put("B05", this.getSectors().get("B05"));
		this.getSectors().get("A05").getNearbyBoxes().put("A04",this.getSectors().get("A04"));
		
		this.getSectors().get("A06").getNearbyBoxes().put("A05",this.getSectors().get("A05"));
		this.getSectors().get("A06").getNearbyBoxes().put("B05",this.getSectors().get("B05"));
		this.getSectors().get("A06").getNearbyBoxes().put("B06",this.getSectors().get("B06"));
		
		this.getSectors().get("A09").getNearbyBoxes().put("B08",this.getSectors().get("B08"));
		this.getSectors().get("A09").getNearbyBoxes().put("B09",this.getSectors().get("B09"));
		this.getSectors().get("A09").getNearbyBoxes().put("A10",this.getSectors().get("A10"));

		this.getSectors().get("A10").getNearbyBoxes().put("A09",this.getSectors().get("A09"));
		this.getSectors().get("A10").getNearbyBoxes().put("B09",this.getSectors().get("B09"));
		this.getSectors().get("A10").getNearbyBoxes().put("B10",this.getSectors().get("B10"));
		this.getSectors().get("A10").getNearbyBoxes().put("A11",this.getSectors().get("A11"));

		this.getSectors().get("A11").getNearbyBoxes().put("A10",this.getSectors().get("A10"));
		this.getSectors().get("A11").getNearbyBoxes().put("B10",this.getSectors().get("B10"));
		this.getSectors().get("A11").getNearbyBoxes().put("B11",this.getSectors().get("B11"));
		this.getSectors().get("A11").getNearbyBoxes().put("A12",this.getSectors().get("A12"));

		this.getSectors().get("A12").getNearbyBoxes().put("A11",this.getSectors().get("A11"));
		this.getSectors().get("A12").getNearbyBoxes().put("B11",this.getSectors().get("B11"));
		this.getSectors().get("A12").getNearbyBoxes().put("B12",this.getSectors().get("B12"));
		this.getSectors().get("A12").getNearbyBoxes().put("A13",this.getSectors().get("A13"));
		
		this.getSectors().get("A13").getNearbyBoxes().put("A12",this.getSectors().get("A12"));
		this.getSectors().get("A13").getNearbyBoxes().put("B12",this.getSectors().get("B12"));
		this.getSectors().get("A13").getNearbyBoxes().put("Z04",this.getSectors().get("Z04"));
		this.getSectors().get("A13").getNearbyBoxes().put("A14",this.getSectors().get("A14"));
		
		this.getSectors().get("A14").getNearbyBoxes().put("A13",this.getSectors().get("A13"));
		this.getSectors().get("A14").getNearbyBoxes().put("Z04",this.getSectors().get("Z04"));
		this.getSectors().get("A14").getNearbyBoxes().put("B14",this.getSectors().get("B14"));
		
		//adiacenti B		
		this.getSectors().get("B01").getNearbyBoxes().put("A02",this.getSectors().get("A02"));
		this.getSectors().get("B01").getNearbyBoxes().put("Z01",this.getSectors().get("Z01"));
		this.getSectors().get("B01").getNearbyBoxes().put("C02",this.getSectors().get("C02"));
		this.getSectors().get("B01").getNearbyBoxes().put("C01",this.getSectors().get("C01"));
	
		this.getSectors().get("B03").getNearbyBoxes().put("A03",this.getSectors().get("A03"));
		this.getSectors().get("B03").getNearbyBoxes().put("A04",this.getSectors().get("A04"));
		this.getSectors().get("B03").getNearbyBoxes().put("Z01",this.getSectors().get("Z01"));
		this.getSectors().get("B03").getNearbyBoxes().put("B04",this.getSectors().get("B04"));
		this.getSectors().get("B03").getNearbyBoxes().put("C03",this.getSectors().get("C03"));
		this.getSectors().get("B03").getNearbyBoxes().put("C04",this.getSectors().get("C04"));
		
		this.getSectors().get("B04").getNearbyBoxes().put("A04",this.getSectors().get("A04"));
		this.getSectors().get("B04").getNearbyBoxes().put("A05",this.getSectors().get("A05"));
		this.getSectors().get("B04").getNearbyBoxes().put("B03",this.getSectors().get("B03"));
		this.getSectors().get("B04").getNearbyBoxes().put("B05",this.getSectors().get("B05"));
		this.getSectors().get("B04").getNearbyBoxes().put("C04",this.getSectors().get("C04"));
		this.getSectors().get("B04").getNearbyBoxes().put("C05",this.getSectors().get("C05"));
		this.getSectors().get("B05").getNearbyBoxes().put("A05",this.getSectors().get("A05"));
		this.getSectors().get("B05").getNearbyBoxes().put("A06",this.getSectors().get("A06"));
		this.getSectors().get("B05").getNearbyBoxes().put("B04",this.getSectors().get("B04"));
		this.getSectors().get("B05").getNearbyBoxes().put("B06",this.getSectors().get("B06"));
		this.getSectors().get("B05").getNearbyBoxes().put("C05",this.getSectors().get("C05"));
		this.getSectors().get("B05").getNearbyBoxes().put("C06",this.getSectors().get("C06"));
		this.getSectors().get("B06").getNearbyBoxes().put("A06",this.getSectors().get("A06"));
		this.getSectors().get("B06").getNearbyBoxes().put("B05",this.getSectors().get("B05"));
		this.getSectors().get("B06").getNearbyBoxes().put("C06",this.getSectors().get("C06"));
		this.getSectors().get("B06").getNearbyBoxes().put("C07",this.getSectors().get("C07"));

		this.getSectors().get("B08").getNearbyBoxes().put("A09",this.getSectors().get("A09"));
		this.getSectors().get("B08").getNearbyBoxes().put("B09",this.getSectors().get("B09"));
		this.getSectors().get("B08").getNearbyBoxes().put("C08",this.getSectors().get("C08"));
		this.getSectors().get("B08").getNearbyBoxes().put("C09",this.getSectors().get("C09"));
		
		this.getSectors().get("B09").getNearbyBoxes().put("A09",this.getSectors().get("A09"));
		this.getSectors().get("B09").getNearbyBoxes().put("A10",this.getSectors().get("A10"));
		this.getSectors().get("B09").getNearbyBoxes().put("B08",this.getSectors().get("B08"));
		this.getSectors().get("B09").getNearbyBoxes().put("B10",this.getSectors().get("B10"));
		this.getSectors().get("B09").getNearbyBoxes().put("C09",this.getSectors().get("C09"));
		this.getSectors().get("B09").getNearbyBoxes().put("C10",this.getSectors().get("C10"));
		
		this.getSectors().get("B10").getNearbyBoxes().put("A10",this.getSectors().get("A10"));
		this.getSectors().get("B10").getNearbyBoxes().put("A11",this.getSectors().get("A11"));
		this.getSectors().get("B10").getNearbyBoxes().put("B09",this.getSectors().get("B09"));
		this.getSectors().get("B10").getNearbyBoxes().put("B11",this.getSectors().get("B11"));
		this.getSectors().get("B10").getNearbyBoxes().put("C10",this.getSectors().get("C10"));
		this.getSectors().get("B10").getNearbyBoxes().put("C11",this.getSectors().get("C11"));
		
		this.getSectors().get("B11").getNearbyBoxes().put("A11",this.getSectors().get("A11"));
		this.getSectors().get("B11").getNearbyBoxes().put("A12",this.getSectors().get("A12"));
		this.getSectors().get("B11").getNearbyBoxes().put("B10",this.getSectors().get("B10"));
		this.getSectors().get("B11").getNearbyBoxes().put("B12",this.getSectors().get("B12"));
		this.getSectors().get("B11").getNearbyBoxes().put("C11",this.getSectors().get("C11"));
		this.getSectors().get("B11").getNearbyBoxes().put("C12",this.getSectors().get("C12"));
		
		this.getSectors().get("B12").getNearbyBoxes().put("A12",this.getSectors().get("A12"));
		this.getSectors().get("B12").getNearbyBoxes().put("A13",this.getSectors().get("A13"));
		this.getSectors().get("B12").getNearbyBoxes().put("B11",this.getSectors().get("B11"));
		this.getSectors().get("B12").getNearbyBoxes().put("Z04",this.getSectors().get("Z04"));
		this.getSectors().get("B12").getNearbyBoxes().put("C12",this.getSectors().get("C12"));
		this.getSectors().get("B12").getNearbyBoxes().put("C13",this.getSectors().get("C13"));
		
		this.getSectors().get("B14").getNearbyBoxes().put("A14",this.getSectors().get("A14"));
		this.getSectors().get("B14").getNearbyBoxes().put("Z04",this.getSectors().get("Z04"));
		this.getSectors().get("B14").getNearbyBoxes().put("C14",this.getSectors().get("C14"));
		
		//adiacenti C		
		this.getSectors().get("C01").getNearbyBoxes().put("B01",this.getSectors().get("B01"));
		this.getSectors().get("C01").getNearbyBoxes().put("C02",this.getSectors().get("C02"));
		
		this.getSectors().get("C02").getNearbyBoxes().put("C01",this.getSectors().get("C01"));
		this.getSectors().get("C02").getNearbyBoxes().put("B01",this.getSectors().get("B01"));
		this.getSectors().get("C02").getNearbyBoxes().put("Z01",this.getSectors().get("Z01"));
		this.getSectors().get("C02").getNearbyBoxes().put("C03",this.getSectors().get("C03"));
		this.getSectors().get("C02").getNearbyBoxes().put("D02",this.getSectors().get("D02"));
		
		this.getSectors().get("C03").getNearbyBoxes().put("C02",this.getSectors().get("C02"));
		this.getSectors().get("C03").getNearbyBoxes().put("Z01",this.getSectors().get("Z01"));
		this.getSectors().get("C03").getNearbyBoxes().put("B03",this.getSectors().get("B03"));
		this.getSectors().get("C03").getNearbyBoxes().put("C04",this.getSectors().get("C04"));
		this.getSectors().get("C03").getNearbyBoxes().put("D02",this.getSectors().get("D02"));
		this.getSectors().get("C03").getNearbyBoxes().put("D03",this.getSectors().get("D03"));
		
		this.getSectors().get("C04").getNearbyBoxes().put("B03",this.getSectors().get("B03"));
		this.getSectors().get("C04").getNearbyBoxes().put("B04",this.getSectors().get("B04"));
		this.getSectors().get("C04").getNearbyBoxes().put("C03",this.getSectors().get("C03"));
		this.getSectors().get("C04").getNearbyBoxes().put("C05",this.getSectors().get("C05"));
		this.getSectors().get("C04").getNearbyBoxes().put("D03",this.getSectors().get("D03"));
		
		this.getSectors().get("C05").getNearbyBoxes().put("B04",this.getSectors().get("B04"));
		this.getSectors().get("C05").getNearbyBoxes().put("B05",this.getSectors().get("B05"));
		this.getSectors().get("C05").getNearbyBoxes().put("C04",this.getSectors().get("C04"));
		this.getSectors().get("C05").getNearbyBoxes().put("C06",this.getSectors().get("C06"));
		this.getSectors().get("C05").getNearbyBoxes().put("D05",this.getSectors().get("D05"));
		
		this.getSectors().get("C06").getNearbyBoxes().put("B05",this.getSectors().get("B05"));
		this.getSectors().get("C06").getNearbyBoxes().put("B06",this.getSectors().get("B06"));
		this.getSectors().get("C06").getNearbyBoxes().put("C05",this.getSectors().get("C05"));
		this.getSectors().get("C06").getNearbyBoxes().put("C07",this.getSectors().get("C07"));
		this.getSectors().get("C06").getNearbyBoxes().put("D05",this.getSectors().get("D05"));
		
		this.getSectors().get("C07").getNearbyBoxes().put("B06",this.getSectors().get("B06"));
		this.getSectors().get("C07").getNearbyBoxes().put("C06",this.getSectors().get("C06"));
		this.getSectors().get("C07").getNearbyBoxes().put("C08",this.getSectors().get("C08"));
				
		this.getSectors().get("C08").getNearbyBoxes().put("B08",this.getSectors().get("B08"));
		this.getSectors().get("C08").getNearbyBoxes().put("C07",this.getSectors().get("C07"));
		this.getSectors().get("C08").getNearbyBoxes().put("C09",this.getSectors().get("C09"));
		this.getSectors().get("C08").getNearbyBoxes().put("D08",this.getSectors().get("D08"));
				
		this.getSectors().get("C09").getNearbyBoxes().put("B08",this.getSectors().get("B08"));
		this.getSectors().get("C09").getNearbyBoxes().put("B09",this.getSectors().get("B09"));
		this.getSectors().get("C09").getNearbyBoxes().put("C08",this.getSectors().get("C08"));
		this.getSectors().get("C09").getNearbyBoxes().put("C10",this.getSectors().get("C10"));
		this.getSectors().get("C09").getNearbyBoxes().put("D08",this.getSectors().get("D08"));
		this.getSectors().get("C09").getNearbyBoxes().put("D09",this.getSectors().get("D09"));
		
		this.getSectors().get("C10").getNearbyBoxes().put("B09",this.getSectors().get("B09"));
		this.getSectors().get("C10").getNearbyBoxes().put("B10",this.getSectors().get("B10"));
		this.getSectors().get("C10").getNearbyBoxes().put("C09",this.getSectors().get("C09"));
		this.getSectors().get("C10").getNearbyBoxes().put("C11",this.getSectors().get("C11"));
		this.getSectors().get("C10").getNearbyBoxes().put("D09",this.getSectors().get("D09"));
		this.getSectors().get("C10").getNearbyBoxes().put("D10",this.getSectors().get("D10"));
		
		this.getSectors().get("C11").getNearbyBoxes().put("B10",this.getSectors().get("B10"));
		this.getSectors().get("C11").getNearbyBoxes().put("B11",this.getSectors().get("B11"));
		this.getSectors().get("C11").getNearbyBoxes().put("C10",this.getSectors().get("C10"));
		this.getSectors().get("C11").getNearbyBoxes().put("C12",this.getSectors().get("C12"));
		this.getSectors().get("C11").getNearbyBoxes().put("D10",this.getSectors().get("D10"));
		this.getSectors().get("C11").getNearbyBoxes().put("D11",this.getSectors().get("D11"));
		
		this.getSectors().get("C12").getNearbyBoxes().put("B11",this.getSectors().get("B11"));
		this.getSectors().get("C12").getNearbyBoxes().put("B12",this.getSectors().get("B12"));
		this.getSectors().get("C12").getNearbyBoxes().put("C11",this.getSectors().get("C11"));
		this.getSectors().get("C12").getNearbyBoxes().put("C13",this.getSectors().get("C13"));
		this.getSectors().get("C12").getNearbyBoxes().put("D11",this.getSectors().get("D11"));
		this.getSectors().get("C12").getNearbyBoxes().put("D12",this.getSectors().get("D12"));
		
		this.getSectors().get("C13").getNearbyBoxes().put("B12",this.getSectors().get("B12"));
		this.getSectors().get("C13").getNearbyBoxes().put("Z04",this.getSectors().get("Z04"));
		this.getSectors().get("C13").getNearbyBoxes().put("C12",this.getSectors().get("C12"));
		this.getSectors().get("C13").getNearbyBoxes().put("C14",this.getSectors().get("C14"));
		this.getSectors().get("C13").getNearbyBoxes().put("D12",this.getSectors().get("D12"));
		this.getSectors().get("C13").getNearbyBoxes().put("D13",this.getSectors().get("D13"));
		
		this.getSectors().get("C14").getNearbyBoxes().put("Z04",this.getSectors().get("Z04"));
		this.getSectors().get("C14").getNearbyBoxes().put("B14",this.getSectors().get("B14"));
		this.getSectors().get("C14").getNearbyBoxes().put("C13",this.getSectors().get("C13"));
		this.getSectors().get("C14").getNearbyBoxes().put("D13",this.getSectors().get("D13"));
		this.getSectors().get("C14").getNearbyBoxes().put("D14",this.getSectors().get("D14"));
		
		//adiacenti D
		this.getSectors().get("D02").getNearbyBoxes().put("C02",this.getSectors().get("C02"));
		this.getSectors().get("D02").getNearbyBoxes().put("C03",this.getSectors().get("C03"));
		this.getSectors().get("D02").getNearbyBoxes().put("D03",this.getSectors().get("D03"));
		this.getSectors().get("D02").getNearbyBoxes().put("E02",this.getSectors().get("E02"));
		this.getSectors().get("D02").getNearbyBoxes().put("E03",this.getSectors().get("E03"));
		
		this.getSectors().get("D03").getNearbyBoxes().put("C03",this.getSectors().get("C03"));
		this.getSectors().get("D03").getNearbyBoxes().put("C04",this.getSectors().get("C04"));
		this.getSectors().get("D03").getNearbyBoxes().put("D02",this.getSectors().get("D02"));
		this.getSectors().get("D03").getNearbyBoxes().put("E03",this.getSectors().get("E03"));
		this.getSectors().get("D03").getNearbyBoxes().put("E04",this.getSectors().get("E04"));
		
		this.getSectors().get("D05").getNearbyBoxes().put("C05",this.getSectors().get("C05"));
		this.getSectors().get("D05").getNearbyBoxes().put("C06",this.getSectors().get("C06"));
		this.getSectors().get("D05").getNearbyBoxes().put("E05",this.getSectors().get("E05"));
		this.getSectors().get("D05").getNearbyBoxes().put("E06",this.getSectors().get("E06"));
		
		this.getSectors().get("D08").getNearbyBoxes().put("C08",this.getSectors().get("C08"));
		this.getSectors().get("D08").getNearbyBoxes().put("C09",this.getSectors().get("C09"));
		this.getSectors().get("D08").getNearbyBoxes().put("D09",this.getSectors().get("D09"));
		this.getSectors().get("D08").getNearbyBoxes().put("E08",this.getSectors().get("E08"));
		this.getSectors().get("D08").getNearbyBoxes().put("E09",this.getSectors().get("E09"));
		
		this.getSectors().get("D09").getNearbyBoxes().put("C09",this.getSectors().get("C09"));
		this.getSectors().get("D09").getNearbyBoxes().put("C10",this.getSectors().get("C10"));
		this.getSectors().get("D09").getNearbyBoxes().put("D08",this.getSectors().get("D08"));
		this.getSectors().get("D09").getNearbyBoxes().put("D10",this.getSectors().get("D10"));
		this.getSectors().get("D09").getNearbyBoxes().put("E09",this.getSectors().get("E09"));
		this.getSectors().get("D09").getNearbyBoxes().put("E10",this.getSectors().get("E10"));
		
		this.getSectors().get("D10").getNearbyBoxes().put("C10",this.getSectors().get("C10"));
		this.getSectors().get("D10").getNearbyBoxes().put("C11",this.getSectors().get("C11"));
		this.getSectors().get("D10").getNearbyBoxes().put("D09",this.getSectors().get("D09"));
		this.getSectors().get("D10").getNearbyBoxes().put("D11",this.getSectors().get("D11"));
		this.getSectors().get("D10").getNearbyBoxes().put("E10",this.getSectors().get("E10"));
		this.getSectors().get("D10").getNearbyBoxes().put("E11",this.getSectors().get("E11"));
		
		this.getSectors().get("D11").getNearbyBoxes().put("C11",this.getSectors().get("C11"));
		this.getSectors().get("D11").getNearbyBoxes().put("C12",this.getSectors().get("C12"));
		this.getSectors().get("D11").getNearbyBoxes().put("D10",this.getSectors().get("D10"));
		this.getSectors().get("D11").getNearbyBoxes().put("D12",this.getSectors().get("D12"));
		this.getSectors().get("D11").getNearbyBoxes().put("E11",this.getSectors().get("E11"));
		this.getSectors().get("D11").getNearbyBoxes().put("E12",this.getSectors().get("E12"));
		
		this.getSectors().get("D12").getNearbyBoxes().put("C12",this.getSectors().get("C12"));
		this.getSectors().get("D12").getNearbyBoxes().put("C13",this.getSectors().get("C13"));
		this.getSectors().get("D12").getNearbyBoxes().put("D11",this.getSectors().get("D11"));
		this.getSectors().get("D12").getNearbyBoxes().put("D13",this.getSectors().get("D13"));
		this.getSectors().get("D12").getNearbyBoxes().put("E12",this.getSectors().get("E12"));
		this.getSectors().get("D12").getNearbyBoxes().put("E13",this.getSectors().get("E13"));
		
		this.getSectors().get("D13").getNearbyBoxes().put("C13",this.getSectors().get("C13"));
		this.getSectors().get("D13").getNearbyBoxes().put("C14",this.getSectors().get("C14"));
		this.getSectors().get("D13").getNearbyBoxes().put("D12",this.getSectors().get("D12"));
		this.getSectors().get("D13").getNearbyBoxes().put("D14",this.getSectors().get("D14"));
		this.getSectors().get("D13").getNearbyBoxes().put("E13",this.getSectors().get("E13"));
		
		this.getSectors().get("D14").getNearbyBoxes().put("C14",this.getSectors().get("C14"));
		this.getSectors().get("D14").getNearbyBoxes().put("D13",this.getSectors().get("D13"));
		
		//adiacenti E
		this.getSectors().get("E02").getNearbyBoxes().put("D02",this.getSectors().get("D02"));
		this.getSectors().get("E02").getNearbyBoxes().put("E03",this.getSectors().get("E03"));
		this.getSectors().get("E02").getNearbyBoxes().put("F01",this.getSectors().get("F01"));
		this.getSectors().get("E02").getNearbyBoxes().put("F02",this.getSectors().get("F02"));
		
		this.getSectors().get("E03").getNearbyBoxes().put("D02",this.getSectors().get("D02"));
		this.getSectors().get("E03").getNearbyBoxes().put("D03",this.getSectors().get("D03"));
		this.getSectors().get("E03").getNearbyBoxes().put("E02",this.getSectors().get("E02"));
		this.getSectors().get("E03").getNearbyBoxes().put("E04",this.getSectors().get("E04"));
		this.getSectors().get("E03").getNearbyBoxes().put("F02",this.getSectors().get("F02"));
		this.getSectors().get("E03").getNearbyBoxes().put("F03",this.getSectors().get("F03"));
		
		this.getSectors().get("E04").getNearbyBoxes().put("D03",this.getSectors().get("D03"));
		this.getSectors().get("E04").getNearbyBoxes().put("E03",this.getSectors().get("E03"));
		this.getSectors().get("E04").getNearbyBoxes().put("E05",this.getSectors().get("E05"));
		this.getSectors().get("E04").getNearbyBoxes().put("F03",this.getSectors().get("F03"));
		this.getSectors().get("E04").getNearbyBoxes().put("F04",this.getSectors().get("F04"));
		
		this.getSectors().get("E05").getNearbyBoxes().put("D05",this.getSectors().get("D05"));
		this.getSectors().get("E05").getNearbyBoxes().put("E04",this.getSectors().get("E04"));
		this.getSectors().get("E05").getNearbyBoxes().put("E06",this.getSectors().get("E06"));
		this.getSectors().get("E05").getNearbyBoxes().put("F04",this.getSectors().get("F04"));
		this.getSectors().get("E05").getNearbyBoxes().put("F05",this.getSectors().get("F05"));
		
		this.getSectors().get("E06").getNearbyBoxes().put("D05",this.getSectors().get("D05"));
		this.getSectors().get("E06").getNearbyBoxes().put("E05",this.getSectors().get("E05"));
		this.getSectors().get("E06").getNearbyBoxes().put("F05",this.getSectors().get("F05"));
		this.getSectors().get("E06").getNearbyBoxes().put("F06",this.getSectors().get("F06"));
		
		this.getSectors().get("E08").getNearbyBoxes().put("D08",this.getSectors().get("D08"));
		this.getSectors().get("E08").getNearbyBoxes().put("E09",this.getSectors().get("E09"));
		this.getSectors().get("E08").getNearbyBoxes().put("F07",this.getSectors().get("F07"));
		this.getSectors().get("E08").getNearbyBoxes().put("F08",this.getSectors().get("F08"));
		
		this.getSectors().get("E09").getNearbyBoxes().put("D08",this.getSectors().get("D08"));
		this.getSectors().get("E09").getNearbyBoxes().put("D09",this.getSectors().get("D09"));
		this.getSectors().get("E09").getNearbyBoxes().put("E08",this.getSectors().get("E08"));
		this.getSectors().get("E09").getNearbyBoxes().put("E10",this.getSectors().get("E10"));
		this.getSectors().get("E09").getNearbyBoxes().put("F08",this.getSectors().get("F08"));
		this.getSectors().get("E09").getNearbyBoxes().put("F09",this.getSectors().get("F09"));
		
		this.getSectors().get("E10").getNearbyBoxes().put("D09",this.getSectors().get("D09"));
		this.getSectors().get("E10").getNearbyBoxes().put("D10",this.getSectors().get("D10"));
		this.getSectors().get("E10").getNearbyBoxes().put("E09",this.getSectors().get("E09"));
		this.getSectors().get("E10").getNearbyBoxes().put("E11",this.getSectors().get("E11"));
		this.getSectors().get("E10").getNearbyBoxes().put("F09",this.getSectors().get("F09"));
		this.getSectors().get("E10").getNearbyBoxes().put("F10",this.getSectors().get("F10"));
		
		this.getSectors().get("E11").getNearbyBoxes().put("D10",this.getSectors().get("D10"));
		this.getSectors().get("E11").getNearbyBoxes().put("D11",this.getSectors().get("D11"));
		this.getSectors().get("E11").getNearbyBoxes().put("E10",this.getSectors().get("E10"));
		this.getSectors().get("E11").getNearbyBoxes().put("E12",this.getSectors().get("E12"));
		this.getSectors().get("E11").getNearbyBoxes().put("F10",this.getSectors().get("F10"));
		this.getSectors().get("E11").getNearbyBoxes().put("F11",this.getSectors().get("F11"));
		
		this.getSectors().get("E12").getNearbyBoxes().put("D11",this.getSectors().get("D11"));
		this.getSectors().get("E12").getNearbyBoxes().put("D12",this.getSectors().get("D12"));
		this.getSectors().get("E12").getNearbyBoxes().put("E11",this.getSectors().get("E11"));
		this.getSectors().get("E12").getNearbyBoxes().put("E13",this.getSectors().get("E13"));
		this.getSectors().get("E12").getNearbyBoxes().put("F11",this.getSectors().get("F11"));

		this.getSectors().get("E13").getNearbyBoxes().put("D12",this.getSectors().get("D12"));
		this.getSectors().get("E13").getNearbyBoxes().put("D13",this.getSectors().get("D13"));
		this.getSectors().get("E13").getNearbyBoxes().put("E12",this.getSectors().get("E12"));

		//adiacenti F		
		this.getSectors().get("F01").getNearbyBoxes().put("E02",this.getSectors().get("E02"));
		this.getSectors().get("F01").getNearbyBoxes().put("F02",this.getSectors().get("F02"));
		this.getSectors().get("F01").getNearbyBoxes().put("G01",this.getSectors().get("G01"));
		this.getSectors().get("F01").getNearbyBoxes().put("G02",this.getSectors().get("G02"));
		
		this.getSectors().get("F02").getNearbyBoxes().put("E02",this.getSectors().get("E02"));
		this.getSectors().get("F02").getNearbyBoxes().put("E03",this.getSectors().get("E03"));
		this.getSectors().get("F02").getNearbyBoxes().put("F01",this.getSectors().get("F01"));
		this.getSectors().get("F02").getNearbyBoxes().put("F03",this.getSectors().get("F03"));
		this.getSectors().get("F02").getNearbyBoxes().put("G02",this.getSectors().get("G02"));
		this.getSectors().get("F02").getNearbyBoxes().put("G03",this.getSectors().get("G03"));
		
		this.getSectors().get("F03").getNearbyBoxes().put("E03",this.getSectors().get("E03"));
		this.getSectors().get("F03").getNearbyBoxes().put("E04",this.getSectors().get("E04"));
		this.getSectors().get("F03").getNearbyBoxes().put("F02",this.getSectors().get("F02"));
		this.getSectors().get("F03").getNearbyBoxes().put("F04",this.getSectors().get("F04"));
		this.getSectors().get("F03").getNearbyBoxes().put("G03",this.getSectors().get("G03"));
		this.getSectors().get("F03").getNearbyBoxes().put("G04",this.getSectors().get("G04"));
		
		this.getSectors().get("F04").getNearbyBoxes().put("E04",this.getSectors().get("E04"));
		this.getSectors().get("F04").getNearbyBoxes().put("E05",this.getSectors().get("E05"));
		this.getSectors().get("F04").getNearbyBoxes().put("F03",this.getSectors().get("F03"));
		this.getSectors().get("F04").getNearbyBoxes().put("F05",this.getSectors().get("F05"));
		this.getSectors().get("F04").getNearbyBoxes().put("G04",this.getSectors().get("G04"));
		this.getSectors().get("F04").getNearbyBoxes().put("G05",this.getSectors().get("G05"));
		
		this.getSectors().get("F05").getNearbyBoxes().put("E05",this.getSectors().get("E05"));
		this.getSectors().get("F05").getNearbyBoxes().put("E06",this.getSectors().get("E06"));
		this.getSectors().get("F05").getNearbyBoxes().put("F04",this.getSectors().get("F04"));
		this.getSectors().get("F05").getNearbyBoxes().put("F06",this.getSectors().get("F06"));
		this.getSectors().get("F05").getNearbyBoxes().put("G05",this.getSectors().get("G05"));
		this.getSectors().get("F05").getNearbyBoxes().put("G06",this.getSectors().get("G06"));
		
		this.getSectors().get("F06").getNearbyBoxes().put("E06",this.getSectors().get("E06"));
		this.getSectors().get("F06").getNearbyBoxes().put("F05",this.getSectors().get("F05"));
		this.getSectors().get("F06").getNearbyBoxes().put("F07",this.getSectors().get("F07"));
		this.getSectors().get("F06").getNearbyBoxes().put("G06",this.getSectors().get("G06"));
		this.getSectors().get("F06").getNearbyBoxes().put("G07",this.getSectors().get("G07"));
		
		this.getSectors().get("F07").getNearbyBoxes().put("E08",this.getSectors().get("E08"));
		this.getSectors().get("F07").getNearbyBoxes().put("F06",this.getSectors().get("F06"));
		this.getSectors().get("F07").getNearbyBoxes().put("F08",this.getSectors().get("F08"));
		this.getSectors().get("F07").getNearbyBoxes().put("G07",this.getSectors().get("G07"));
		this.getSectors().get("F07").getNearbyBoxes().put("G08",this.getSectors().get("G08"));
		
		this.getSectors().get("F08").getNearbyBoxes().put("E08",this.getSectors().get("E08"));
		this.getSectors().get("F08").getNearbyBoxes().put("E09",this.getSectors().get("E09"));
		this.getSectors().get("F08").getNearbyBoxes().put("F07",this.getSectors().get("F07"));
		this.getSectors().get("F08").getNearbyBoxes().put("F09",this.getSectors().get("F09"));
		this.getSectors().get("F08").getNearbyBoxes().put("G08",this.getSectors().get("G08"));
		this.getSectors().get("F08").getNearbyBoxes().put("G09",this.getSectors().get("G09"));
		
		this.getSectors().get("F09").getNearbyBoxes().put("E09",this.getSectors().get("E09"));
		this.getSectors().get("F09").getNearbyBoxes().put("E10",this.getSectors().get("E10"));
		this.getSectors().get("F09").getNearbyBoxes().put("F08",this.getSectors().get("F08"));
		this.getSectors().get("F09").getNearbyBoxes().put("F10",this.getSectors().get("F10"));
		this.getSectors().get("F09").getNearbyBoxes().put("G09",this.getSectors().get("G09"));
		this.getSectors().get("F09").getNearbyBoxes().put("G10",this.getSectors().get("G10"));
		
		this.getSectors().get("F10").getNearbyBoxes().put("E10",this.getSectors().get("E10"));
		this.getSectors().get("F10").getNearbyBoxes().put("E11",this.getSectors().get("E11"));
		this.getSectors().get("F10").getNearbyBoxes().put("F09",this.getSectors().get("F09"));
		this.getSectors().get("F10").getNearbyBoxes().put("F11",this.getSectors().get("F11"));
		this.getSectors().get("F10").getNearbyBoxes().put("G10",this.getSectors().get("G10"));
		this.getSectors().get("F10").getNearbyBoxes().put("G11",this.getSectors().get("G11"));
		
		this.getSectors().get("F11").getNearbyBoxes().put("E11",this.getSectors().get("E11"));
		this.getSectors().get("F11").getNearbyBoxes().put("E12",this.getSectors().get("E12"));
		this.getSectors().get("F11").getNearbyBoxes().put("F10",this.getSectors().get("F10"));
		this.getSectors().get("F11").getNearbyBoxes().put("G11",this.getSectors().get("G11"));
		this.getSectors().get("F11").getNearbyBoxes().put("G12",this.getSectors().get("G12"));
		
		//adiacenti G
		this.getSectors().get("G01").getNearbyBoxes().put("F01",this.getSectors().get("F01"));
		this.getSectors().get("G01").getNearbyBoxes().put("G02",this.getSectors().get("G02"));
		this.getSectors().get("G01").getNearbyBoxes().put("H01",this.getSectors().get("H01"));
		
		this.getSectors().get("G02").getNearbyBoxes().put("F01",this.getSectors().get("F01"));
		this.getSectors().get("G02").getNearbyBoxes().put("F02",this.getSectors().get("F02"));
		this.getSectors().get("G02").getNearbyBoxes().put("G01",this.getSectors().get("G01"));
		this.getSectors().get("G02").getNearbyBoxes().put("G03",this.getSectors().get("G03"));
		this.getSectors().get("G02").getNearbyBoxes().put("H01",this.getSectors().get("H01"));
		this.getSectors().get("G02").getNearbyBoxes().put("H02",this.getSectors().get("H02"));
		
		this.getSectors().get("G03").getNearbyBoxes().put("F02",this.getSectors().get("F02"));
		this.getSectors().get("G03").getNearbyBoxes().put("F03",this.getSectors().get("F03"));
		this.getSectors().get("G03").getNearbyBoxes().put("G02",this.getSectors().get("G02"));
		this.getSectors().get("G03").getNearbyBoxes().put("G04",this.getSectors().get("G04"));
		this.getSectors().get("G03").getNearbyBoxes().put("H02",this.getSectors().get("H02"));
		this.getSectors().get("G03").getNearbyBoxes().put("H03",this.getSectors().get("H03"));
		
		this.getSectors().get("G04").getNearbyBoxes().put("F03",this.getSectors().get("F03"));
		this.getSectors().get("G04").getNearbyBoxes().put("F04",this.getSectors().get("F04"));
		this.getSectors().get("G04").getNearbyBoxes().put("G03",this.getSectors().get("G03"));
		this.getSectors().get("G04").getNearbyBoxes().put("G05",this.getSectors().get("G05"));
		this.getSectors().get("G04").getNearbyBoxes().put("H03",this.getSectors().get("H03"));
		this.getSectors().get("G04").getNearbyBoxes().put("H04",this.getSectors().get("H04"));
		
		this.getSectors().get("G05").getNearbyBoxes().put("F04",this.getSectors().get("F04"));
		this.getSectors().get("G05").getNearbyBoxes().put("F05",this.getSectors().get("F05"));
		this.getSectors().get("G05").getNearbyBoxes().put("G04",this.getSectors().get("G04"));
		this.getSectors().get("G05").getNearbyBoxes().put("G06",this.getSectors().get("G06"));
		this.getSectors().get("G05").getNearbyBoxes().put("H04",this.getSectors().get("H04"));

		this.getSectors().get("G06").getNearbyBoxes().put("F05",this.getSectors().get("F05"));
		this.getSectors().get("G06").getNearbyBoxes().put("F06",this.getSectors().get("F06"));
		this.getSectors().get("G06").getNearbyBoxes().put("G05",this.getSectors().get("G05"));
		this.getSectors().get("G06").getNearbyBoxes().put("G07",this.getSectors().get("G07"));
		this.getSectors().get("G06").getNearbyBoxes().put("H06",this.getSectors().get("H06"));
		
		this.getSectors().get("G07").getNearbyBoxes().put("F06",this.getSectors().get("F06"));
		this.getSectors().get("G07").getNearbyBoxes().put("F07",this.getSectors().get("F07"));
		this.getSectors().get("G07").getNearbyBoxes().put("G06",this.getSectors().get("G06"));
		this.getSectors().get("G07").getNearbyBoxes().put("G08",this.getSectors().get("G08"));
		this.getSectors().get("G07").getNearbyBoxes().put("H06",this.getSectors().get("H06"));
		this.getSectors().get("G07").getNearbyBoxes().put("H07",this.getSectors().get("H07"));
		
		this.getSectors().get("G08").getNearbyBoxes().put("F07",this.getSectors().get("F07"));
		this.getSectors().get("G08").getNearbyBoxes().put("F08",this.getSectors().get("F08"));
		this.getSectors().get("G08").getNearbyBoxes().put("G07",this.getSectors().get("G07"));
		this.getSectors().get("G08").getNearbyBoxes().put("G09",this.getSectors().get("G09"));
		this.getSectors().get("G08").getNearbyBoxes().put("H07",this.getSectors().get("H07"));
		this.getSectors().get("G08").getNearbyBoxes().put("H08",this.getSectors().get("H08"));
		
		this.getSectors().get("G09").getNearbyBoxes().put("F08",this.getSectors().get("F08"));
		this.getSectors().get("G09").getNearbyBoxes().put("F09",this.getSectors().get("F09"));
		this.getSectors().get("G09").getNearbyBoxes().put("G08",this.getSectors().get("G08"));
		this.getSectors().get("G09").getNearbyBoxes().put("G10",this.getSectors().get("G10"));
		this.getSectors().get("G09").getNearbyBoxes().put("H08",this.getSectors().get("H08"));
		this.getSectors().get("G09").getNearbyBoxes().put("H09",this.getSectors().get("H09"));
		
		this.getSectors().get("G10").getNearbyBoxes().put("F09",this.getSectors().get("F09"));
		this.getSectors().get("G10").getNearbyBoxes().put("F10",this.getSectors().get("F10"));
		this.getSectors().get("G10").getNearbyBoxes().put("G09",this.getSectors().get("G09"));
		this.getSectors().get("G10").getNearbyBoxes().put("G11",this.getSectors().get("G11"));
		this.getSectors().get("G10").getNearbyBoxes().put("H09",this.getSectors().get("H09"));

		this.getSectors().get("G11").getNearbyBoxes().put("F10",this.getSectors().get("F10"));
		this.getSectors().get("G11").getNearbyBoxes().put("F11",this.getSectors().get("F11"));
		this.getSectors().get("G11").getNearbyBoxes().put("G10",this.getSectors().get("G10"));
		this.getSectors().get("G11").getNearbyBoxes().put("G12",this.getSectors().get("G12"));

		this.getSectors().get("G12").getNearbyBoxes().put("F11",this.getSectors().get("F11"));
		this.getSectors().get("G12").getNearbyBoxes().put("G11",this.getSectors().get("G11"));
		this.getSectors().get("G12").getNearbyBoxes().put("G13",this.getSectors().get("G13"));
		this.getSectors().get("G12").getNearbyBoxes().put("H12",this.getSectors().get("H12"));
		
		this.getSectors().get("G13").getNearbyBoxes().put("G12",this.getSectors().get("G12"));
		this.getSectors().get("G13").getNearbyBoxes().put("G14",this.getSectors().get("G14"));
		this.getSectors().get("G13").getNearbyBoxes().put("H12",this.getSectors().get("H12"));
		this.getSectors().get("G13").getNearbyBoxes().put("H13",this.getSectors().get("H13"));

		this.getSectors().get("G14").getNearbyBoxes().put("G13",this.getSectors().get("G13"));
		this.getSectors().get("G14").getNearbyBoxes().put("H13",this.getSectors().get("H13"));
		this.getSectors().get("G14").getNearbyBoxes().put("H14",this.getSectors().get("H14"));

		
		//adiacenti H
		this.getSectors().get("H01").getNearbyBoxes().put("G01",this.getSectors().get("G01"));
		this.getSectors().get("H01").getNearbyBoxes().put("G02",this.getSectors().get("G02"));
		this.getSectors().get("H01").getNearbyBoxes().put("H02",this.getSectors().get("H02"));
		this.getSectors().get("H01").getNearbyBoxes().put("I01",this.getSectors().get("I01"));
		this.getSectors().get("H01").getNearbyBoxes().put("I02",this.getSectors().get("I02"));
		
		this.getSectors().get("H02").getNearbyBoxes().put("G02",this.getSectors().get("G02"));
		this.getSectors().get("H02").getNearbyBoxes().put("G03",this.getSectors().get("G03"));
		this.getSectors().get("H02").getNearbyBoxes().put("H01",this.getSectors().get("H01"));
		this.getSectors().get("H02").getNearbyBoxes().put("H03",this.getSectors().get("H03"));
		this.getSectors().get("H02").getNearbyBoxes().put("I02",this.getSectors().get("I02"));
		this.getSectors().get("H02").getNearbyBoxes().put("I03",this.getSectors().get("I03"));
		
		this.getSectors().get("H03").getNearbyBoxes().put("G03",this.getSectors().get("G03"));
		this.getSectors().get("H03").getNearbyBoxes().put("G04",this.getSectors().get("G04"));
		this.getSectors().get("H03").getNearbyBoxes().put("H02",this.getSectors().get("H02"));
		this.getSectors().get("H03").getNearbyBoxes().put("H04",this.getSectors().get("H04"));
		this.getSectors().get("H03").getNearbyBoxes().put("I03",this.getSectors().get("I03"));
		this.getSectors().get("H03").getNearbyBoxes().put("I04",this.getSectors().get("I04"));
		
		this.getSectors().get("H04").getNearbyBoxes().put("G04",this.getSectors().get("G04"));
		this.getSectors().get("H04").getNearbyBoxes().put("G05",this.getSectors().get("G05"));
		this.getSectors().get("H04").getNearbyBoxes().put("H03",this.getSectors().get("H03"));
		this.getSectors().get("H04").getNearbyBoxes().put("I04",this.getSectors().get("I04"));
		this.getSectors().get("H04").getNearbyBoxes().put("I05",this.getSectors().get("I05"));
		
		this.getSectors().get("H06").getNearbyBoxes().put("G06",this.getSectors().get("G06"));
		this.getSectors().get("H06").getNearbyBoxes().put("G07",this.getSectors().get("G07"));
		this.getSectors().get("H06").getNearbyBoxes().put("H07",this.getSectors().get("H07"));
		this.getSectors().get("H06").getNearbyBoxes().put("I07",this.getSectors().get("I07"));
		
		this.getSectors().get("H07").getNearbyBoxes().put("G07",this.getSectors().get("G07"));
		this.getSectors().get("H07").getNearbyBoxes().put("G08",this.getSectors().get("G08"));
		this.getSectors().get("H07").getNearbyBoxes().put("H06",this.getSectors().get("H06"));
		this.getSectors().get("H07").getNearbyBoxes().put("H08",this.getSectors().get("H08"));
		this.getSectors().get("H07").getNearbyBoxes().put("I07",this.getSectors().get("I07"));
		this.getSectors().get("H07").getNearbyBoxes().put("I08",this.getSectors().get("I08"));
		
		this.getSectors().get("H08").getNearbyBoxes().put("G08",this.getSectors().get("G08"));
		this.getSectors().get("H08").getNearbyBoxes().put("G09",this.getSectors().get("G09"));
		this.getSectors().get("H08").getNearbyBoxes().put("H07",this.getSectors().get("H07"));
		this.getSectors().get("H08").getNearbyBoxes().put("H09",this.getSectors().get("H09"));
		this.getSectors().get("H08").getNearbyBoxes().put("I08",this.getSectors().get("I08"));
		this.getSectors().get("H08").getNearbyBoxes().put("I09",this.getSectors().get("I09"));
		
		this.getSectors().get("H09").getNearbyBoxes().put("G09",this.getSectors().get("G09"));
		this.getSectors().get("H09").getNearbyBoxes().put("G10",this.getSectors().get("G10"));
		this.getSectors().get("H09").getNearbyBoxes().put("H08",this.getSectors().get("H08"));
		this.getSectors().get("H09").getNearbyBoxes().put("I09",this.getSectors().get("I09"));
		this.getSectors().get("H09").getNearbyBoxes().put("I10",this.getSectors().get("I10"));
		
		this.getSectors().get("H12").getNearbyBoxes().put("G12",this.getSectors().get("G12"));
		this.getSectors().get("H12").getNearbyBoxes().put("G13",this.getSectors().get("G13"));
		this.getSectors().get("H12").getNearbyBoxes().put("H13",this.getSectors().get("H13"));
		this.getSectors().get("H12").getNearbyBoxes().put("I13",this.getSectors().get("I13"));
		
		this.getSectors().get("H13").getNearbyBoxes().put("G13",this.getSectors().get("G13"));
		this.getSectors().get("H13").getNearbyBoxes().put("G14",this.getSectors().get("G14"));
		this.getSectors().get("H13").getNearbyBoxes().put("H12",this.getSectors().get("H12"));
		this.getSectors().get("H13").getNearbyBoxes().put("H14",this.getSectors().get("H14"));
		this.getSectors().get("H13").getNearbyBoxes().put("I13",this.getSectors().get("I13"));
		this.getSectors().get("H13").getNearbyBoxes().put("I14",this.getSectors().get("I14"));
		
		this.getSectors().get("H14").getNearbyBoxes().put("G14",this.getSectors().get("G14"));
		this.getSectors().get("H14").getNearbyBoxes().put("H13",this.getSectors().get("H13"));
		this.getSectors().get("H14").getNearbyBoxes().put("I14",this.getSectors().get("I14"));
		
		//adiacenti I 
		this.getSectors().get("I01").getNearbyBoxes().put("H01",this.getSectors().get("H01"));
		this.getSectors().get("I01").getNearbyBoxes().put("I02",this.getSectors().get("I02"));
		this.getSectors().get("I01").getNearbyBoxes().put("J01",this.getSectors().get("J01"));
		
		this.getSectors().get("I02").getNearbyBoxes().put("H01",this.getSectors().get("H01"));
		this.getSectors().get("I02").getNearbyBoxes().put("H02",this.getSectors().get("H02"));
		this.getSectors().get("I02").getNearbyBoxes().put("I01",this.getSectors().get("I01"));
		this.getSectors().get("I02").getNearbyBoxes().put("I03",this.getSectors().get("I03"));
		this.getSectors().get("I02").getNearbyBoxes().put("J01",this.getSectors().get("J01"));
		this.getSectors().get("I02").getNearbyBoxes().put("J02",this.getSectors().get("J02"));
		
		this.getSectors().get("I03").getNearbyBoxes().put("H02",this.getSectors().get("H02"));
		this.getSectors().get("I03").getNearbyBoxes().put("H03",this.getSectors().get("H03"));
		this.getSectors().get("I03").getNearbyBoxes().put("I02",this.getSectors().get("I02"));
		this.getSectors().get("I03").getNearbyBoxes().put("I04",this.getSectors().get("I04"));
		this.getSectors().get("I03").getNearbyBoxes().put("J02",this.getSectors().get("J02"));
		this.getSectors().get("I03").getNearbyBoxes().put("J03",this.getSectors().get("J03"));
		
		this.getSectors().get("I04").getNearbyBoxes().put("H03",this.getSectors().get("H03"));
		this.getSectors().get("I04").getNearbyBoxes().put("H04",this.getSectors().get("H04"));
		this.getSectors().get("I04").getNearbyBoxes().put("I03",this.getSectors().get("I03"));
		this.getSectors().get("I04").getNearbyBoxes().put("I05",this.getSectors().get("I05"));
		this.getSectors().get("I04").getNearbyBoxes().put("J03",this.getSectors().get("J03"));
		this.getSectors().get("I04").getNearbyBoxes().put("J04",this.getSectors().get("J04"));
		
		this.getSectors().get("I05").getNearbyBoxes().put("H04",this.getSectors().get("H04"));
		this.getSectors().get("I05").getNearbyBoxes().put("I04",this.getSectors().get("I04"));
		this.getSectors().get("I05").getNearbyBoxes().put("J04",this.getSectors().get("J04"));
		this.getSectors().get("I05").getNearbyBoxes().put("J05",this.getSectors().get("J05"));
		
		this.getSectors().get("I07").getNearbyBoxes().put("H06",this.getSectors().get("H06"));
		this.getSectors().get("I07").getNearbyBoxes().put("H07",this.getSectors().get("H07"));
		this.getSectors().get("I07").getNearbyBoxes().put("I08",this.getSectors().get("I08"));
		this.getSectors().get("I07").getNearbyBoxes().put("J06",this.getSectors().get("J06"));

		this.getSectors().get("I08").getNearbyBoxes().put("H07",this.getSectors().get("H07"));
		this.getSectors().get("I08").getNearbyBoxes().put("H08",this.getSectors().get("H08"));
		this.getSectors().get("I08").getNearbyBoxes().put("I07",this.getSectors().get("I07"));
		this.getSectors().get("I08").getNearbyBoxes().put("I09",this.getSectors().get("I09"));
		this.getSectors().get("I08").getNearbyBoxes().put("J08",this.getSectors().get("J08"));
		
		this.getSectors().get("I09").getNearbyBoxes().put("H08",this.getSectors().get("H08"));
		this.getSectors().get("I09").getNearbyBoxes().put("H09",this.getSectors().get("H09"));
		this.getSectors().get("I09").getNearbyBoxes().put("I08",this.getSectors().get("I08"));
		this.getSectors().get("I09").getNearbyBoxes().put("I10",this.getSectors().get("I10"));
		this.getSectors().get("I09").getNearbyBoxes().put("J08",this.getSectors().get("J08"));
		this.getSectors().get("I09").getNearbyBoxes().put("J09",this.getSectors().get("J09"));
		
		this.getSectors().get("I10").getNearbyBoxes().put("H09",this.getSectors().get("H09"));
		this.getSectors().get("I10").getNearbyBoxes().put("I09",this.getSectors().get("I09"));
		this.getSectors().get("I10").getNearbyBoxes().put("I11",this.getSectors().get("I11"));
		this.getSectors().get("I10").getNearbyBoxes().put("J09",this.getSectors().get("J09"));
		this.getSectors().get("I10").getNearbyBoxes().put("J10",this.getSectors().get("J10"));
		
		this.getSectors().get("I11").getNearbyBoxes().put("I10",this.getSectors().get("I10"));
		this.getSectors().get("I11").getNearbyBoxes().put("J10",this.getSectors().get("J10"));
		this.getSectors().get("I11").getNearbyBoxes().put("J11",this.getSectors().get("J11"));

		this.getSectors().get("I13").getNearbyBoxes().put("H12",this.getSectors().get("H12"));
		this.getSectors().get("I13").getNearbyBoxes().put("H13",this.getSectors().get("H13"));
		this.getSectors().get("I13").getNearbyBoxes().put("I14",this.getSectors().get("I14"));
		this.getSectors().get("I13").getNearbyBoxes().put("J13",this.getSectors().get("J13"));
		
		this.getSectors().get("I14").getNearbyBoxes().put("H13",this.getSectors().get("H13"));
		this.getSectors().get("I14").getNearbyBoxes().put("H14",this.getSectors().get("H14"));
		this.getSectors().get("I14").getNearbyBoxes().put("I13",this.getSectors().get("I13"));
		this.getSectors().get("I14").getNearbyBoxes().put("J13",this.getSectors().get("J13"));
		this.getSectors().get("I14").getNearbyBoxes().put("J14",this.getSectors().get("J14"));

		//adiacenti J
		this.getSectors().get("J01").getNearbyBoxes().put("I01",this.getSectors().get("I01"));
		this.getSectors().get("J01").getNearbyBoxes().put("I02",this.getSectors().get("I02"));
		this.getSectors().get("J01").getNearbyBoxes().put("J02",this.getSectors().get("J02"));
		this.getSectors().get("J01").getNearbyBoxes().put("K01",this.getSectors().get("K01"));
		this.getSectors().get("J01").getNearbyBoxes().put("K02",this.getSectors().get("K02"));
		
		this.getSectors().get("J02").getNearbyBoxes().put("I02",this.getSectors().get("I02"));
		this.getSectors().get("J02").getNearbyBoxes().put("I03",this.getSectors().get("I03"));
		this.getSectors().get("J02").getNearbyBoxes().put("J01",this.getSectors().get("J01"));
		this.getSectors().get("J02").getNearbyBoxes().put("J03",this.getSectors().get("J03"));
		this.getSectors().get("J02").getNearbyBoxes().put("K02",this.getSectors().get("K02"));
		this.getSectors().get("J02").getNearbyBoxes().put("K03",this.getSectors().get("K03"));
		
		this.getSectors().get("J03").getNearbyBoxes().put("I03",this.getSectors().get("I03"));
		this.getSectors().get("J03").getNearbyBoxes().put("I04",this.getSectors().get("I04"));
		this.getSectors().get("J03").getNearbyBoxes().put("J02",this.getSectors().get("J02"));
		this.getSectors().get("J03").getNearbyBoxes().put("J04",this.getSectors().get("J04"));
		this.getSectors().get("J03").getNearbyBoxes().put("K03",this.getSectors().get("K03"));
		this.getSectors().get("J03").getNearbyBoxes().put("K04",this.getSectors().get("K04"));
		
		this.getSectors().get("J04").getNearbyBoxes().put("I04",this.getSectors().get("I04"));
		this.getSectors().get("J04").getNearbyBoxes().put("I05",this.getSectors().get("I05"));
		this.getSectors().get("J04").getNearbyBoxes().put("J03",this.getSectors().get("J03"));
		this.getSectors().get("J04").getNearbyBoxes().put("J05",this.getSectors().get("J05"));
		this.getSectors().get("J04").getNearbyBoxes().put("K04",this.getSectors().get("K04"));
		this.getSectors().get("J04").getNearbyBoxes().put("K05",this.getSectors().get("K05"));
		
		this.getSectors().get("J05").getNearbyBoxes().put("I05",this.getSectors().get("I05"));
		this.getSectors().get("J05").getNearbyBoxes().put("J04",this.getSectors().get("J04"));
		this.getSectors().get("J05").getNearbyBoxes().put("J06",this.getSectors().get("J06"));
		this.getSectors().get("J05").getNearbyBoxes().put("K05",this.getSectors().get("K05"));
		this.getSectors().get("J05").getNearbyBoxes().put("K06",this.getSectors().get("K06"));
		
		this.getSectors().get("J06").getNearbyBoxes().put("I07",this.getSectors().get("I07"));
		this.getSectors().get("J06").getNearbyBoxes().put("J05",this.getSectors().get("J05"));
		this.getSectors().get("J06").getNearbyBoxes().put("K06",this.getSectors().get("K06"));
		
		this.getSectors().get("J08").getNearbyBoxes().put("I08",this.getSectors().get("I08"));
		this.getSectors().get("J08").getNearbyBoxes().put("I09",this.getSectors().get("I09"));
		this.getSectors().get("J08").getNearbyBoxes().put("J09",this.getSectors().get("J09"));
		this.getSectors().get("J08").getNearbyBoxes().put("K08",this.getSectors().get("K08"));
		this.getSectors().get("J08").getNearbyBoxes().put("K09",this.getSectors().get("K09"));
		
		this.getSectors().get("J09").getNearbyBoxes().put("I09",this.getSectors().get("I09"));
		this.getSectors().get("J09").getNearbyBoxes().put("I10",this.getSectors().get("I10"));
		this.getSectors().get("J09").getNearbyBoxes().put("J08",this.getSectors().get("J08"));
		this.getSectors().get("J09").getNearbyBoxes().put("J10",this.getSectors().get("J10"));
		this.getSectors().get("J09").getNearbyBoxes().put("K09",this.getSectors().get("K09"));
		this.getSectors().get("J09").getNearbyBoxes().put("K10",this.getSectors().get("K10"));
		
		this.getSectors().get("J10").getNearbyBoxes().put("I10",this.getSectors().get("I10"));
		this.getSectors().get("J10").getNearbyBoxes().put("I11",this.getSectors().get("I11"));
		this.getSectors().get("J10").getNearbyBoxes().put("J09",this.getSectors().get("J09"));
		this.getSectors().get("J10").getNearbyBoxes().put("J11",this.getSectors().get("J11"));
		this.getSectors().get("J10").getNearbyBoxes().put("K10",this.getSectors().get("K10"));
		this.getSectors().get("J10").getNearbyBoxes().put("K11",this.getSectors().get("K11"));
		
		this.getSectors().get("J11").getNearbyBoxes().put("I11",this.getSectors().get("I11"));
		this.getSectors().get("J11").getNearbyBoxes().put("J10",this.getSectors().get("J10"));
		this.getSectors().get("J11").getNearbyBoxes().put("K11",this.getSectors().get("K11"));
		this.getSectors().get("J11").getNearbyBoxes().put("K12",this.getSectors().get("K12"));
		
		this.getSectors().get("J13").getNearbyBoxes().put("I13",this.getSectors().get("I13"));
		this.getSectors().get("J13").getNearbyBoxes().put("I14",this.getSectors().get("I14"));
		this.getSectors().get("J13").getNearbyBoxes().put("J14",this.getSectors().get("J14"));
		this.getSectors().get("J13").getNearbyBoxes().put("K14",this.getSectors().get("K14"));
		
		this.getSectors().get("J14").getNearbyBoxes().put("I14",this.getSectors().get("I14"));
		this.getSectors().get("J14").getNearbyBoxes().put("J13",this.getSectors().get("J13"));
		this.getSectors().get("J14").getNearbyBoxes().put("K14",this.getSectors().get("K14"));
		
		//adiacenti K
		this.getSectors().get("K01").getNearbyBoxes().put("J01",this.getSectors().get("J01"));
		this.getSectors().get("K01").getNearbyBoxes().put("K02",this.getSectors().get("K02"));
		this.getSectors().get("K01").getNearbyBoxes().put("L01",this.getSectors().get("L01"));
		
		this.getSectors().get("K02").getNearbyBoxes().put("J01",this.getSectors().get("J01"));
		this.getSectors().get("K02").getNearbyBoxes().put("J02",this.getSectors().get("J02"));
		this.getSectors().get("K02").getNearbyBoxes().put("K01",this.getSectors().get("K01"));
		this.getSectors().get("K02").getNearbyBoxes().put("K03",this.getSectors().get("K03"));
		this.getSectors().get("K02").getNearbyBoxes().put("L01",this.getSectors().get("L01"));
		this.getSectors().get("K02").getNearbyBoxes().put("L02",this.getSectors().get("L02"));
		
		this.getSectors().get("K03").getNearbyBoxes().put("J02",this.getSectors().get("J02"));
		this.getSectors().get("K03").getNearbyBoxes().put("J03",this.getSectors().get("J03"));
		this.getSectors().get("K03").getNearbyBoxes().put("K02",this.getSectors().get("K02"));
		this.getSectors().get("K03").getNearbyBoxes().put("K04",this.getSectors().get("K04"));
		this.getSectors().get("K03").getNearbyBoxes().put("L02",this.getSectors().get("L02"));
		this.getSectors().get("K03").getNearbyBoxes().put("L03",this.getSectors().get("L03"));
		
		this.getSectors().get("K04").getNearbyBoxes().put("J03",this.getSectors().get("J03"));
		this.getSectors().get("K04").getNearbyBoxes().put("J04",this.getSectors().get("J04"));
		this.getSectors().get("K04").getNearbyBoxes().put("K03",this.getSectors().get("K03"));
		this.getSectors().get("K04").getNearbyBoxes().put("K05",this.getSectors().get("K05"));
		this.getSectors().get("K04").getNearbyBoxes().put("L03",this.getSectors().get("L03"));
		this.getSectors().get("K04").getNearbyBoxes().put("L04",this.getSectors().get("L04"));
		
		this.getSectors().get("K05").getNearbyBoxes().put("J04",this.getSectors().get("J04"));
		this.getSectors().get("K05").getNearbyBoxes().put("J05",this.getSectors().get("J05"));
		this.getSectors().get("K05").getNearbyBoxes().put("K04",this.getSectors().get("K04"));
		this.getSectors().get("K05").getNearbyBoxes().put("K06",this.getSectors().get("K06"));
		this.getSectors().get("K05").getNearbyBoxes().put("L04",this.getSectors().get("L04"));
		this.getSectors().get("K05").getNearbyBoxes().put("L05",this.getSectors().get("L05"));
		
		this.getSectors().get("K06").getNearbyBoxes().put("J05",this.getSectors().get("J05"));
		this.getSectors().get("K06").getNearbyBoxes().put("J06",this.getSectors().get("J06"));
		this.getSectors().get("K06").getNearbyBoxes().put("K05",this.getSectors().get("K05"));
		this.getSectors().get("K06").getNearbyBoxes().put("L05",this.getSectors().get("L05"));
		this.getSectors().get("K06").getNearbyBoxes().put("XA",this.getSectors().get("XA"));
		
		this.getSectors().get("K08").getNearbyBoxes().put("J08",this.getSectors().get("J08"));
		this.getSectors().get("K08").getNearbyBoxes().put("K09",this.getSectors().get("K09"));
		this.getSectors().get("K08").getNearbyBoxes().put("XU",this.getSectors().get("XU"));
		
		this.getSectors().get("K09").getNearbyBoxes().put("J08",this.getSectors().get("J08"));
		this.getSectors().get("K09").getNearbyBoxes().put("J09",this.getSectors().get("J09"));
		this.getSectors().get("K09").getNearbyBoxes().put("K08",this.getSectors().get("K08"));
		this.getSectors().get("K09").getNearbyBoxes().put("K10",this.getSectors().get("K10"));
		this.getSectors().get("K09").getNearbyBoxes().put("XU",this.getSectors().get("XU"));
		this.getSectors().get("K09").getNearbyBoxes().put("L09",this.getSectors().get("L09"));
		
		this.getSectors().get("K10").getNearbyBoxes().put("J09",this.getSectors().get("J09"));
		this.getSectors().get("K10").getNearbyBoxes().put("J10",this.getSectors().get("J10"));
		this.getSectors().get("K10").getNearbyBoxes().put("K09",this.getSectors().get("K09"));
		this.getSectors().get("K10").getNearbyBoxes().put("K11",this.getSectors().get("K11"));
		this.getSectors().get("K10").getNearbyBoxes().put("L09",this.getSectors().get("L09"));
		this.getSectors().get("K10").getNearbyBoxes().put("L10",this.getSectors().get("L10"));
		
		this.getSectors().get("K11").getNearbyBoxes().put("J10",this.getSectors().get("J10"));
		this.getSectors().get("K11").getNearbyBoxes().put("J11",this.getSectors().get("J11"));
		this.getSectors().get("K11").getNearbyBoxes().put("K10",this.getSectors().get("K10"));
		this.getSectors().get("K11").getNearbyBoxes().put("K12",this.getSectors().get("K12"));
		this.getSectors().get("K11").getNearbyBoxes().put("L10",this.getSectors().get("L10"));
		this.getSectors().get("K11").getNearbyBoxes().put("L11",this.getSectors().get("L11"));
		
		this.getSectors().get("K12").getNearbyBoxes().put("J11",this.getSectors().get("J11"));
		this.getSectors().get("K12").getNearbyBoxes().put("K11",this.getSectors().get("K11"));
		this.getSectors().get("K12").getNearbyBoxes().put("L11",this.getSectors().get("L11"));
		this.getSectors().get("K12").getNearbyBoxes().put("L12",this.getSectors().get("L12"));

		this.getSectors().get("K14").getNearbyBoxes().put("J13",this.getSectors().get("J13"));
		this.getSectors().get("K14").getNearbyBoxes().put("J14",this.getSectors().get("J14"));
		this.getSectors().get("K14").getNearbyBoxes().put("L13",this.getSectors().get("L13"));
		this.getSectors().get("K14").getNearbyBoxes().put("L14",this.getSectors().get("L14"));

		
		//adiacenti L
		this.getSectors().get("L01").getNearbyBoxes().put("K01",this.getSectors().get("K01"));
		this.getSectors().get("L01").getNearbyBoxes().put("K02",this.getSectors().get("K02"));
		this.getSectors().get("L01").getNearbyBoxes().put("L02",this.getSectors().get("L02"));
		this.getSectors().get("L01").getNearbyBoxes().put("M01",this.getSectors().get("M01"));
		this.getSectors().get("L01").getNearbyBoxes().put("M02",this.getSectors().get("M02"));
		
		this.getSectors().get("L02").getNearbyBoxes().put("K02",this.getSectors().get("K02"));
		this.getSectors().get("L02").getNearbyBoxes().put("K03",this.getSectors().get("K03"));
		this.getSectors().get("L02").getNearbyBoxes().put("L01",this.getSectors().get("L01"));
		this.getSectors().get("L02").getNearbyBoxes().put("L03",this.getSectors().get("L03"));
		this.getSectors().get("L02").getNearbyBoxes().put("M02",this.getSectors().get("M02"));
		this.getSectors().get("L02").getNearbyBoxes().put("M03",this.getSectors().get("M03"));
		
		this.getSectors().get("L03").getNearbyBoxes().put("K03",this.getSectors().get("K03"));
		this.getSectors().get("L03").getNearbyBoxes().put("K04",this.getSectors().get("K04"));
		this.getSectors().get("L03").getNearbyBoxes().put("L02",this.getSectors().get("L02"));
		this.getSectors().get("L03").getNearbyBoxes().put("L04",this.getSectors().get("L04"));
		this.getSectors().get("L03").getNearbyBoxes().put("M03",this.getSectors().get("M03"));
		this.getSectors().get("L03").getNearbyBoxes().put("M04",this.getSectors().get("M04"));
		
		this.getSectors().get("L04").getNearbyBoxes().put("K04",this.getSectors().get("K04"));
		this.getSectors().get("L04").getNearbyBoxes().put("K05",this.getSectors().get("K05"));
		this.getSectors().get("L04").getNearbyBoxes().put("L03",this.getSectors().get("L03"));
		this.getSectors().get("L04").getNearbyBoxes().put("L05",this.getSectors().get("L05"));
		this.getSectors().get("L04").getNearbyBoxes().put("M04",this.getSectors().get("M04"));
		this.getSectors().get("L04").getNearbyBoxes().put("M05",this.getSectors().get("M05"));
		
		this.getSectors().get("L05").getNearbyBoxes().put("K05",this.getSectors().get("K05"));
		this.getSectors().get("L05").getNearbyBoxes().put("K06",this.getSectors().get("K06"));
		this.getSectors().get("L05").getNearbyBoxes().put("L04",this.getSectors().get("L04"));
		this.getSectors().get("L05").getNearbyBoxes().put("XA", this.getSectors().get("XA"));
		this.getSectors().get("L05").getNearbyBoxes().put("M05",this.getSectors().get("M05"));
		this.getSectors().get("L05").getNearbyBoxes().put("M06",this.getSectors().get("M06"));
		
		this.getSectors().get("L09").getNearbyBoxes().put("K09",this.getSectors().get("K09"));
		this.getSectors().get("L09").getNearbyBoxes().put("K10",this.getSectors().get("K10"));
		this.getSectors().get("L09").getNearbyBoxes().put("XU",this.getSectors().get("XU"));
		this.getSectors().get("L09").getNearbyBoxes().put("L10",this.getSectors().get("L10"));
		this.getSectors().get("L09").getNearbyBoxes().put("M09",this.getSectors().get("M09"));
		this.getSectors().get("L09").getNearbyBoxes().put("M10",this.getSectors().get("M10"));
		
		this.getSectors().get("L10").getNearbyBoxes().put("K10",this.getSectors().get("K10"));
		this.getSectors().get("L10").getNearbyBoxes().put("K11",this.getSectors().get("K11"));
		this.getSectors().get("L10").getNearbyBoxes().put("L09",this.getSectors().get("L09"));
		this.getSectors().get("L10").getNearbyBoxes().put("L11",this.getSectors().get("L11"));
		this.getSectors().get("L10").getNearbyBoxes().put("M10",this.getSectors().get("M10"));
		this.getSectors().get("L10").getNearbyBoxes().put("M11",this.getSectors().get("M11"));
		
		this.getSectors().get("L11").getNearbyBoxes().put("K11",this.getSectors().get("K11"));
		this.getSectors().get("L11").getNearbyBoxes().put("K12",this.getSectors().get("K12"));
		this.getSectors().get("L11").getNearbyBoxes().put("L10",this.getSectors().get("L10"));
		this.getSectors().get("L11").getNearbyBoxes().put("L12",this.getSectors().get("L12"));
		this.getSectors().get("L11").getNearbyBoxes().put("M11",this.getSectors().get("M11"));
		this.getSectors().get("L11").getNearbyBoxes().put("M12",this.getSectors().get("M12"));
		
		this.getSectors().get("L12").getNearbyBoxes().put("K12",this.getSectors().get("K12"));
		this.getSectors().get("L12").getNearbyBoxes().put("L11",this.getSectors().get("L11"));
		this.getSectors().get("L12").getNearbyBoxes().put("L13",this.getSectors().get("L13"));
		this.getSectors().get("L12").getNearbyBoxes().put("M12",this.getSectors().get("M12"));
		this.getSectors().get("L12").getNearbyBoxes().put("M13",this.getSectors().get("M13"));
		
		this.getSectors().get("L13").getNearbyBoxes().put("K14",this.getSectors().get("K14"));
		this.getSectors().get("L13").getNearbyBoxes().put("L12",this.getSectors().get("L12"));
		this.getSectors().get("L13").getNearbyBoxes().put("L14",this.getSectors().get("L14"));
		this.getSectors().get("L13").getNearbyBoxes().put("M13",this.getSectors().get("M13"));
		this.getSectors().get("L13").getNearbyBoxes().put("M14",this.getSectors().get("M14"));
		
		this.getSectors().get("L14").getNearbyBoxes().put("K14",this.getSectors().get("K14"));
		this.getSectors().get("L14").getNearbyBoxes().put("L13",this.getSectors().get("L13"));
		this.getSectors().get("L14").getNearbyBoxes().put("M14",this.getSectors().get("M14"));
		
		//adiacenti M
		this.getSectors().get("M01").getNearbyBoxes().put("L01",this.getSectors().get("L01"));
		this.getSectors().get("M01").getNearbyBoxes().put("M02",this.getSectors().get("M02"));
		this.getSectors().get("M01").getNearbyBoxes().put("N01",this.getSectors().get("N01"));
		
		this.getSectors().get("M02").getNearbyBoxes().put("L01",this.getSectors().get("L01"));
		this.getSectors().get("M02").getNearbyBoxes().put("L02",this.getSectors().get("L02"));
		this.getSectors().get("M02").getNearbyBoxes().put("M01",this.getSectors().get("M01"));
		this.getSectors().get("M02").getNearbyBoxes().put("M03",this.getSectors().get("M03"));
		this.getSectors().get("M02").getNearbyBoxes().put("N01",this.getSectors().get("N01"));
		this.getSectors().get("M02").getNearbyBoxes().put("N02",this.getSectors().get("N02"));
		
		this.getSectors().get("M03").getNearbyBoxes().put("L02",this.getSectors().get("L02"));
		this.getSectors().get("M03").getNearbyBoxes().put("L03",this.getSectors().get("L03"));
		this.getSectors().get("M03").getNearbyBoxes().put("M02",this.getSectors().get("M02"));
		this.getSectors().get("M03").getNearbyBoxes().put("M04",this.getSectors().get("M04"));
		this.getSectors().get("M03").getNearbyBoxes().put("N02",this.getSectors().get("N02"));
		this.getSectors().get("M03").getNearbyBoxes().put("N03",this.getSectors().get("N03"));
		
		this.getSectors().get("M04").getNearbyBoxes().put("L03",this.getSectors().get("L03"));
		this.getSectors().get("M04").getNearbyBoxes().put("L04",this.getSectors().get("L04"));
		this.getSectors().get("M04").getNearbyBoxes().put("M03",this.getSectors().get("M03"));
		this.getSectors().get("M04").getNearbyBoxes().put("M05",this.getSectors().get("M05"));
		this.getSectors().get("M04").getNearbyBoxes().put("N03",this.getSectors().get("N03"));
		this.getSectors().get("M04").getNearbyBoxes().put("N04",this.getSectors().get("N04"));
		
		this.getSectors().get("M05").getNearbyBoxes().put("L04",this.getSectors().get("L04"));
		this.getSectors().get("M05").getNearbyBoxes().put("L05",this.getSectors().get("L05"));
		this.getSectors().get("M05").getNearbyBoxes().put("M04",this.getSectors().get("M04"));
		this.getSectors().get("M05").getNearbyBoxes().put("M06",this.getSectors().get("M06"));
		this.getSectors().get("M05").getNearbyBoxes().put("N04",this.getSectors().get("N04"));
		this.getSectors().get("M05").getNearbyBoxes().put("N05",this.getSectors().get("N05"));
		
		this.getSectors().get("M06").getNearbyBoxes().put("L05",this.getSectors().get("L05"));
		this.getSectors().get("M06").getNearbyBoxes().put("XA",this.getSectors().get("XA"));
		this.getSectors().get("M06").getNearbyBoxes().put("M05",this.getSectors().get("M05"));
		this.getSectors().get("M06").getNearbyBoxes().put("N05",this.getSectors().get("N05"));
		this.getSectors().get("M06").getNearbyBoxes().put("N06",this.getSectors().get("N06"));

		this.getSectors().get("M08").getNearbyBoxes().put("XU",this.getSectors().get("XU"));
		this.getSectors().get("M08").getNearbyBoxes().put("M09",this.getSectors().get("M09"));
		this.getSectors().get("M08").getNearbyBoxes().put("N08",this.getSectors().get("N08"));

		this.getSectors().get("M09").getNearbyBoxes().put("XU",this.getSectors().get("XU"));
		this.getSectors().get("M09").getNearbyBoxes().put("L09",this.getSectors().get("L09"));
		this.getSectors().get("M09").getNearbyBoxes().put("M08",this.getSectors().get("M08"));
		this.getSectors().get("M09").getNearbyBoxes().put("M10",this.getSectors().get("M10"));
		this.getSectors().get("M09").getNearbyBoxes().put("N08",this.getSectors().get("N08"));
		this.getSectors().get("M09").getNearbyBoxes().put("N09",this.getSectors().get("N09"));
		
		this.getSectors().get("M10").getNearbyBoxes().put("L09",this.getSectors().get("L09"));
		this.getSectors().get("M10").getNearbyBoxes().put("L10",this.getSectors().get("L10"));
		this.getSectors().get("M10").getNearbyBoxes().put("M09",this.getSectors().get("M09"));
		this.getSectors().get("M10").getNearbyBoxes().put("M11",this.getSectors().get("M11"));
		this.getSectors().get("M10").getNearbyBoxes().put("N09",this.getSectors().get("N09"));
		this.getSectors().get("M10").getNearbyBoxes().put("N10",this.getSectors().get("N10"));
		
		this.getSectors().get("M11").getNearbyBoxes().put("L10",this.getSectors().get("L10"));
		this.getSectors().get("M11").getNearbyBoxes().put("L11",this.getSectors().get("L11"));
		this.getSectors().get("M11").getNearbyBoxes().put("M10",this.getSectors().get("M10"));
		this.getSectors().get("M11").getNearbyBoxes().put("M12",this.getSectors().get("M12"));
		this.getSectors().get("M11").getNearbyBoxes().put("N10",this.getSectors().get("N10"));
		this.getSectors().get("M11").getNearbyBoxes().put("N11",this.getSectors().get("N11"));
		
		this.getSectors().get("M12").getNearbyBoxes().put("L11",this.getSectors().get("L11"));
		this.getSectors().get("M12").getNearbyBoxes().put("L12",this.getSectors().get("L12"));
		this.getSectors().get("M12").getNearbyBoxes().put("M11",this.getSectors().get("M11"));
		this.getSectors().get("M12").getNearbyBoxes().put("M13",this.getSectors().get("M13"));
		this.getSectors().get("M12").getNearbyBoxes().put("N11",this.getSectors().get("N11"));
		this.getSectors().get("M12").getNearbyBoxes().put("N12",this.getSectors().get("N12"));
		
		this.getSectors().get("M13").getNearbyBoxes().put("L12",this.getSectors().get("L12"));
		this.getSectors().get("M13").getNearbyBoxes().put("L13",this.getSectors().get("L13"));
		this.getSectors().get("M13").getNearbyBoxes().put("M12",this.getSectors().get("M12"));
		this.getSectors().get("M13").getNearbyBoxes().put("M14",this.getSectors().get("M14"));
		this.getSectors().get("M13").getNearbyBoxes().put("N12",this.getSectors().get("N12"));
		this.getSectors().get("M13").getNearbyBoxes().put("N13",this.getSectors().get("N13"));
		
		this.getSectors().get("M14").getNearbyBoxes().put("L13",this.getSectors().get("L13"));
		this.getSectors().get("M14").getNearbyBoxes().put("L14",this.getSectors().get("L14"));
		this.getSectors().get("M14").getNearbyBoxes().put("M13",this.getSectors().get("M13"));
		this.getSectors().get("M14").getNearbyBoxes().put("N13",this.getSectors().get("N13"));
		this.getSectors().get("M14").getNearbyBoxes().put("N14",this.getSectors().get("N14"));

		
		//adiacenti N
		this.getSectors().get("N01").getNearbyBoxes().put("M01",this.getSectors().get("M01"));
		this.getSectors().get("N01").getNearbyBoxes().put("M02",this.getSectors().get("M02"));
		this.getSectors().get("N01").getNearbyBoxes().put("N02",this.getSectors().get("N02"));
		this.getSectors().get("N01").getNearbyBoxes().put("O02",this.getSectors().get("O02"));
		
		this.getSectors().get("N02").getNearbyBoxes().put("M02",this.getSectors().get("M02"));
		this.getSectors().get("N02").getNearbyBoxes().put("M03",this.getSectors().get("M03"));
		this.getSectors().get("N02").getNearbyBoxes().put("N01",this.getSectors().get("N01"));
		this.getSectors().get("N02").getNearbyBoxes().put("N03",this.getSectors().get("N03"));
		this.getSectors().get("N02").getNearbyBoxes().put("O02",this.getSectors().get("O02"));
		
		this.getSectors().get("N03").getNearbyBoxes().put("M03",this.getSectors().get("M03"));
		this.getSectors().get("N03").getNearbyBoxes().put("M04",this.getSectors().get("M04"));
		this.getSectors().get("N03").getNearbyBoxes().put("N02",this.getSectors().get("N02"));
		this.getSectors().get("N03").getNearbyBoxes().put("N04",this.getSectors().get("N04"));

		this.getSectors().get("N04").getNearbyBoxes().put("M04",this.getSectors().get("M04"));
		this.getSectors().get("N04").getNearbyBoxes().put("M05",this.getSectors().get("M05"));
		this.getSectors().get("N04").getNearbyBoxes().put("N03",this.getSectors().get("N03"));
		this.getSectors().get("N04").getNearbyBoxes().put("N05",this.getSectors().get("N05"));
		this.getSectors().get("N04").getNearbyBoxes().put("O05",this.getSectors().get("O05"));
		
		this.getSectors().get("N05").getNearbyBoxes().put("M05",this.getSectors().get("M05"));
		this.getSectors().get("N05").getNearbyBoxes().put("M06",this.getSectors().get("M06"));
		this.getSectors().get("N05").getNearbyBoxes().put("N04",this.getSectors().get("N04"));
		this.getSectors().get("N05").getNearbyBoxes().put("N06",this.getSectors().get("N06"));
		this.getSectors().get("N05").getNearbyBoxes().put("O05",this.getSectors().get("O05"));
		this.getSectors().get("N05").getNearbyBoxes().put("O06",this.getSectors().get("O06"));
		
		this.getSectors().get("N06").getNearbyBoxes().put("M06",this.getSectors().get("M06"));
		this.getSectors().get("N06").getNearbyBoxes().put("N05",this.getSectors().get("N05"));
		this.getSectors().get("N06").getNearbyBoxes().put("O06",this.getSectors().get("O06"));
		this.getSectors().get("N06").getNearbyBoxes().put("O07",this.getSectors().get("O07"));

		this.getSectors().get("N08").getNearbyBoxes().put("M08",this.getSectors().get("M08"));
		this.getSectors().get("N08").getNearbyBoxes().put("M09",this.getSectors().get("M09"));
		this.getSectors().get("N08").getNearbyBoxes().put("N09",this.getSectors().get("N09"));
		this.getSectors().get("N08").getNearbyBoxes().put("O08",this.getSectors().get("O08"));
		this.getSectors().get("N08").getNearbyBoxes().put("O09",this.getSectors().get("O09"));
		
		this.getSectors().get("N09").getNearbyBoxes().put("M09",this.getSectors().get("M09"));
		this.getSectors().get("N09").getNearbyBoxes().put("M10",this.getSectors().get("M10"));
		this.getSectors().get("N09").getNearbyBoxes().put("N08",this.getSectors().get("N08"));
		this.getSectors().get("N09").getNearbyBoxes().put("N10",this.getSectors().get("N10"));
		this.getSectors().get("N09").getNearbyBoxes().put("O09",this.getSectors().get("O09"));
		this.getSectors().get("N09").getNearbyBoxes().put("O10",this.getSectors().get("O10"));
		
		this.getSectors().get("N10").getNearbyBoxes().put("M10",this.getSectors().get("M10"));
		this.getSectors().get("N10").getNearbyBoxes().put("M11",this.getSectors().get("M11"));
		this.getSectors().get("N10").getNearbyBoxes().put("N09",this.getSectors().get("N09"));
		this.getSectors().get("N10").getNearbyBoxes().put("N11",this.getSectors().get("N11"));
		this.getSectors().get("N10").getNearbyBoxes().put("O10",this.getSectors().get("O10"));
		this.getSectors().get("N10").getNearbyBoxes().put("O11",this.getSectors().get("O11"));
		
		this.getSectors().get("N11").getNearbyBoxes().put("M11",this.getSectors().get("M11"));
		this.getSectors().get("N11").getNearbyBoxes().put("M12",this.getSectors().get("M12"));
		this.getSectors().get("N11").getNearbyBoxes().put("N10",this.getSectors().get("N10"));
		this.getSectors().get("N11").getNearbyBoxes().put("N12",this.getSectors().get("N12"));
		this.getSectors().get("N11").getNearbyBoxes().put("O11",this.getSectors().get("O11"));
		this.getSectors().get("N11").getNearbyBoxes().put("O12",this.getSectors().get("O12"));
		
		this.getSectors().get("N12").getNearbyBoxes().put("M12",this.getSectors().get("M12"));
		this.getSectors().get("N12").getNearbyBoxes().put("M13",this.getSectors().get("M13"));
		this.getSectors().get("N12").getNearbyBoxes().put("N11",this.getSectors().get("N11"));
		this.getSectors().get("N12").getNearbyBoxes().put("N13",this.getSectors().get("N13"));
		this.getSectors().get("N12").getNearbyBoxes().put("O12",this.getSectors().get("O12"));
		this.getSectors().get("N12").getNearbyBoxes().put("O13",this.getSectors().get("O13"));
		
		this.getSectors().get("N13").getNearbyBoxes().put("M13",this.getSectors().get("M13"));
		this.getSectors().get("N13").getNearbyBoxes().put("M14",this.getSectors().get("M14"));
		this.getSectors().get("N13").getNearbyBoxes().put("N12",this.getSectors().get("N12"));
		this.getSectors().get("N13").getNearbyBoxes().put("N14",this.getSectors().get("N14"));
		this.getSectors().get("N13").getNearbyBoxes().put("O13",this.getSectors().get("O13"));
		this.getSectors().get("N13").getNearbyBoxes().put("O14",this.getSectors().get("O14"));
		
		this.getSectors().get("N14").getNearbyBoxes().put("M14",this.getSectors().get("M14"));
		this.getSectors().get("N14").getNearbyBoxes().put("N13",this.getSectors().get("N13"));
		this.getSectors().get("N14").getNearbyBoxes().put("O14",this.getSectors().get("O14"));
		
		//adiacenti O
		this.getSectors().get("O02").getNearbyBoxes().put("N01",this.getSectors().get("N01"));
		this.getSectors().get("O02").getNearbyBoxes().put("N02",this.getSectors().get("N02"));
		this.getSectors().get("O02").getNearbyBoxes().put("P01",this.getSectors().get("P01"));
		this.getSectors().get("O02").getNearbyBoxes().put("P02",this.getSectors().get("P02"));
		this.getSectors().get("O05").getNearbyBoxes().put("N04",this.getSectors().get("N04"));
		this.getSectors().get("O05").getNearbyBoxes().put("N05",this.getSectors().get("N05"));
		this.getSectors().get("O05").getNearbyBoxes().put("O06",this.getSectors().get("O06"));
		this.getSectors().get("O05").getNearbyBoxes().put("P04",this.getSectors().get("P04"));
		this.getSectors().get("O05").getNearbyBoxes().put("P05",this.getSectors().get("P05"));
		
		this.getSectors().get("O06").getNearbyBoxes().put("N05",this.getSectors().get("N05"));
		this.getSectors().get("O06").getNearbyBoxes().put("N06",this.getSectors().get("N06"));
		this.getSectors().get("O06").getNearbyBoxes().put("O05",this.getSectors().get("O05"));
		this.getSectors().get("O06").getNearbyBoxes().put("O07",this.getSectors().get("O07"));
		this.getSectors().get("O06").getNearbyBoxes().put("P05",this.getSectors().get("P05"));
		this.getSectors().get("O06").getNearbyBoxes().put("P06",this.getSectors().get("P06"));
		
		this.getSectors().get("O07").getNearbyBoxes().put("N06",this.getSectors().get("N06"));
		this.getSectors().get("O07").getNearbyBoxes().put("O06",this.getSectors().get("O06"));
		this.getSectors().get("O07").getNearbyBoxes().put("O08",this.getSectors().get("O08"));
		this.getSectors().get("O07").getNearbyBoxes().put("P06",this.getSectors().get("P06"));
		this.getSectors().get("O07").getNearbyBoxes().put("P07",this.getSectors().get("P07"));

		this.getSectors().get("O08").getNearbyBoxes().put("N08",this.getSectors().get("N08"));
		this.getSectors().get("O08").getNearbyBoxes().put("O07",this.getSectors().get("O07"));
		this.getSectors().get("O08").getNearbyBoxes().put("O09",this.getSectors().get("O09"));
		this.getSectors().get("O08").getNearbyBoxes().put("P07",this.getSectors().get("P07"));
		this.getSectors().get("O08").getNearbyBoxes().put("P08",this.getSectors().get("P08"));
		
		this.getSectors().get("O09").getNearbyBoxes().put("N08",this.getSectors().get("N08"));
		this.getSectors().get("O09").getNearbyBoxes().put("N09",this.getSectors().get("N09"));
		this.getSectors().get("O09").getNearbyBoxes().put("O08",this.getSectors().get("O08"));
		this.getSectors().get("O09").getNearbyBoxes().put("O10",this.getSectors().get("O10"));
		this.getSectors().get("O09").getNearbyBoxes().put("P08",this.getSectors().get("P08"));
		this.getSectors().get("O09").getNearbyBoxes().put("P09",this.getSectors().get("P09"));
		
		this.getSectors().get("O10").getNearbyBoxes().put("N09",this.getSectors().get("N09"));
		this.getSectors().get("O10").getNearbyBoxes().put("N10",this.getSectors().get("N10"));
		this.getSectors().get("O10").getNearbyBoxes().put("O09",this.getSectors().get("O09"));
		this.getSectors().get("O10").getNearbyBoxes().put("O11",this.getSectors().get("O11"));
		this.getSectors().get("O10").getNearbyBoxes().put("P09",this.getSectors().get("P09"));
		this.getSectors().get("O10").getNearbyBoxes().put("P10",this.getSectors().get("P10"));
		
		this.getSectors().get("O11").getNearbyBoxes().put("N10",this.getSectors().get("N10"));
		this.getSectors().get("O11").getNearbyBoxes().put("N11",this.getSectors().get("N11"));
		this.getSectors().get("O11").getNearbyBoxes().put("O10",this.getSectors().get("O10"));
		this.getSectors().get("O11").getNearbyBoxes().put("O12",this.getSectors().get("O12"));
		this.getSectors().get("O11").getNearbyBoxes().put("P10",this.getSectors().get("P10"));
		this.getSectors().get("O11").getNearbyBoxes().put("P11",this.getSectors().get("P11"));
		
		this.getSectors().get("O12").getNearbyBoxes().put("N11",this.getSectors().get("N11"));
		this.getSectors().get("O12").getNearbyBoxes().put("N12",this.getSectors().get("N12"));
		this.getSectors().get("O12").getNearbyBoxes().put("O11",this.getSectors().get("O11"));
		this.getSectors().get("O12").getNearbyBoxes().put("O13",this.getSectors().get("O13"));
		this.getSectors().get("O12").getNearbyBoxes().put("P11",this.getSectors().get("P11"));
		this.getSectors().get("O12").getNearbyBoxes().put("P12",this.getSectors().get("P12"));
		
		this.getSectors().get("O13").getNearbyBoxes().put("N12",this.getSectors().get("N12"));
		this.getSectors().get("O13").getNearbyBoxes().put("N13",this.getSectors().get("N13"));
		this.getSectors().get("O13").getNearbyBoxes().put("O12",this.getSectors().get("O12"));
		this.getSectors().get("O13").getNearbyBoxes().put("O14",this.getSectors().get("O14"));
		this.getSectors().get("O13").getNearbyBoxes().put("P12",this.getSectors().get("P12"));
		this.getSectors().get("O13").getNearbyBoxes().put("P13",this.getSectors().get("P13"));
		
		this.getSectors().get("O14").getNearbyBoxes().put("N13",this.getSectors().get("N13"));
		this.getSectors().get("O14").getNearbyBoxes().put("N14",this.getSectors().get("N14"));
		this.getSectors().get("O14").getNearbyBoxes().put("O13",this.getSectors().get("O13"));
		this.getSectors().get("O14").getNearbyBoxes().put("P13",this.getSectors().get("P13"));
		this.getSectors().get("O14").getNearbyBoxes().put("P14",this.getSectors().get("P14"));

		
		//adiacenti P
		this.getSectors().get("P01").getNearbyBoxes().put("O02",this.getSectors().get("O02"));
		this.getSectors().get("P01").getNearbyBoxes().put("P02",this.getSectors().get("P02"));
		this.getSectors().get("P01").getNearbyBoxes().put("Q01",this.getSectors().get("Q01"));
		this.getSectors().get("P01").getNearbyBoxes().put("Q02",this.getSectors().get("Q02"));
		
		this.getSectors().get("P02").getNearbyBoxes().put("O02",this.getSectors().get("O02"));
		this.getSectors().get("P02").getNearbyBoxes().put("P01",this.getSectors().get("P01"));
		this.getSectors().get("P02").getNearbyBoxes().put("P03",this.getSectors().get("P03"));
		this.getSectors().get("P02").getNearbyBoxes().put("Q02",this.getSectors().get("Q02"));
		this.getSectors().get("P02").getNearbyBoxes().put("Q03",this.getSectors().get("Q03"));

		this.getSectors().get("P03").getNearbyBoxes().put("P02",this.getSectors().get("P02"));
		this.getSectors().get("P03").getNearbyBoxes().put("P04",this.getSectors().get("P04"));
		this.getSectors().get("P03").getNearbyBoxes().put("Q03",this.getSectors().get("Q03"));
		this.getSectors().get("P03").getNearbyBoxes().put("Q04",this.getSectors().get("Q04"));

		this.getSectors().get("P04").getNearbyBoxes().put("O05",this.getSectors().get("O05"));
		this.getSectors().get("P04").getNearbyBoxes().put("P03",this.getSectors().get("P03"));
		this.getSectors().get("P04").getNearbyBoxes().put("P05",this.getSectors().get("P05"));
		this.getSectors().get("P04").getNearbyBoxes().put("Q04",this.getSectors().get("Q04"));
		this.getSectors().get("P04").getNearbyBoxes().put("Q05",this.getSectors().get("Q05"));
		
		this.getSectors().get("P05").getNearbyBoxes().put("O05",this.getSectors().get("O05"));
		this.getSectors().get("P05").getNearbyBoxes().put("O06",this.getSectors().get("O06"));
		this.getSectors().get("P05").getNearbyBoxes().put("P04",this.getSectors().get("P04"));
		this.getSectors().get("P05").getNearbyBoxes().put("P06",this.getSectors().get("P06"));
		this.getSectors().get("P05").getNearbyBoxes().put("Q05",this.getSectors().get("Q05"));
		this.getSectors().get("P05").getNearbyBoxes().put("Q06",this.getSectors().get("Q06"));
		
		this.getSectors().get("P06").getNearbyBoxes().put("O06",this.getSectors().get("O06"));
		this.getSectors().get("P06").getNearbyBoxes().put("O07",this.getSectors().get("O07"));
		this.getSectors().get("P06").getNearbyBoxes().put("P05",this.getSectors().get("P05"));
		this.getSectors().get("P06").getNearbyBoxes().put("P07",this.getSectors().get("P07"));
		this.getSectors().get("P06").getNearbyBoxes().put("Q06",this.getSectors().get("Q06"));
		this.getSectors().get("P06").getNearbyBoxes().put("Q07",this.getSectors().get("Q07"));
		
		this.getSectors().get("P07").getNearbyBoxes().put("O07",this.getSectors().get("O07"));
		this.getSectors().get("P07").getNearbyBoxes().put("O08",this.getSectors().get("O08"));
		this.getSectors().get("P07").getNearbyBoxes().put("P06",this.getSectors().get("P06"));
		this.getSectors().get("P07").getNearbyBoxes().put("P08",this.getSectors().get("P08"));
		this.getSectors().get("P07").getNearbyBoxes().put("Q07",this.getSectors().get("Q07"));
		this.getSectors().get("P07").getNearbyBoxes().put("Q08",this.getSectors().get("Q08"));
		
		this.getSectors().get("P08").getNearbyBoxes().put("O08",this.getSectors().get("O08"));
		this.getSectors().get("P08").getNearbyBoxes().put("O09",this.getSectors().get("O09"));
		this.getSectors().get("P08").getNearbyBoxes().put("P07",this.getSectors().get("P07"));
		this.getSectors().get("P08").getNearbyBoxes().put("P09",this.getSectors().get("P09"));
		this.getSectors().get("P08").getNearbyBoxes().put("Q08",this.getSectors().get("Q08"));
		this.getSectors().get("P08").getNearbyBoxes().put("Q09",this.getSectors().get("Q09"));
		
		this.getSectors().get("P09").getNearbyBoxes().put("O09",this.getSectors().get("O09"));
		this.getSectors().get("P09").getNearbyBoxes().put("O10",this.getSectors().get("O10"));
		this.getSectors().get("P09").getNearbyBoxes().put("P08",this.getSectors().get("P08"));
		this.getSectors().get("P09").getNearbyBoxes().put("P10",this.getSectors().get("P10"));
		this.getSectors().get("P09").getNearbyBoxes().put("Q09",this.getSectors().get("Q09"));
		this.getSectors().get("P09").getNearbyBoxes().put("Q10",this.getSectors().get("Q10"));
		
		this.getSectors().get("P10").getNearbyBoxes().put("O10",this.getSectors().get("O10"));
		this.getSectors().get("P10").getNearbyBoxes().put("O11",this.getSectors().get("O11"));
		this.getSectors().get("P10").getNearbyBoxes().put("P09",this.getSectors().get("P09"));
		this.getSectors().get("P10").getNearbyBoxes().put("P11",this.getSectors().get("P11"));
		this.getSectors().get("P10").getNearbyBoxes().put("Q10",this.getSectors().get("Q10"));
		this.getSectors().get("P10").getNearbyBoxes().put("Q11",this.getSectors().get("Q11"));
		
		this.getSectors().get("P11").getNearbyBoxes().put("O11",this.getSectors().get("O11"));
		this.getSectors().get("P11").getNearbyBoxes().put("O12",this.getSectors().get("O12"));
		this.getSectors().get("P11").getNearbyBoxes().put("P10",this.getSectors().get("P10"));
		this.getSectors().get("P11").getNearbyBoxes().put("P12",this.getSectors().get("P12"));
		this.getSectors().get("P11").getNearbyBoxes().put("Q11",this.getSectors().get("Q11"));
		this.getSectors().get("P11").getNearbyBoxes().put("Q12",this.getSectors().get("Q12"));
		
		this.getSectors().get("P12").getNearbyBoxes().put("O12",this.getSectors().get("O12"));
		this.getSectors().get("P12").getNearbyBoxes().put("O13",this.getSectors().get("O13"));
		this.getSectors().get("P12").getNearbyBoxes().put("P11",this.getSectors().get("P11"));
		this.getSectors().get("P12").getNearbyBoxes().put("P13",this.getSectors().get("P13"));
		this.getSectors().get("P12").getNearbyBoxes().put("Q12",this.getSectors().get("Q12"));
		this.getSectors().get("P12").getNearbyBoxes().put("Q13",this.getSectors().get("Q13"));
		
		this.getSectors().get("P13").getNearbyBoxes().put("O13",this.getSectors().get("O13"));
		this.getSectors().get("P13").getNearbyBoxes().put("O14",this.getSectors().get("O14"));
		this.getSectors().get("P13").getNearbyBoxes().put("P12",this.getSectors().get("P12"));
		this.getSectors().get("P13").getNearbyBoxes().put("P14",this.getSectors().get("P14"));
		this.getSectors().get("P13").getNearbyBoxes().put("Q13",this.getSectors().get("Q13"));
		this.getSectors().get("P13").getNearbyBoxes().put("Q14",this.getSectors().get("Q14"));
		
		this.getSectors().get("P14").getNearbyBoxes().put("O14",this.getSectors().get("O14"));
		this.getSectors().get("P14").getNearbyBoxes().put("P13",this.getSectors().get("P13"));
		this.getSectors().get("P14").getNearbyBoxes().put("Q14",this.getSectors().get("Q14"));
		
		//adiacenti Q
		this.getSectors().get("Q01").getNearbyBoxes().put("P01",this.getSectors().get("P01"));
		this.getSectors().get("Q01").getNearbyBoxes().put("Q02",this.getSectors().get("Q02"));
		this.getSectors().get("Q01").getNearbyBoxes().put("R01",this.getSectors().get("R01"));
		
		this.getSectors().get("Q02").getNearbyBoxes().put("P01",this.getSectors().get("P01"));
		this.getSectors().get("Q02").getNearbyBoxes().put("P02",this.getSectors().get("P02"));
		this.getSectors().get("Q02").getNearbyBoxes().put("Q01",this.getSectors().get("Q01"));
		this.getSectors().get("Q02").getNearbyBoxes().put("Q03",this.getSectors().get("Q03"));
		this.getSectors().get("Q02").getNearbyBoxes().put("R01",this.getSectors().get("R01"));
		this.getSectors().get("Q02").getNearbyBoxes().put("R02",this.getSectors().get("R02"));
		
		this.getSectors().get("Q03").getNearbyBoxes().put("P02",this.getSectors().get("P02"));
		this.getSectors().get("Q03").getNearbyBoxes().put("P03",this.getSectors().get("P03"));
		this.getSectors().get("Q03").getNearbyBoxes().put("Q02",this.getSectors().get("Q02"));
		this.getSectors().get("Q03").getNearbyBoxes().put("Q04",this.getSectors().get("Q04"));
		this.getSectors().get("Q03").getNearbyBoxes().put("R02",this.getSectors().get("R02"));
		this.getSectors().get("Q03").getNearbyBoxes().put("R03",this.getSectors().get("R03"));
		
		this.getSectors().get("Q04").getNearbyBoxes().put("P03",this.getSectors().get("P03"));
		this.getSectors().get("Q04").getNearbyBoxes().put("P04",this.getSectors().get("P04"));
		this.getSectors().get("Q04").getNearbyBoxes().put("Q03",this.getSectors().get("Q03"));
		this.getSectors().get("Q04").getNearbyBoxes().put("Q05",this.getSectors().get("Q05"));
		this.getSectors().get("Q04").getNearbyBoxes().put("R03",this.getSectors().get("R03"));
		this.getSectors().get("Q04").getNearbyBoxes().put("R04",this.getSectors().get("R04"));
		
		this.getSectors().get("Q05").getNearbyBoxes().put("P04",this.getSectors().get("P04"));
		this.getSectors().get("Q05").getNearbyBoxes().put("P05",this.getSectors().get("P05"));
		this.getSectors().get("Q05").getNearbyBoxes().put("Q04",this.getSectors().get("Q04"));
		this.getSectors().get("Q05").getNearbyBoxes().put("Q06",this.getSectors().get("Q06"));
		this.getSectors().get("Q05").getNearbyBoxes().put("R04",this.getSectors().get("R04"));
		this.getSectors().get("Q05").getNearbyBoxes().put("R05",this.getSectors().get("R05"));
		
		this.getSectors().get("Q06").getNearbyBoxes().put("P05",this.getSectors().get("P05"));
		this.getSectors().get("Q06").getNearbyBoxes().put("P06",this.getSectors().get("P06"));
		this.getSectors().get("Q06").getNearbyBoxes().put("Q05",this.getSectors().get("Q05"));
		this.getSectors().get("Q06").getNearbyBoxes().put("Q07",this.getSectors().get("Q07"));
		this.getSectors().get("Q06").getNearbyBoxes().put("R05",this.getSectors().get("R05"));
		this.getSectors().get("Q06").getNearbyBoxes().put("R06",this.getSectors().get("R06"));
		
		this.getSectors().get("Q07").getNearbyBoxes().put("P06",this.getSectors().get("P06"));
		this.getSectors().get("Q07").getNearbyBoxes().put("P07",this.getSectors().get("P07"));
		this.getSectors().get("Q07").getNearbyBoxes().put("Q06",this.getSectors().get("Q06"));
		this.getSectors().get("Q07").getNearbyBoxes().put("Q08",this.getSectors().get("Q08"));
		this.getSectors().get("Q07").getNearbyBoxes().put("R06",this.getSectors().get("R06"));
		this.getSectors().get("Q07").getNearbyBoxes().put("R07",this.getSectors().get("R07"));
		
		this.getSectors().get("Q08").getNearbyBoxes().put("P07",this.getSectors().get("P07"));
		this.getSectors().get("Q08").getNearbyBoxes().put("P08",this.getSectors().get("P08"));
		this.getSectors().get("Q08").getNearbyBoxes().put("Q07",this.getSectors().get("Q07"));
		this.getSectors().get("Q08").getNearbyBoxes().put("Q09",this.getSectors().get("Q09"));
		this.getSectors().get("Q08").getNearbyBoxes().put("R07",this.getSectors().get("R07"));
		this.getSectors().get("Q08").getNearbyBoxes().put("R08",this.getSectors().get("R08"));
		
		this.getSectors().get("Q09").getNearbyBoxes().put("P08",this.getSectors().get("P08"));
		this.getSectors().get("Q09").getNearbyBoxes().put("P09",this.getSectors().get("P09"));
		this.getSectors().get("Q09").getNearbyBoxes().put("Q08",this.getSectors().get("Q08"));
		this.getSectors().get("Q09").getNearbyBoxes().put("Q10",this.getSectors().get("Q10"));
		this.getSectors().get("Q09").getNearbyBoxes().put("R08",this.getSectors().get("R08"));
		this.getSectors().get("Q09").getNearbyBoxes().put("R09",this.getSectors().get("R09"));
		
		this.getSectors().get("Q10").getNearbyBoxes().put("P09",this.getSectors().get("P09"));
		this.getSectors().get("Q10").getNearbyBoxes().put("P10",this.getSectors().get("P10"));
		this.getSectors().get("Q10").getNearbyBoxes().put("Q09",this.getSectors().get("Q09"));
		this.getSectors().get("Q10").getNearbyBoxes().put("Q11",this.getSectors().get("Q11"));
		this.getSectors().get("Q10").getNearbyBoxes().put("R09",this.getSectors().get("R09"));

		this.getSectors().get("Q11").getNearbyBoxes().put("P10",this.getSectors().get("P10"));
		this.getSectors().get("Q11").getNearbyBoxes().put("P11",this.getSectors().get("P11"));
		this.getSectors().get("Q11").getNearbyBoxes().put("Q10",this.getSectors().get("Q10"));
		this.getSectors().get("Q11").getNearbyBoxes().put("Q12",this.getSectors().get("Q12"));

		this.getSectors().get("Q12").getNearbyBoxes().put("P11",this.getSectors().get("P11"));
		this.getSectors().get("Q12").getNearbyBoxes().put("P12",this.getSectors().get("P12"));
		this.getSectors().get("Q12").getNearbyBoxes().put("Q11",this.getSectors().get("Q11"));
		this.getSectors().get("Q12").getNearbyBoxes().put("Q13",this.getSectors().get("Q13"));
		this.getSectors().get("Q12").getNearbyBoxes().put("R12",this.getSectors().get("R12"));
		
		this.getSectors().get("Q13").getNearbyBoxes().put("P12",this.getSectors().get("P12"));
		this.getSectors().get("Q13").getNearbyBoxes().put("P13",this.getSectors().get("P13"));
		this.getSectors().get("Q13").getNearbyBoxes().put("Q12",this.getSectors().get("Q12"));
		this.getSectors().get("Q13").getNearbyBoxes().put("Q14",this.getSectors().get("Q14"));
		this.getSectors().get("Q13").getNearbyBoxes().put("R12",this.getSectors().get("R12"));
		this.getSectors().get("Q13").getNearbyBoxes().put("R13",this.getSectors().get("R13"));
		
		this.getSectors().get("Q14").getNearbyBoxes().put("P13",this.getSectors().get("P13"));
		this.getSectors().get("Q14").getNearbyBoxes().put("P14",this.getSectors().get("P14"));
		this.getSectors().get("Q14").getNearbyBoxes().put("Q13",this.getSectors().get("Q13"));
		this.getSectors().get("Q14").getNearbyBoxes().put("R13",this.getSectors().get("R13"));
		
		//adiacenti R
		this.getSectors().get("R01").getNearbyBoxes().put("Q01",this.getSectors().get("Q01"));
		this.getSectors().get("R01").getNearbyBoxes().put("Q02",this.getSectors().get("Q02"));
		this.getSectors().get("R01").getNearbyBoxes().put("R02",this.getSectors().get("R02"));
		this.getSectors().get("R01").getNearbyBoxes().put("S02",this.getSectors().get("S02"));
		
		this.getSectors().get("R02").getNearbyBoxes().put("Q02",this.getSectors().get("Q02"));
		this.getSectors().get("R02").getNearbyBoxes().put("Q03",this.getSectors().get("Q03"));
		this.getSectors().get("R02").getNearbyBoxes().put("R01",this.getSectors().get("R01"));
		this.getSectors().get("R02").getNearbyBoxes().put("R03",this.getSectors().get("R03"));
		this.getSectors().get("R02").getNearbyBoxes().put("S02",this.getSectors().get("S02"));

		this.getSectors().get("R03").getNearbyBoxes().put("Q03",this.getSectors().get("Q03"));
		this.getSectors().get("R03").getNearbyBoxes().put("Q04",this.getSectors().get("Q04"));
		this.getSectors().get("R03").getNearbyBoxes().put("R02",this.getSectors().get("R02"));
		this.getSectors().get("R03").getNearbyBoxes().put("R04",this.getSectors().get("R04"));
		this.getSectors().get("R03").getNearbyBoxes().put("S04",this.getSectors().get("S04"));
		
		this.getSectors().get("R04").getNearbyBoxes().put("Q04",this.getSectors().get("Q04"));
		this.getSectors().get("R04").getNearbyBoxes().put("Q05",this.getSectors().get("Q05"));
		this.getSectors().get("R04").getNearbyBoxes().put("R03",this.getSectors().get("R03"));
		this.getSectors().get("R04").getNearbyBoxes().put("R05",this.getSectors().get("R05"));
		this.getSectors().get("R04").getNearbyBoxes().put("S04",this.getSectors().get("S04"));
		this.getSectors().get("R04").getNearbyBoxes().put("S05",this.getSectors().get("S05"));
		
		this.getSectors().get("R05").getNearbyBoxes().put("Q05",this.getSectors().get("Q05"));
		this.getSectors().get("R05").getNearbyBoxes().put("Q06",this.getSectors().get("Q06"));
		this.getSectors().get("R05").getNearbyBoxes().put("R04",this.getSectors().get("R04"));
		this.getSectors().get("R05").getNearbyBoxes().put("R06",this.getSectors().get("R06"));
		this.getSectors().get("R05").getNearbyBoxes().put("S05",this.getSectors().get("S05"));
		this.getSectors().get("R05").getNearbyBoxes().put("S06",this.getSectors().get("S06"));
		
		this.getSectors().get("R06").getNearbyBoxes().put("Q06",this.getSectors().get("Q06"));
		this.getSectors().get("R06").getNearbyBoxes().put("Q07",this.getSectors().get("Q07"));
		this.getSectors().get("R06").getNearbyBoxes().put("R05",this.getSectors().get("R05"));
		this.getSectors().get("R06").getNearbyBoxes().put("R07",this.getSectors().get("R07"));
		this.getSectors().get("R06").getNearbyBoxes().put("S06",this.getSectors().get("S06"));
		this.getSectors().get("R06").getNearbyBoxes().put("S07",this.getSectors().get("S07"));
		
		this.getSectors().get("R07").getNearbyBoxes().put("Q07",this.getSectors().get("Q07"));
		this.getSectors().get("R07").getNearbyBoxes().put("Q08",this.getSectors().get("Q08"));
		this.getSectors().get("R07").getNearbyBoxes().put("R06",this.getSectors().get("R06"));
		this.getSectors().get("R07").getNearbyBoxes().put("R08",this.getSectors().get("R08"));
		this.getSectors().get("R07").getNearbyBoxes().put("S07",this.getSectors().get("S07"));
		this.getSectors().get("R07").getNearbyBoxes().put("S08",this.getSectors().get("S08"));
		
		this.getSectors().get("R08").getNearbyBoxes().put("Q08",this.getSectors().get("Q08"));
		this.getSectors().get("R08").getNearbyBoxes().put("Q09",this.getSectors().get("Q09"));
		this.getSectors().get("R08").getNearbyBoxes().put("R07",this.getSectors().get("R07"));
		this.getSectors().get("R08").getNearbyBoxes().put("R09",this.getSectors().get("R09"));
		this.getSectors().get("R08").getNearbyBoxes().put("S08",this.getSectors().get("S08"));
		this.getSectors().get("R08").getNearbyBoxes().put("S09",this.getSectors().get("S09"));
		
		this.getSectors().get("R09").getNearbyBoxes().put("Q09",this.getSectors().get("Q09"));
		this.getSectors().get("R09").getNearbyBoxes().put("Q10",this.getSectors().get("Q10"));
		this.getSectors().get("R09").getNearbyBoxes().put("R08",this.getSectors().get("R08"));
		this.getSectors().get("R09").getNearbyBoxes().put("S09",this.getSectors().get("S09"));

		this.getSectors().get("R12").getNearbyBoxes().put("Q12",this.getSectors().get("Q12"));
		this.getSectors().get("R12").getNearbyBoxes().put("Q13",this.getSectors().get("Q13"));
		this.getSectors().get("R12").getNearbyBoxes().put("R13",this.getSectors().get("R13"));
		this.getSectors().get("R12").getNearbyBoxes().put("S12",this.getSectors().get("S12"));
		this.getSectors().get("R12").getNearbyBoxes().put("S13",this.getSectors().get("S13"));
		
		this.getSectors().get("R13").getNearbyBoxes().put("Q13",this.getSectors().get("Q13"));
		this.getSectors().get("R13").getNearbyBoxes().put("Q14",this.getSectors().get("Q14"));
		this.getSectors().get("R13").getNearbyBoxes().put("R12",this.getSectors().get("R12"));
		this.getSectors().get("R13").getNearbyBoxes().put("S13",this.getSectors().get("S13"));
		
		//adiacenti S
		this.getSectors().get("S02").getNearbyBoxes().put("R01",this.getSectors().get("R01"));
		this.getSectors().get("S02").getNearbyBoxes().put("R02",this.getSectors().get("R02"));
		this.getSectors().get("S02").getNearbyBoxes().put("T02",this.getSectors().get("T02"));

		this.getSectors().get("S04").getNearbyBoxes().put("R03",this.getSectors().get("R03"));
		this.getSectors().get("S04").getNearbyBoxes().put("R04",this.getSectors().get("R04"));
		this.getSectors().get("S04").getNearbyBoxes().put("S05",this.getSectors().get("S05"));

		this.getSectors().get("S05").getNearbyBoxes().put("R04",this.getSectors().get("R04"));
		this.getSectors().get("S05").getNearbyBoxes().put("R05",this.getSectors().get("R05"));
		this.getSectors().get("S05").getNearbyBoxes().put("S04",this.getSectors().get("S04"));
		this.getSectors().get("S05").getNearbyBoxes().put("S06",this.getSectors().get("S06"));
		this.getSectors().get("S05").getNearbyBoxes().put("T05",this.getSectors().get("T05"));
		
		this.getSectors().get("S06").getNearbyBoxes().put("R05",this.getSectors().get("R05"));
		this.getSectors().get("S06").getNearbyBoxes().put("R06",this.getSectors().get("R06"));
		this.getSectors().get("S06").getNearbyBoxes().put("S05",this.getSectors().get("S05"));
		this.getSectors().get("S06").getNearbyBoxes().put("S07",this.getSectors().get("S07"));
		this.getSectors().get("S06").getNearbyBoxes().put("T05",this.getSectors().get("T05"));
		this.getSectors().get("S06").getNearbyBoxes().put("T06",this.getSectors().get("T06"));
		
		this.getSectors().get("S07").getNearbyBoxes().put("R06",this.getSectors().get("R06"));
		this.getSectors().get("S07").getNearbyBoxes().put("R07",this.getSectors().get("R07"));
		this.getSectors().get("S07").getNearbyBoxes().put("S06",this.getSectors().get("S06"));
		this.getSectors().get("S07").getNearbyBoxes().put("S08",this.getSectors().get("S08"));
		this.getSectors().get("S07").getNearbyBoxes().put("T06",this.getSectors().get("T06"));
		this.getSectors().get("S07").getNearbyBoxes().put("T07",this.getSectors().get("T07"));
		
		this.getSectors().get("S08").getNearbyBoxes().put("R07",this.getSectors().get("R07"));
		this.getSectors().get("S08").getNearbyBoxes().put("R08",this.getSectors().get("R08"));
		this.getSectors().get("S08").getNearbyBoxes().put("S07",this.getSectors().get("S07"));
		this.getSectors().get("S08").getNearbyBoxes().put("S09",this.getSectors().get("S09"));
		this.getSectors().get("S08").getNearbyBoxes().put("T07",this.getSectors().get("T07"));
		this.getSectors().get("S08").getNearbyBoxes().put("T08",this.getSectors().get("T08"));
		
		this.getSectors().get("S09").getNearbyBoxes().put("R08",this.getSectors().get("R08"));
		this.getSectors().get("S09").getNearbyBoxes().put("R09",this.getSectors().get("R09"));
		this.getSectors().get("S09").getNearbyBoxes().put("S08",this.getSectors().get("S08"));
		this.getSectors().get("S09").getNearbyBoxes().put("T08",this.getSectors().get("T08"));

		this.getSectors().get("S12").getNearbyBoxes().put("R12",this.getSectors().get("R12"));
		this.getSectors().get("S12").getNearbyBoxes().put("S13",this.getSectors().get("S13"));
		this.getSectors().get("S12").getNearbyBoxes().put("T11",this.getSectors().get("T11"));
		this.getSectors().get("S12").getNearbyBoxes().put("T12",this.getSectors().get("T12"));
		
		this.getSectors().get("S13").getNearbyBoxes().put("R12",this.getSectors().get("R12"));
		this.getSectors().get("S13").getNearbyBoxes().put("R13",this.getSectors().get("R13"));
		this.getSectors().get("S13").getNearbyBoxes().put("S12",this.getSectors().get("S12"));
		this.getSectors().get("S13").getNearbyBoxes().put("T12",this.getSectors().get("T12"));
		this.getSectors().get("S13").getNearbyBoxes().put("T13",this.getSectors().get("T13"));
		
		
		//adiacenti T
		this.getSectors().get("T02").getNearbyBoxes().put("S02",this.getSectors().get("S02"));
		this.getSectors().get("T02").getNearbyBoxes().put("U02",this.getSectors().get("U02"));
		this.getSectors().get("T02").getNearbyBoxes().put("U03",this.getSectors().get("U03"));
		
		this.getSectors().get("T05").getNearbyBoxes().put("S05",this.getSectors().get("S05"));
		this.getSectors().get("T05").getNearbyBoxes().put("S06",this.getSectors().get("S06"));
		this.getSectors().get("T05").getNearbyBoxes().put("T06",this.getSectors().get("T06"));
		this.getSectors().get("T05").getNearbyBoxes().put("U05",this.getSectors().get("U05"));
		this.getSectors().get("T05").getNearbyBoxes().put("U06",this.getSectors().get("U06"));
		
		this.getSectors().get("T06").getNearbyBoxes().put("S06",this.getSectors().get("S06"));
		this.getSectors().get("T06").getNearbyBoxes().put("S07",this.getSectors().get("S07"));
		this.getSectors().get("T06").getNearbyBoxes().put("T05",this.getSectors().get("T05"));
		this.getSectors().get("T06").getNearbyBoxes().put("T07",this.getSectors().get("T07"));
		this.getSectors().get("T06").getNearbyBoxes().put("U06",this.getSectors().get("U06"));
		this.getSectors().get("T06").getNearbyBoxes().put("U07",this.getSectors().get("U07"));
		
		this.getSectors().get("T07").getNearbyBoxes().put("S07",this.getSectors().get("S07"));
		this.getSectors().get("T07").getNearbyBoxes().put("S08",this.getSectors().get("S08"));
		this.getSectors().get("T07").getNearbyBoxes().put("T06",this.getSectors().get("T06"));
		this.getSectors().get("T07").getNearbyBoxes().put("T08",this.getSectors().get("T08"));
		this.getSectors().get("T07").getNearbyBoxes().put("U07",this.getSectors().get("U07"));
		this.getSectors().get("T07").getNearbyBoxes().put("U08",this.getSectors().get("U08"));
		
		this.getSectors().get("T08").getNearbyBoxes().put("S08",this.getSectors().get("S08"));
		this.getSectors().get("T08").getNearbyBoxes().put("S09",this.getSectors().get("S09"));
		this.getSectors().get("T08").getNearbyBoxes().put("T07",this.getSectors().get("T07"));
		this.getSectors().get("T08").getNearbyBoxes().put("U08",this.getSectors().get("U08"));
		this.getSectors().get("T08").getNearbyBoxes().put("U09",this.getSectors().get("U09"));
		
		this.getSectors().get("T11").getNearbyBoxes().put("S12",this.getSectors().get("S12"));
		this.getSectors().get("T11").getNearbyBoxes().put("T12",this.getSectors().get("T12"));
		this.getSectors().get("T11").getNearbyBoxes().put("U11",this.getSectors().get("U11"));
		this.getSectors().get("T11").getNearbyBoxes().put("U12",this.getSectors().get("U12"));
		
		this.getSectors().get("T12").getNearbyBoxes().put("S12",this.getSectors().get("S12"));
		this.getSectors().get("T12").getNearbyBoxes().put("S13",this.getSectors().get("S13"));
		this.getSectors().get("T12").getNearbyBoxes().put("T11",this.getSectors().get("T11"));
		this.getSectors().get("T12").getNearbyBoxes().put("T13",this.getSectors().get("T13"));
		this.getSectors().get("T12").getNearbyBoxes().put("U12",this.getSectors().get("U12"));
		this.getSectors().get("T12").getNearbyBoxes().put("U13",this.getSectors().get("U13"));
		
		this.getSectors().get("T13").getNearbyBoxes().put("S13",this.getSectors().get("S13"));
		this.getSectors().get("T13").getNearbyBoxes().put("T12",this.getSectors().get("T12"));
		this.getSectors().get("T13").getNearbyBoxes().put("T14",this.getSectors().get("T14"));
		this.getSectors().get("T13").getNearbyBoxes().put("U13",this.getSectors().get("U13"));
		this.getSectors().get("T13").getNearbyBoxes().put("U14",this.getSectors().get("U14"));
		
		this.getSectors().get("T14").getNearbyBoxes().put("T13",this.getSectors().get("T13"));
		this.getSectors().get("T14").getNearbyBoxes().put("U14",this.getSectors().get("U14"));
		
		//adiacenti U
		this.getSectors().get("U01").getNearbyBoxes().put("U02",this.getSectors().get("U02"));
		this.getSectors().get("U01").getNearbyBoxes().put("V01",this.getSectors().get("V01"));

		this.getSectors().get("U02").getNearbyBoxes().put("T02",this.getSectors().get("T02"));
		this.getSectors().get("U02").getNearbyBoxes().put("U01",this.getSectors().get("U01"));
		this.getSectors().get("U02").getNearbyBoxes().put("U03",this.getSectors().get("U03"));
		this.getSectors().get("U02").getNearbyBoxes().put("V01",this.getSectors().get("V01"));
		this.getSectors().get("U02").getNearbyBoxes().put("Z02",this.getSectors().get("Z02"));
		
		this.getSectors().get("U03").getNearbyBoxes().put("T02",this.getSectors().get("T02"));
		this.getSectors().get("U03").getNearbyBoxes().put("U02",this.getSectors().get("U02"));
		this.getSectors().get("U03").getNearbyBoxes().put("U04",this.getSectors().get("U04"));
		this.getSectors().get("U03").getNearbyBoxes().put("Z02",this.getSectors().get("Z02"));
		this.getSectors().get("U03").getNearbyBoxes().put("V03",this.getSectors().get("V03"));
		
		this.getSectors().get("U04").getNearbyBoxes().put("U03",this.getSectors().get("U03"));
		this.getSectors().get("U04").getNearbyBoxes().put("U05",this.getSectors().get("U05"));
		this.getSectors().get("U04").getNearbyBoxes().put("V03",this.getSectors().get("V03"));
		this.getSectors().get("U04").getNearbyBoxes().put("V04",this.getSectors().get("V04"));

		this.getSectors().get("U05").getNearbyBoxes().put("T05",this.getSectors().get("T05"));
		this.getSectors().get("U05").getNearbyBoxes().put("U04",this.getSectors().get("U04"));
		this.getSectors().get("U05").getNearbyBoxes().put("U06",this.getSectors().get("U06"));
		this.getSectors().get("U05").getNearbyBoxes().put("V04",this.getSectors().get("V04"));
		this.getSectors().get("U05").getNearbyBoxes().put("V05",this.getSectors().get("V05"));
		
		this.getSectors().get("U06").getNearbyBoxes().put("T05",this.getSectors().get("T05"));
		this.getSectors().get("U06").getNearbyBoxes().put("T06",this.getSectors().get("T06"));
		this.getSectors().get("U06").getNearbyBoxes().put("U05",this.getSectors().get("U05"));
		this.getSectors().get("U06").getNearbyBoxes().put("U07",this.getSectors().get("U07"));
		this.getSectors().get("U06").getNearbyBoxes().put("V05",this.getSectors().get("V05"));
		this.getSectors().get("U06").getNearbyBoxes().put("V06",this.getSectors().get("V06"));
		
		this.getSectors().get("U07").getNearbyBoxes().put("T06",this.getSectors().get("T06"));
		this.getSectors().get("U07").getNearbyBoxes().put("T07",this.getSectors().get("T07"));
		this.getSectors().get("U07").getNearbyBoxes().put("U06",this.getSectors().get("U06"));
		this.getSectors().get("U07").getNearbyBoxes().put("U08",this.getSectors().get("U08"));
		this.getSectors().get("U07").getNearbyBoxes().put("V06",this.getSectors().get("V06"));

		this.getSectors().get("U08").getNearbyBoxes().put("T07",this.getSectors().get("T07"));
		this.getSectors().get("U08").getNearbyBoxes().put("T08",this.getSectors().get("T08"));
		this.getSectors().get("U08").getNearbyBoxes().put("U07",this.getSectors().get("U07"));
		this.getSectors().get("U08").getNearbyBoxes().put("U09",this.getSectors().get("U09"));
		this.getSectors().get("U08").getNearbyBoxes().put("V08",this.getSectors().get("V08"));
		
		this.getSectors().get("U09").getNearbyBoxes().put("T08",this.getSectors().get("T08"));
		this.getSectors().get("U09").getNearbyBoxes().put("U08",this.getSectors().get("U08"));
		this.getSectors().get("U09").getNearbyBoxes().put("U10",this.getSectors().get("U10"));
		this.getSectors().get("U09").getNearbyBoxes().put("V08",this.getSectors().get("V08"));
		this.getSectors().get("U09").getNearbyBoxes().put("V09",this.getSectors().get("V09"));
		
		this.getSectors().get("U10").getNearbyBoxes().put("U09",this.getSectors().get("U09"));
		this.getSectors().get("U10").getNearbyBoxes().put("U11",this.getSectors().get("U11"));
		this.getSectors().get("U10").getNearbyBoxes().put("V09",this.getSectors().get("V09"));
		this.getSectors().get("U10").getNearbyBoxes().put("V10",this.getSectors().get("V10"));

		this.getSectors().get("U11").getNearbyBoxes().put("T11",this.getSectors().get("T11"));
		this.getSectors().get("U11").getNearbyBoxes().put("U10",this.getSectors().get("U10"));
		this.getSectors().get("U11").getNearbyBoxes().put("U12",this.getSectors().get("U12"));
		this.getSectors().get("U11").getNearbyBoxes().put("V10",this.getSectors().get("V10"));
		this.getSectors().get("U11").getNearbyBoxes().put("V11",this.getSectors().get("V11"));
		
		this.getSectors().get("U12").getNearbyBoxes().put("T11",this.getSectors().get("T11"));
		this.getSectors().get("U12").getNearbyBoxes().put("T12",this.getSectors().get("T12"));
		this.getSectors().get("U12").getNearbyBoxes().put("U11",this.getSectors().get("U11"));
		this.getSectors().get("U12").getNearbyBoxes().put("U13",this.getSectors().get("U13"));
		this.getSectors().get("U12").getNearbyBoxes().put("V11",this.getSectors().get("V11"));
		this.getSectors().get("U12").getNearbyBoxes().put("V12",this.getSectors().get("V12"));
		
		this.getSectors().get("U13").getNearbyBoxes().put("T12",this.getSectors().get("T12"));
		this.getSectors().get("U13").getNearbyBoxes().put("T13",this.getSectors().get("T13"));
		this.getSectors().get("U13").getNearbyBoxes().put("U12",this.getSectors().get("U12"));
		this.getSectors().get("U13").getNearbyBoxes().put("U14",this.getSectors().get("U14"));
		this.getSectors().get("U13").getNearbyBoxes().put("V12",this.getSectors().get("V12"));
		this.getSectors().get("U13").getNearbyBoxes().put("Z03",this.getSectors().get("Z03"));
		
		this.getSectors().get("U14").getNearbyBoxes().put("T13",this.getSectors().get("T13"));
		this.getSectors().get("U14").getNearbyBoxes().put("T14",this.getSectors().get("T14"));
		this.getSectors().get("U14").getNearbyBoxes().put("U13",this.getSectors().get("U13"));
		this.getSectors().get("U14").getNearbyBoxes().put("Z03",this.getSectors().get("Z03"));
		this.getSectors().get("U14").getNearbyBoxes().put("V14",this.getSectors().get("V14"));

		
		//adiacenti V
		this.getSectors().get("V01").getNearbyBoxes().put("U01",this.getSectors().get("U01"));
		this.getSectors().get("V01").getNearbyBoxes().put("U02",this.getSectors().get("U02"));
		this.getSectors().get("V01").getNearbyBoxes().put("Z02",this.getSectors().get("Z02"));
		this.getSectors().get("V01").getNearbyBoxes().put("W02",this.getSectors().get("W02"));
		
		this.getSectors().get("V03").getNearbyBoxes().put("U03",this.getSectors().get("U03"));
		this.getSectors().get("V03").getNearbyBoxes().put("U04",this.getSectors().get("U04"));
		this.getSectors().get("V03").getNearbyBoxes().put("Z02",this.getSectors().get("Z02"));
		this.getSectors().get("V03").getNearbyBoxes().put("V04",this.getSectors().get("V04"));
		this.getSectors().get("V03").getNearbyBoxes().put("W03",this.getSectors().get("W03"));
		this.getSectors().get("V03").getNearbyBoxes().put("W04",this.getSectors().get("W04"));
		
		this.getSectors().get("V04").getNearbyBoxes().put("U04",this.getSectors().get("U04"));
		this.getSectors().get("V04").getNearbyBoxes().put("U05",this.getSectors().get("U05"));
		this.getSectors().get("V04").getNearbyBoxes().put("V03",this.getSectors().get("V03"));
		this.getSectors().get("V04").getNearbyBoxes().put("V05",this.getSectors().get("V05"));
		this.getSectors().get("V04").getNearbyBoxes().put("W04",this.getSectors().get("W04"));
		this.getSectors().get("V04").getNearbyBoxes().put("W05",this.getSectors().get("W05"));
		
		this.getSectors().get("V05").getNearbyBoxes().put("U05",this.getSectors().get("U05"));
		this.getSectors().get("V05").getNearbyBoxes().put("U06",this.getSectors().get("U06"));
		this.getSectors().get("V05").getNearbyBoxes().put("V04",this.getSectors().get("V04"));
		this.getSectors().get("V05").getNearbyBoxes().put("V06",this.getSectors().get("V06"));
		this.getSectors().get("V05").getNearbyBoxes().put("W05",this.getSectors().get("W05"));
		this.getSectors().get("V05").getNearbyBoxes().put("W06",this.getSectors().get("W06"));
		
		this.getSectors().get("V06").getNearbyBoxes().put("U06",this.getSectors().get("U06"));
		this.getSectors().get("V06").getNearbyBoxes().put("U07",this.getSectors().get("U07"));
		this.getSectors().get("V06").getNearbyBoxes().put("V05",this.getSectors().get("V05"));
		this.getSectors().get("V06").getNearbyBoxes().put("W06",this.getSectors().get("W06"));
		
		this.getSectors().get("V08").getNearbyBoxes().put("U08",this.getSectors().get("U08"));
		this.getSectors().get("V08").getNearbyBoxes().put("U09",this.getSectors().get("U09"));
		this.getSectors().get("V08").getNearbyBoxes().put("V09",this.getSectors().get("V09"));
		this.getSectors().get("V08").getNearbyBoxes().put("W09",this.getSectors().get("W09"));
		
		this.getSectors().get("V09").getNearbyBoxes().put("U09",this.getSectors().get("U09"));
		this.getSectors().get("V09").getNearbyBoxes().put("U10",this.getSectors().get("U10"));
		this.getSectors().get("V09").getNearbyBoxes().put("V08",this.getSectors().get("V08"));
		this.getSectors().get("V09").getNearbyBoxes().put("V10",this.getSectors().get("V10"));
		this.getSectors().get("V09").getNearbyBoxes().put("W09",this.getSectors().get("W09"));
		this.getSectors().get("V09").getNearbyBoxes().put("W10",this.getSectors().get("W10"));
		
		this.getSectors().get("V10").getNearbyBoxes().put("U10",this.getSectors().get("U10"));
		this.getSectors().get("V10").getNearbyBoxes().put("U11",this.getSectors().get("U11"));
		this.getSectors().get("V10").getNearbyBoxes().put("V09",this.getSectors().get("V09"));
		this.getSectors().get("V10").getNearbyBoxes().put("V11",this.getSectors().get("V11"));
		this.getSectors().get("V10").getNearbyBoxes().put("W10",this.getSectors().get("W10"));
		this.getSectors().get("V10").getNearbyBoxes().put("W11",this.getSectors().get("W11"));
		
		this.getSectors().get("V11").getNearbyBoxes().put("U11",this.getSectors().get("U11"));
		this.getSectors().get("V11").getNearbyBoxes().put("U12",this.getSectors().get("U12"));
		this.getSectors().get("V11").getNearbyBoxes().put("V10",this.getSectors().get("V10"));
		this.getSectors().get("V11").getNearbyBoxes().put("V12",this.getSectors().get("V12"));
		this.getSectors().get("V11").getNearbyBoxes().put("W11",this.getSectors().get("W11"));
		this.getSectors().get("V11").getNearbyBoxes().put("W12",this.getSectors().get("W12"));
		
		this.getSectors().get("V12").getNearbyBoxes().put("U12",this.getSectors().get("U12"));
		this.getSectors().get("V12").getNearbyBoxes().put("U13",this.getSectors().get("U13"));
		this.getSectors().get("V12").getNearbyBoxes().put("V11",this.getSectors().get("V11"));
		this.getSectors().get("V12").getNearbyBoxes().put("Z03",this.getSectors().get("Z03"));
		this.getSectors().get("V12").getNearbyBoxes().put("W12",this.getSectors().get("W12"));
		this.getSectors().get("V12").getNearbyBoxes().put("W13",this.getSectors().get("W13"));

		this.getSectors().get("V14").getNearbyBoxes().put("U14",this.getSectors().get("U14"));
		this.getSectors().get("V14").getNearbyBoxes().put("Z03",this.getSectors().get("Z03"));
		this.getSectors().get("V14").getNearbyBoxes().put("W14",this.getSectors().get("W14"));

		//adiacenti W
		this.getSectors().get("W02").getNearbyBoxes().put("V01",this.getSectors().get("V01"));
		this.getSectors().get("W02").getNearbyBoxes().put("Z02",this.getSectors().get("Z02"));
		this.getSectors().get("W02").getNearbyBoxes().put("W03",this.getSectors().get("W03"));

		this.getSectors().get("W03").getNearbyBoxes().put("Z02",this.getSectors().get("Z02"));
		this.getSectors().get("W03").getNearbyBoxes().put("V03",this.getSectors().get("V03"));
		this.getSectors().get("W03").getNearbyBoxes().put("W02",this.getSectors().get("W02"));
		this.getSectors().get("W03").getNearbyBoxes().put("W04",this.getSectors().get("W04"));

		this.getSectors().get("W04").getNearbyBoxes().put("V03",this.getSectors().get("V03"));
		this.getSectors().get("W04").getNearbyBoxes().put("V04",this.getSectors().get("V04"));
		this.getSectors().get("W04").getNearbyBoxes().put("W03",this.getSectors().get("W03"));
		this.getSectors().get("W04").getNearbyBoxes().put("W05",this.getSectors().get("W05"));

		this.getSectors().get("W05").getNearbyBoxes().put("V04",this.getSectors().get("V04"));
		this.getSectors().get("W05").getNearbyBoxes().put("V05",this.getSectors().get("V05"));
		this.getSectors().get("W05").getNearbyBoxes().put("W04",this.getSectors().get("W04"));
		this.getSectors().get("W05").getNearbyBoxes().put("W06",this.getSectors().get("W06"));

		this.getSectors().get("W06").getNearbyBoxes().put("V05",this.getSectors().get("V05"));
		this.getSectors().get("W06").getNearbyBoxes().put("V06",this.getSectors().get("V06"));
		this.getSectors().get("W06").getNearbyBoxes().put("W05",this.getSectors().get("W05"));

		this.getSectors().get("W09").getNearbyBoxes().put("V08",this.getSectors().get("V08"));
		this.getSectors().get("W09").getNearbyBoxes().put("V09",this.getSectors().get("V09"));
		this.getSectors().get("W09").getNearbyBoxes().put("W10",this.getSectors().get("W10"));

		this.getSectors().get("W10").getNearbyBoxes().put("V09",this.getSectors().get("V09"));
		this.getSectors().get("W10").getNearbyBoxes().put("V10",this.getSectors().get("V10"));
		this.getSectors().get("W10").getNearbyBoxes().put("W09",this.getSectors().get("W09"));
		this.getSectors().get("W10").getNearbyBoxes().put("W11",this.getSectors().get("W11"));

		this.getSectors().get("W11").getNearbyBoxes().put("V10",this.getSectors().get("V10"));
		this.getSectors().get("W11").getNearbyBoxes().put("V11",this.getSectors().get("V11"));
		this.getSectors().get("W11").getNearbyBoxes().put("W10",this.getSectors().get("W10"));
		this.getSectors().get("W11").getNearbyBoxes().put("W12",this.getSectors().get("W12"));

		this.getSectors().get("W12").getNearbyBoxes().put("V11",this.getSectors().get("V11"));
		this.getSectors().get("W12").getNearbyBoxes().put("V12",this.getSectors().get("V12"));
		this.getSectors().get("W12").getNearbyBoxes().put("W11",this.getSectors().get("W11"));
		this.getSectors().get("W12").getNearbyBoxes().put("W13",this.getSectors().get("W13"));

		this.getSectors().get("W13").getNearbyBoxes().put("V12",this.getSectors().get("V12"));
		this.getSectors().get("W13").getNearbyBoxes().put("Z03",this.getSectors().get("Z03"));
		this.getSectors().get("W13").getNearbyBoxes().put("W12",this.getSectors().get("W12"));
		this.getSectors().get("W13").getNearbyBoxes().put("W14",this.getSectors().get("W14"));

		this.getSectors().get("W14").getNearbyBoxes().put("Z03",this.getSectors().get("Z03"));
		this.getSectors().get("W14").getNearbyBoxes().put("V14",this.getSectors().get("V14"));
		this.getSectors().get("W14").getNearbyBoxes().put("W13",this.getSectors().get("W13"));
		
	}

	public Map<String, BoxSingleton> getSectors() {
		return sectors;
	}

	public void setSectors(Map<String, BoxSingleton> settori) {
		this.sectors = settori;
	}
}
