package it.polimi.ingsw.colucciacapoferri.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;


public class ListenerSeverSide implements Runnable{
		
	/** The server socket. */
	private ServerSocket serverSocket;
		
	/** The t. */
	private Thread t;

	/**Current Game*/
	private Game game;
	
	public ListenerSeverSide(Game game){
		this.game=game;
	}
	
	
	public void run(){
	   try {
		   serverSocket = new ServerSocket(Constants.PORTSOCKET);
	
		   while(true){
			   Socket clientSocket = serverSocket.accept();
			   ConnectionToClient client = new ConnectionToClient(clientSocket);
			   if(clientSocket!=null && clientSocket.isConnected()){
				   ConnectionListener cl = new ConnectionListener(client, getGame());
				   t = new Thread(cl);
				   t.start();
			   }
		   }
	   } catch (UnknownHostException e) {
		   Server.getLogfile().warning(e.toString());
	   } catch (IOException e) {
		Server.getLogfile().warning(e.toString());
	   }
	}


	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}
}
