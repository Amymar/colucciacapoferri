package it.polimi.ingsw.colucciacapoferri.controller;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.Color;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.Humans;
import it.polimi.ingsw.colucciacapoferri.model.GameBoard;
import it.polimi.ingsw.colucciacapoferri.model.character_card.AlienCharacter;
import it.polimi.ingsw.colucciacapoferri.model.character_card.HumanCharacter;
import it.polimi.ingsw.colucciacapoferri.model.character_card.InterfaceCharacterCard;
import it.polimi.ingsw.colucciacapoferri.model.object.AdrenalineObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.AttackObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.DefenceObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.LightObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.SedativesObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.object.TeleportObjectCard;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.FakeEmptySectorCard;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.FakeFraughtSectorCard;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.InterfaceCardSector;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.RealEmptySectorCard;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.RealFraughtSectorCard;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.SilenceSectorCard;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import model.BoatCard;

public class GamingData {
	private GameBoard gameBoard;
	private Game game;
	private List <InterfaceCardSector> interfaceCardSectors= new  LinkedList<InterfaceCardSector>();
	private List<InterfaceObjectCard> interfaceObjectCards= new LinkedList <InterfaceObjectCard>();
	private List <BoatCard> boatCards= new LinkedList <BoatCard> ();
	private Map <String, InterfaceCharacterCard> humans=new TreeMap <String, InterfaceCharacterCard>();
	private Map <String, InterfaceCharacterCard> aliens=new TreeMap <String, InterfaceCharacterCard>();

	public GamingData(){
		this.gameBoard=new GameBoard();
		creatingSectorCards();
		creatingObjectCards();
		creatingBoatCards();
		creatingPgCards();
	}
	
	public GamingData(Game game){

		//inizzializzo mappa
		this.gameBoard=new GameBoard();
		this.game=game;

		creatingSectorCards();
		creatingObjectCards();
		creatingBoatCards();
		creatingPgCards();
	}

		//inizzializzo Carte settore
	public void creatingSectorCards(){
		Map <String,InterfaceCardSector> cards= new TreeMap <String,InterfaceCardSector>();
		InterfaceCardSector interfaceCardSector;
		for(int  i=0; i<(Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE);i++){
			interfaceCardSector=new FakeEmptySectorCard();
			interfaceCardSector.getThis("FF"+i, cards);
			interfaceCardSector= new FakeFraughtSectorCard();
			interfaceCardSector.getThis("FT"+i, cards);
			interfaceCardSector=new SilenceSectorCard();
			interfaceCardSector.getThis("S"+i, cards);
			interfaceCardSector=new RealFraughtSectorCard();
			interfaceCardSector.getThis("RT"+i, cards);
			interfaceCardSector=new RealEmptySectorCard();
			interfaceCardSector.getThis("RF"+i, cards);
		}		
		Random ran =new Random();
		List<String> keySet =new ArrayList<String>(cards.keySet());
		String randomKey;
		while (keySet.size()!=0){
			randomKey= keySet.get(ran.nextInt(keySet.size()));
			getSectorCards().add(cards.get(randomKey));
			cards.remove(randomKey);
			int i=keySet.indexOf(randomKey);
			keySet.remove(i);
		}
		
	}


		//inizzializzo carte oggetto
	private void creatingObjectCards(){
		Map<String, InterfaceObjectCard> ob= new TreeMap<String, InterfaceObjectCard>();
		InterfaceObjectCard interfaceObjectCard=null;
		for (int j=0; j<(Constants.NOBJECTCARDS/Enumerations.ObjectCardType.values().length);j++){
			interfaceObjectCard=new AttackObjectCard();
			interfaceObjectCard.getThis(Enumerations.ObjectCardType.ATTACK.toString()+j, ob);
			interfaceObjectCard= new TeleportObjectCard();
			interfaceObjectCard.getThis(Enumerations.ObjectCardType.TELEPORT.toString()+j, ob);
			interfaceObjectCard=new SedativesObjectCard();
			interfaceObjectCard.getThis(Enumerations.ObjectCardType.SEDATIVES.toString()+j, ob);
			interfaceObjectCard=new LightObjectCard();
			interfaceObjectCard.getThis(Enumerations.ObjectCardType.LIGHT.toString()+j, ob);
			interfaceObjectCard=new DefenceObjectCard();
			interfaceObjectCard.getThis(Enumerations.ObjectCardType.DEFENCE.toString()+j, ob);
			interfaceObjectCard=new AdrenalineObjectCard();
			interfaceObjectCard.getThis(Enumerations.ObjectCardType.ADRENALINE.toString()+j, ob);
		}
		Random ran =new Random();
		List<String> keySet =new ArrayList<String>(ob.keySet());
		String randomKey;
		while (!keySet.isEmpty()){
			randomKey= keySet.get(ran.nextInt(keySet.size()));
			getObjectCards().add(ob.get(randomKey));
			ob.remove(randomKey);
			int i=keySet.indexOf(randomKey);
			keySet.remove(i);
		}
	}



		//inizzializzo carte scialuppa
		//assicurandomi che almeno esista la possibilità
		//che in caso di 8 giocatori tutti e quattro gli umani possano scappare
	private void creatingBoatCards(){
		Map<String, BoatCard> card= new TreeMap <String, BoatCard>();
		BoatCard boat=null;
		for(Integer w=0; w<Constants.NBOATCARDS;w++){
			if(w<Constants.NBOATCARDS-Constants.NBOATS){
				boat=new BoatCard(w.toString(),Color.RED);
			}else{
				boat=new BoatCard(w.toString(),Color.GREEN);
			}
			card.put(w.toString(), boat);
		}
		Random ran =new Random();
		List<String> keySet =new ArrayList<String>(card.keySet());
		String randomKey;
		while (keySet.size()!=0){
			randomKey= keySet.get(ran.nextInt(keySet.size()));
			getBoatCards().add(card.get(randomKey));
			card.remove(randomKey);
			int i=keySet.indexOf(randomKey);
			keySet.remove(i);
		}
	}



		//inizzializzo carte personaggio
		// e le posiziono nelle rispettive basi
	private void creatingPgCards(){
		InterfaceCharacterCard personaggio;
		for(Humans per : Enumerations.Humans.values()) {
			personaggio=new HumanCharacter(per.toString(),getGameBoard().getSectors().get("XU"));
			getHumans().put(per.toString(), personaggio);
		}
		for(Enumerations.Aliens al: Enumerations.Aliens.values()){
			personaggio=new AlienCharacter(al.toString(), getGameBoard().getSectors().get("XA"));
			getAliens().put(al.toString(), personaggio);
		}
	}
	public GameBoard getGameBoard() {
		return gameBoard;
	}
	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public List<InterfaceCardSector> getSectorCards() {
		return interfaceCardSectors;
	}
	public void setSectorCards(List<InterfaceCardSector> carteSettore) {
		this.interfaceCardSectors = carteSettore;
	}
	public List<InterfaceObjectCard> getObjectCards() {
		return interfaceObjectCards;
	}
	public void setObjectCards(List<InterfaceObjectCard> carteOggetto) {
		this.interfaceObjectCards = carteOggetto;
	}
	public List<BoatCard> getBoatCards() {
		return boatCards;
	}
	public void setBoatCards(List<BoatCard> carteScialuppa) {
		this.boatCards = carteScialuppa;
	}
	public Map<String, InterfaceCharacterCard> getHumans() {
		return humans;
	}
	public void setHumans(Map<String, InterfaceCharacterCard> umani) {
		this.humans = umani;
	}
	public Map<String, InterfaceCharacterCard> getAliens() {
		return aliens;
	}
	public void setAliens(Map<String, InterfaceCharacterCard> alieni) {
		this.aliens = alieni;
	}
}
