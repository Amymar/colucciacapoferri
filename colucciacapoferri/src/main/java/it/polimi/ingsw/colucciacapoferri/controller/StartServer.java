 package it.polimi.ingsw.colucciacapoferri.controller;

 /**
  * Class that can instantiate multiple servers 
  * @author Marilena Coluccia
  *
  */
public class StartServer {

	/**
	 * 
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
//		while (true) {
			Server server=new Server();
			server.start();
			server.join();
			System.gc();
		}
				
//	}

}
