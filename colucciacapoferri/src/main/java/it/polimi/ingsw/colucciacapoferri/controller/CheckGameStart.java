package it.polimi.ingsw.colucciacapoferri.controller;

import java.util.Date;

public class CheckGameStart implements Runnable {
	
		
	public void run() {
		while (true){
			Long time=new Date().getTime();
			if(Server.getGame().getPlayers().size()>Constants.MINPLAYERS && (Server.getGame().getGameId()+Constants.STARTGAME-time)<0){
				Game game =new Game();
				if(Server.getGame().getInGamePlayer().size()>Constants.MAXPLAYERS){
					for(int i=Server.getGame().getInGamePlayer().size()-1; i!=Constants.MAXPLAYERS;i--){
						game.getPlayers().put(Server.getGame().getInGamePlayer().get(i).getNickName(), Server.getGame().getInGamePlayer().get(i));
						game.getInGamePlayer().add(Server.getGame().getInGamePlayer().get(i));
						Server.getGame().getInGamePlayer().remove(i);	
					}					
				}
				System.out.println("game started");
				Thread gameThread =new Thread (Server.getGame());
				gameThread.start();
				Server.getGamesOn().put(Server.getGame().getGameId(), Server.getGame());
				Server.setGame(game);
			}
		}

	}

}
