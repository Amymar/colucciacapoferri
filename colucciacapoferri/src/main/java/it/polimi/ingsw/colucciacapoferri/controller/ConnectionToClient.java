package it.polimi.ingsw.colucciacapoferri.controller;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.rmi.Remote;
	
public class ConnectionToClient {
	
	
	private Socket clientSocket;
//	private RmiRemote clientRmi;
	private BufferedReader in;
	private PrintWriter out;
	
	/**
	 * Instantiates a new connection client.
	 *
	 * @param socket the socket
	 * @param stub the stub
	 * @throws IOException 
	 */
	
	public ConnectionToClient(Socket socket) {
		if(socket!=null){
			this.clientSocket=socket;
//			this.clientRmi=null;
		}
		this.in= new BufferedReader(new InputStreamReader(this.getInputStream()));
		this.out= new PrintWriter(this.getOutputStream(),true);
	}
	
//	public ConnectionToClient(RmiStub clientRmi){
//		if(clientRmi!=null){
//			this.clientSocket=null;
//			this.clientRmi=stub;
//		}
//		this.in= new BufferedReader(new InputStreamReader(this.getInputStream()));
//		this.out= new PrintWriter(this.getOutputStream(),true);
//	}
	
	/**
	 * Gets the output stream.
	 *
	 * @return the output stream
	 * @throws IOException 
	 */
	public OutputStream getOutputStream() {
		try {
			if(clientSocket!=null){
				return clientSocket.getOutputStream();
							
//		}else{
//			return clientRmi.getOutputStream();
			}
		} catch (IOException e) {
			Server.getLogfile().warning(e.toString());
		}
		return null;
	}
	
	/**
	 * Gets the input stream.
	 *
	 * @return the input stream
	 * @throws IOException 
	 */
	public InputStream getInputStream(){
		try {
			if(clientSocket!=null){
				
					return clientSocket.getInputStream();
				
//				}else{
//					return clientRmi.getInputStream();
				}
			} catch (IOException e) {
				Server.getLogfile().warning(e.toString());
				
				}
		return null;
	}

	/**
	 * Gets the client socket.
	 *
	 * @return the client socket
	 */
	public Socket getClientSocket() {
		return clientSocket;
	}

	/**
	 * Sets the client socket.
	 *
	 * @param clientSocket the new client socket
	 */
	public void setClientSocket(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	/**
	 * Gets the client rmi.
	 *
	 * @return the client rmi
	 */
//	public RmiRemote getClientRmi() {
//		return clientRmi;
//	}
//
	/**
	 * Sets the client rmi.
	 *
	 * @param clientRmi the new client rmi
	 */
//	public void setClientRmi(RmiRemote clientRmi) {
//		this.clientRmi = clientRmi;
//	}

	/**
	 * Gets the in.
	 *
	 * @return the in
	 */
	public BufferedReader getIn() {
		return in;
	}

	/**
	 * Sets the in.
	 *
	 * @param in the new in
	 */
	public void setIn(BufferedReader in) {
		this.in = in;
	}

	/**
	 * Gets the out.
	 *
	 * @return the out
	 */
	public PrintWriter getOut() {
		return out;
	}

	/**
	 * Sets the out.
	 *
	 * @param out the new out
	 */
	public void setOut(PrintWriter out) {
		this.out = out;
	}

}
