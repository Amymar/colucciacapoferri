package it.polimi.ingsw.colucciacapoferri.model.boxes;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.Ground;

import java.util.Map;

public interface BoxSingleton {
	
	public  BoxSingleton getThis(String name, Enumerations.Ground tipo, Map<String, BoxSingleton> all);
	public String getId ();
	public Ground getType();
	public Map<String, BoxSingleton> getNearbyBoxes();
	
}
