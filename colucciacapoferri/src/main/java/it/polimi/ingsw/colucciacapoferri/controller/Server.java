package it.polimi.ingsw.colucciacapoferri.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

public class Server extends Thread {
	/**
	 * Id Server
	 */
	private String idServer;
	
	/**
	 * Logger for exception
	 */
	private static Logger logfile=Logger.getLogger("logfile");
	/**
	 * Set of matches still in play
	 */
	private static Map<Long, Game> gamesOn=new TreeMap <Long, Game>();
	
	/**
	 * Set of players still in a game
	 */
	private static Map<String, Player> players=new TreeMap <String, Player>();
	
	/**
	 * Set  that maps each player to the game when playing 
	 */
	private static Map<String, Game> playerGame=new TreeMap<String, Game>();
	/**
	 * Thread List
	 */
	private List <Thread>threadList;
	/**
	 * Game instance 
	 */
	private static Game game;
	
	
	public Server(){
		this.idServer="Milano";
		Server.game=Server.getGame();
		this.threadList=new ArrayList <Thread>();
	}
	
	@Override
	public void run(){
				
		Server.logfile.info("Server "+getIdServer()+" is UP");
		
		Thread lss= new Thread(new ListenerSeverSide(getGame()));
		getThreadList().add(lss);
		lss.start();
		
		Thread cgs= new Thread(new CheckGameStart());
		getThreadList().add(cgs);
		cgs.start();
		
		Thread reset=new Thread(new ResetServer(getThreadList()));
		reset.start();
	
		try {
			reset.join();
		} catch (InterruptedException e) {
			Server.getLogfile().warning(e.toString());
		}
		return;
	}

	private class ResetServer implements Runnable{

		long time;
		List <Thread>threadList;
		
		public ResetServer(List <Thread> threadList){
			this.threadList=threadList;
			this.time=new Date().getTime();			
		}
		public void run() {
			while(true){
				
				if(getTime()%Constants.RESTART==0){
					for(int i=0;i<getThreadList().size();i++){
						getThreadList().get(i).interrupt();
					}
					return;
				}else if(getTime()%Constants.RESTARTHALF==0){
					String line="Riavvio del Server tra 30 min#";
					for(Player player:Server.getPlayers().values()){
						player.getConnection().getOut().print(line);
					}
				}else if(getTime()%Constants.RESTARTFIVE==0){
					String line="Riavvio del Server tra 5 min#";
					for(Player player:Server.getPlayers().values()){
						player.getConnection().getOut().print(line);
					}
				}
			}	
		}
		public long getTime() {
			return time;
		}
		
		public void setTime(long time) {
			this.time = time;
		}
		public List<Thread> getThreadList() {
			return threadList;
		}
		
		public void setThreadList(List<Thread> threadList) {
			this.threadList = threadList;
		}	
	}

	public String getIdServer() {
		return idServer;
	}

	public void setIdServer(String idServer) {
		this.idServer = idServer;
	}

	public static Logger getLogfile() {
		return logfile;
	}

	public static void setLogfile(Logger logfile) {
		Server.logfile = logfile;
	}

	public static Map<Long, Game> getGamesOn() {
		return gamesOn;
	}

	public static void setGamesOn(Map<Long, Game> gamesOn) {
		Server.gamesOn = gamesOn;
	}

	public static Map<String, Player> getPlayers() {
		return players;
	}

	public static void setPlayers(Map<String, Player> players) {
		Server.players = players;
	}

	public static Map<String, Game> getPlayerGame() {
		return playerGame;
	}

	public static void setPlayerGame(Map<String, Game> playerGame) {
		Server.playerGame = playerGame;
	}

	public List<Thread> getThreadList() {
		return threadList;
	}

	public void setThreadList(List<Thread> threadList) {
		this.threadList = threadList;
	}

	public static Game getGame() {
		if(Server.game==null){
			Server.game=new Game();
			
		}
		return game;
	}

	public static void setGame(Game game) {
		Server.game = game;
	}
	
	public static void startGame(){
		Thread game =new Thread (Server.game);
		game.start();
		getGamesOn().put(Server.game.getGameId(), Server.game);
		
		
	}

	}
