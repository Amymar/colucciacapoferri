package it.polimi.ingsw.colucciacapoferri.controller;

import it.polimi.ingsw.colucciacapoferri.model.character_card.*;

public class Player {
	String nickName;
	ConnectionToClient connection;
	InterfaceCharacterCard character;
	boolean victory=false;
	boolean death=false;
	
	public Player(){
		
	}
	
	public Player(ConnectionToClient connection){
		this.connection=connection;
	}
	
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nome) {
		nickName = nome;
	}
	public ConnectionToClient getConnection() {
		return connection;
	}
	public void setConnection(ConnectionToClient connessione) {
		this.connection = connessione;
	}
	public InterfaceCharacterCard getCharacter() {
		return character;
	}
	public void setCharacter(InterfaceCharacterCard personaggio) {
		this.character = personaggio;
	}
	public boolean isVictory() {
		return victory;
	}
	public void setVictory(boolean vittoria) {
		this.victory = vittoria;
	}
	public boolean isDeath() {
		return death;
	}
	public void setDeath(boolean morte) {
		this.death = morte;
	}
	
	
}
