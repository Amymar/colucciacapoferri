package it.polimi.ingsw.colucciacapoferri.model.sectorCard;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.SectorCardType;

import java.util.Map;

public class FakeFraughtSectorCard implements InterfaceCardSector {
	private String id;
	private static SectorCardType type = SectorCardType.FAKE;
	private static boolean object=true;
	
	public FakeFraughtSectorCard(){
		
	}
	
	public FakeFraughtSectorCard(String id) {
		this.setId(id);
	}

	public InterfaceCardSector getThis(String id, Map <String, InterfaceCardSector> map ) {
		if(map.containsKey(id)){
			return map.get(id);
		}else{
			InterfaceCardSector card = new FakeFraughtSectorCard(id);
			map.put(id, card);
			return card;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public SectorCardType getType() {
		return type;
	}

	public void setType(SectorCardType type) {
		FakeFraughtSectorCard.type = type;
	}

	public boolean isObject() {
		return object;
	}

	public void setObject(boolean object) {
		FakeFraughtSectorCard.object = object;
	}
}
