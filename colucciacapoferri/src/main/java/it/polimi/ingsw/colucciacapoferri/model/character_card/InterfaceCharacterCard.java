package it.polimi.ingsw.colucciacapoferri.model.character_card;

import it.polimi.ingsw.colucciacapoferri.controller.Round;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.Race;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoxSingleton;
import it.polimi.ingsw.colucciacapoferri.model.object.InterfaceObjectCard;

import java.util.List;
import java.util.Map;

public interface InterfaceCharacterCard {
	
	public InterfaceCharacterCard getThis(String name,BoxSingleton box, Map <String,InterfaceCharacterCard >map);
	public String getName() ;	
	public void setName(String name) ;
	public Race getRace();
	public void setRace(Race race);
	public BoxSingleton getPosition();
	public void setPosition(BoxSingleton boxSingleton);
	public List<Round> getOldRounds();
	public void setOldRounds(List<Round> oldRounds);
	public List<InterfaceObjectCard> getObjects();
	public void setObjects(List<InterfaceObjectCard> objects);
	public boolean isEaten();
	public void setEaten(boolean eaten);
}
