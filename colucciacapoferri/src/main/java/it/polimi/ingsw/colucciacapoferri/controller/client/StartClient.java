package it.polimi.ingsw.colucciacapoferri.controller.client;

import it.polimi.ingsw.colucciacapoferri.controller.Constants;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Logger;

public class StartClient {
	private Logger log=Logger.getLogger("log");
	private Socket socket;

	public static void main(String[] args) {
		StartClient client=new StartClient();
		try { 
			client.setSocket(new Socket(InetAddress.getLocalHost(), Constants.PORTSOCKET));
		} catch (UnknownHostException e) {
			client.getLog().warning(e.toString());
		} catch (IOException e) {
			client.getLog().warning(e.toString());
		}
		
		boolean gui=false;
		boolean check=true;
		System.out.println("Benvenuto su 'Fuga dagli alieni'\n ");
		Scanner in = new Scanner(System.in);
		while(check){
			System.out.println("Preferisci passare alla modalità grafica? :[Y] si o [N] no");
			String i=in.nextLine();
			if(i.equals("y")||i.equals("Y")){
				gui=true;
				check=false;
			}else if (i.equals("n")||i.equals("N")){
				gui=false;
				check=false;
			}
		}
		
		client.startGame(gui, client.getSocket(), client.getLog());
	}

	public Logger getLog() {
		return log;
	}

	public void setLog(Logger log) {
		this.log = log;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	private void startGame(boolean gui, Socket socket, Logger log){
		
		
		if (gui==false && socket.isConnected()){
			Thread cliLaunch=new Thread (new ListenerClientSide(socket, log));
			cliLaunch.start();
		}
	}
}
