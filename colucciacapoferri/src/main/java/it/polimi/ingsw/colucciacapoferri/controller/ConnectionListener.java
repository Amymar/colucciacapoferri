package it.polimi.ingsw.colucciacapoferri.controller;

import java.io.IOException;
import java.util.Date;

/**
	 * 
	 */
public class ConnectionListener implements Runnable{
		
		/** The client. */
		private ConnectionToClient client;
		
		/** The Player. */
		private Player player;
		
		/**Current Game*/
		private Game game;
		
		/**
		 * Instantiates a new connection listener.
		 *
		 * @param client the client
		 */
		public ConnectionListener (ConnectionToClient client, Game game){
			this.client=client;
			this.game=game;
		}

		/**
		 * Gets the player.
		 *
		 * @return the player
		 */
		public Player getPlayer() {
			return player;
		}

		/**
		 * Gets the client.
		 *
		 * @return the client
		 */
		public ConnectionToClient getClient() {
			return client;
		}

		/**
		 * Sets the client.
		 *
		 * @param client the client to set
		 */
		public void setClient(ConnectionToClient client) {
			this.client = client;
		}

		/**
		 * Sets the player.
		 *
		 * @param player the player to set
		 */
		public void setPlayer(Player player) {
			this.player = player;
		}
		
		/**
		 * Checks if is socket connection.
		 *
		 * @return true, if is socket connection
		 */
//		private boolean isSocketConnection(){
//			return getClient().getClientSocket()!=null && getClient().getClientRmi()==null;
//		}
		
		public Game getGame() {
			return game;
		}

		public void setGame(Game game) {
			this.game = game;
		}

		/**
		 * Checks if is rmi connection.
		 *
		 * @return true, if is rmi connection
		 */
//		private boolean isRmiConnection(){
//			return getClient().getClientRmi()!=null && getClient().getClientSocket()==null;
//		}

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		
		
		public void run(){
		   try {
			   String idUser=getClient().getIn().readLine();
//			   for(Player player: Server.getPlayers().values()){
//				   if(player.getNickName().equals(idUser)){
//					   Server.getPlayerGame().get(idUser).getPlayers().get(idUser).setConnection(getClient());
//					   return;
//				   }
//			   }
			   boolean sem=true;
			   while (sem && !Server.getPlayers().isEmpty()){
				   for(String name: Server.getPlayers().keySet()){
					   if(idUser.equals(name)){
						   getClient().getOut().print("Nome già in uso.\n Digitare nuovo nick\n");
						   idUser=getClient().getIn().readLine();
						   sem=true;
					   }else{
						   sem=false;
					   }
				   }
			   }
			   setPlayer(new Player(getClient()));
			   getPlayer().setNickName(idUser);
			   getGame().getPlayers().put(idUser, getPlayer());
			   getGame().getInGamePlayer().add(getPlayer());
			   Server.getPlayerGame().put(idUser, getGame());
			   if(getGame().getPlayers().size()==1){
				   getGame().setGameId(new Date().getTime());
			   }
			   //TODO controllare se manca qualcosa
		   } catch (IOException e) {
			   Server.getLogfile().warning(e.toString());
		}
		   
		}
}
