package it.polimi.ingsw.colucciacapoferri.model.sectorCard;


import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;

import java.util.Map;
public class RealFraughtSectorCard implements InterfaceCardSector {


	private String id;
	private static SectorCardType type = SectorCardType.REAL;
	private static boolean object=true;
	
	public RealFraughtSectorCard(){
		
	}
	
	public RealFraughtSectorCard(String id) {
		this.setId(id);
	}

	public InterfaceCardSector getThis(String id, Map <String, InterfaceCardSector> map ) {
		if(map.containsKey(id)){
			return map.get(id);
		}else{
			InterfaceCardSector card = new RealFraughtSectorCard(id);
			map.put(id, card);
			return card;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public  SectorCardType getType() {
		return type;
	}

	public  void setType(SectorCardType type) {
		RealFraughtSectorCard.type = type;
	}

	public  boolean isObject() {
		return object;
	}

	public  void setObject(boolean object) {
		RealFraughtSectorCard.object = object;
	}

	

	
}
