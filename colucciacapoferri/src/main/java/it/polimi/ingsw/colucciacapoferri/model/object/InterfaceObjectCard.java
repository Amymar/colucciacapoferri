package it.polimi.ingsw.colucciacapoferri.model.object;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.ObjectCardType;

import java.util.Map;

public interface InterfaceObjectCard {
	
	public InterfaceObjectCard getThis(String id, Map<String, InterfaceObjectCard>map);
	public String getId();
	public void setId(String id);
	public ObjectCardType getType() ;
	public void setType(ObjectCardType type) ;

}
