package it.polimi.ingsw.colucciacapoferri.controller;

public class Constants {
	/**
	 * How many times an Aline can move
	 */	
	public static final int ALIENMOVEMENT=2;
	/**
	 * Number boat cards present at the start of the match
	 */
	public static final int NBOATCARDS =6;
	
	/**
	 * Number BoatBox in GameBoard
	 */
	public static final int NBOATS= 4;
	
	/**
	 * maximum number of items per player
	 */
	public static final int NMAXOBJECTS=3;
	
	/**
	 * maximum number of rounds
	 */
	public static final int NMAXROUNDS=39;
	
	/**
	 * Number item cards present at the start of the match
	 */
	public static final int NOBJECTCARDS=12;
	
	/**
	 * Number type of item cards
	 */
	public static final int NOBJECTTYPE=6;
	
	/**
	 * Number sector cards present at the start of the match
	 */
	public static final int NSECTORCARDS =25;
	
	
	/**
	 * Number type of sector cards
	 */
	public static final int NSECTORCARDSTYPE=5;
	
	/**
	 * Socket port
	 */
	public static final int PORTSOCKET= 1025;
	
	/**
	 * Restart server time (1000*seconds*minutes*hours)
	 */
	public static final long RESTART= 8640000;
	
	/**
	 * Restart server time warning to client
	 */
	public static final long RESTARTHALF=Constants.RESTART-(1000*60*30);
	
	/**
	 * Restart server time warning to client
	 */
	public static final long RESTARTFIVE=Constants.RESTART-(1000*60*5);
	
	/**
	 * Start game time
	 */
	public static final long STARTGAME= 10000;
	
	/**
	 * Max palyers
	 */
	public static final int MAXPLAYERS=8;
	
	/**
	 * Min Players
	 */
	public static final int MINPLAYERS=1;
}
