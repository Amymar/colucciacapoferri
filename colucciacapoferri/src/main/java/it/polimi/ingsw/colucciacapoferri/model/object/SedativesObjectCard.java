package it.polimi.ingsw.colucciacapoferri.model.object;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.ObjectCardType;

import java.util.Map;

public class SedativesObjectCard implements InterfaceObjectCard{
	private String id;
	private static ObjectCardType type = ObjectCardType.SEDATIVES;
	
	public SedativesObjectCard(){
		
	}
	public SedativesObjectCard(String id){
		this.id=id;
	}

	public InterfaceObjectCard getThis(String id,Map<String, InterfaceObjectCard> map) {
		if(map.containsKey(id)){
			return map.get(id);
		}else{
			InterfaceObjectCard card = new SedativesObjectCard(id);
			map.put(id, card);
			return card;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id=id;
	}

	public ObjectCardType getType() {
		return type;
	}

	public void setType(ObjectCardType type) {
		SedativesObjectCard.type=type;
	}
}
