package model;

import it.polimi.ingsw.colucciacapoferri.controller.GamingData;
import it.polimi.ingsw.colucciacapoferri.model.character_card.InterfaceCharacterCard;
import it.polimi.ingsw.colucciacapoferri.model.sectorCard.InterfaceCardSector;


public class Prova {

	public static void main(String[] args) {
		GamingData data=new GamingData();
		for(InterfaceCharacterCard s: data.getHumans().values()){
			System.out.println(s.getName()+"--"+s.getPosition().getId());
		}
		for(InterfaceCharacterCard s: data.getAliens().values()){
			System.out.println(s.getName()+"--"+s.getPosition().getId());
		}
		for (InterfaceCardSector s: data.getSectorCards()){
			System.out.println(s.getId()+"---"+s.getType().toString()+"---"+s.isObject());
		}
		
//		Map <String, InterfaceCardSector> prova = new TreeMap <String, InterfaceCardSector>();
//		InterfaceCardSector inter;
//		Map<String, InterfaceObjectCard> ob= new TreeMap<String, InterfaceObjectCard>();
//		InterfaceObjectCard card;
//		for(int  i=0; i<(Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE);i++){
//			inter=new FakeEmptySectorCard();
//			inter.getThis("FT"+i, prova);
//			inter= new FakeFraughtSectorCard();
//			inter.getThis("FF"+i, prova);
//			inter=new SilenceSectorCard();
//			inter.getThis("S"+i, prova);
//			inter=new RealFraughtSectorCard();
//			inter.getThis("RT"+i, prova);
//			inter=new RealEmptySectorCard();
//			inter.getThis("RF"+i, prova);
//		}
//		for (int j=0; j<(Constants.NOBJECTCARDS/Enumerations.ObjectCardType.values().length);j++){
//			card=new AttackObjectCard();
//			card.getThis(Enumerations.ObjectCardType.ATTACK.toString()+j, ob);
//			card= new TeleportObjectCard();
//			card.getThis(Enumerations.ObjectCardType.TELEPORT.toString()+j, ob);
//			card=new SedativesObjectCard();
//			card.getThis(Enumerations.ObjectCardType.SEDATIVES.toString()+j, ob);
//			card=new LightObjectCard();
//			card.getThis(Enumerations.ObjectCardType.LIGHT.toString()+j, ob);
//			card=new DefenceObjectCard();
//			card.getThis(Enumerations.ObjectCardType.DEFENCE.toString()+j, ob);
//			card=new AdrenalineObjectCard();
//			card.getThis(Enumerations.ObjectCardType.ADRENALINE.toString()+j, ob);
//		}
//		for (String s: prova.keySet()){
//			System.out.println(prova.get(s).getId()+"---"+prova.get(s).getType().toString()+"---"+prova.get(s).isObject());
//		}
//		for (String s: ob.keySet()){
//			System.out.println(ob.get(s).getId()+"---"+ob.get(s).getType().toString());
//		}
//		

	}
}


