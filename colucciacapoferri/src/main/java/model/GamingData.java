package model;

import it.polimi.ingsw.colucciacapoferri.controller.Constants;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations;
import it.polimi.ingsw.colucciacapoferri.controller.Game;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;
import it.polimi.ingsw.colucciacapoferri.model.GameBoard;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

/**
 * 
 * @author Marilena Coluccia
 * 
 */

public class GamingData {

	private GameBoard gameBoard;
	private Game game;
	private List <CardSector> cardSectors= new  LinkedList<CardSector>();
	private List<ObjectCard> objectCards= new LinkedList <ObjectCard>();
	private List <BoatCard> boatCards= new LinkedList <BoatCard> ();
	private Map <String, CharacterCard> humans=new TreeMap <String, CharacterCard>();
	private Map <String, CharacterCard> aliens=new TreeMap <String, CharacterCard>();

	public GamingData(){
		this.gameBoard=new GameBoard();
		creatingSectorCards();
		creatingObjectCards();
		creatingBoatCards();
		creatingPgCards();
	}
	
	public GamingData(Game game){

		//inizzializzo mappa
		this.gameBoard=new GameBoard();
		this.game=game;

		creatingSectorCards();
		creatingObjectCards();
		creatingBoatCards();
		creatingPgCards();
	}

		//inizzializzo Carte settore
	public void creatingSectorCards(){
		Map <String,CardSector> cards= new TreeMap <String,CardSector>();
		CardSector cardSector=null;
		for(Integer j=0;j<Constants.NSECTORCARDS;j++){
			if (j<(Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE)){
				cardSector=new CardSector(j.toString(), SectorCardType.REAL, true);
			}else if(j>=(Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE) && j<((Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE)*2)){
				cardSector=new CardSector(j.toString(), SectorCardType.REAL, false);
			}else if(j>=((Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE)*2) && j<((Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE)*3)){
				cardSector=new CardSector(j.toString(), SectorCardType.FAKE, false);
			}else if(j>=((Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE)*3) && j<((Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE)*4)){
				cardSector=new CardSector(j.toString(), SectorCardType.FAKE, true);
			}else if(j>=((Constants.NSECTORCARDS/Constants.NSECTORCARDSTYPE)*4) && j<Constants.NSECTORCARDS){
				cardSector=new CardSector(j.toString(), SectorCardType.SILENCE, false);
			}
			cards.put(j.toString(), cardSector);
		}
		
		Random ran =new Random();
		List<String> keySet =new ArrayList<String>(cards.keySet());
		String randomKey;
		while (!keySet.isEmpty()){
			randomKey= keySet.get(ran.nextInt(keySet.size()));
			getSectorCards().add(cards.get(randomKey));
			cards.remove(randomKey);
			int i=keySet.indexOf(randomKey);
			keySet.remove(i);
		}
		
	}


		//inizzializzo carte oggetto
	private void creatingObjectCards(){
		Map<String, ObjectCard> card= new TreeMap<String, ObjectCard>();
		ObjectCard objectCard=null;
		for(Integer i=0;i<Constants.NOBJECTCARDS;i++){
			if (i<(Constants.NOBJECTCARDS/Constants.NOBJECTTYPE)){
				objectCard=new ObjectCard(i, ObjectCardType.ATTACK);
			}else if(i>=(Constants.NOBJECTCARDS/Constants.NOBJECTTYPE) && i<((Constants.NOBJECTCARDS/Constants.NOBJECTTYPE)*2)){
				objectCard=new ObjectCard(i, ObjectCardType.TELEPORT);
			}else if(i>=((Constants.NOBJECTCARDS/Constants.NOBJECTTYPE)*2) && i<((Constants.NOBJECTCARDS/Constants.NOBJECTTYPE)*3)){
				objectCard=new ObjectCard(i, ObjectCardType.ADRENALINE);
			}else if(i>=((Constants.NOBJECTCARDS/Constants.NOBJECTTYPE)*3) && i<((Constants.NOBJECTCARDS/Constants.NOBJECTTYPE)*4)){
				objectCard=new ObjectCard(i, ObjectCardType.SEDATIVES);
			}else if(i>=((Constants.NOBJECTCARDS/Constants.NOBJECTTYPE)*4) && i<((Constants.NOBJECTCARDS/Constants.NOBJECTTYPE)*5)){
				objectCard=new ObjectCard(i, ObjectCardType.LIGHT);
			}else if(i>=((Constants.NOBJECTCARDS/Constants.NOBJECTTYPE)*5) && i<Constants.NOBJECTCARDS){
				objectCard=new ObjectCard(i, ObjectCardType.DEFENCE);
			}
			card.put(i.toString(), objectCard);
		}
		Random ran =new Random();
		List<String> keySet =new ArrayList<String>(card.keySet());
		String randomKey;
		while (keySet.size()!=0){
			randomKey= keySet.get(ran.nextInt(keySet.size()));
			getObjectCards().add(card.get(randomKey));
			card.remove(randomKey);
			int i=keySet.indexOf(randomKey);
			keySet.remove(i);
		}
	}



		//inizzializzo carte scialuppa
		//assicurandomi che almeno esista la possibilità
		//che in caso di 8 giocatori tutti e quattro gli umani possano scappare
	private void creatingBoatCards(){
		Map<String, BoatCard> card= new TreeMap <String, BoatCard>();
		BoatCard boat=null;
		for(Integer w=0; w<Constants.NBOATCARDS;w++){
			if(w<Constants.NBOATCARDS-Constants.NBOATS){
				boat=new BoatCard(w.toString(),Color.RED);
			}else{
				boat=new BoatCard(w.toString(),Color.GREEN);
			}
			card.put(w.toString(), boat);
		}
		Random ran =new Random();
		List<String> keySet =new ArrayList<String>(card.keySet());
		String randomKey;
		while (keySet.size()!=0){
			randomKey= keySet.get(ran.nextInt(keySet.size()));
			getBoatCards().add(card.get(randomKey));
			card.remove(randomKey);
			int i=keySet.indexOf(randomKey);
			keySet.remove(i);
		}
	}



		//inizzializzo carte personaggio
		// e le posiziono nelle rispettive basi
	private void creatingPgCards(){
		CharacterCard personaggio=null;
		for(Humans per : Enumerations.Humans.values()) {
			personaggio=new CharacterCard(per.toString(), Race.HUMAN,getGameBoard().getSectors().get("XU"));
			getHumans().put(per.toString(), personaggio);
		}
		for(Enumerations.Aliens al: Enumerations.Aliens.values()){
			personaggio=new CharacterCard(al.toString(), Race.ALIEN,getGameBoard().getSectors().get("XA"));
			getAliens().put(al.toString(), personaggio);
		}
	}
	public GameBoard getGameBoard() {
		return gameBoard;
	}
	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public List<CardSector> getSectorCards() {
		return cardSectors;
	}
	public void setSectorCards(List<CardSector> carteSettore) {
		this.cardSectors = carteSettore;
	}
	public List<ObjectCard> getObjectCards() {
		return objectCards;
	}
	public void setObjectCards(List<ObjectCard> carteOggetto) {
		this.objectCards = carteOggetto;
	}
	public List<BoatCard> getBoatCards() {
		return boatCards;
	}
	public void setBoatCards(List<BoatCard> carteScialuppa) {
		this.boatCards = carteScialuppa;
	}
	public Map<String, CharacterCard> getHumans() {
		return humans;
	}
	public void setHumans(Map<String, CharacterCard> umani) {
		this.humans = umani;
	}
	public Map<String, CharacterCard> getAliens() {
		return aliens;
	}
	public void setAliens(Map<String, CharacterCard> alieni) {
		this.aliens = alieni;
	}
}


