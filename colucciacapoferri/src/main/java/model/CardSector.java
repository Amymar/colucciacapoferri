package model;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;

/**
 * 
 * @author Marilena Coluccia
 * The class SectorCard
 */
public class CardSector {
	
	/**
	 * It's used to distinguish each card in the deck
	 */
	private String id;
	/**
	 * Sector card type
	 */
	private SectorCardType type;
	/**
	 * item presence
	 */
	private boolean object;
	
	/**
	 * Constructor
	 * @param id
	 * @param tipo
	 * @param oggetto
	 */
	public CardSector (String id, SectorCardType tipo, boolean oggetto){
		this.id=id;
		this.type=tipo;
		this.object=oggetto;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public SectorCardType getType() {
		return type;
	}

	public void setType(SectorCardType type) {
		this.type = type;
	}

	public boolean isObject() {
		return object;
	}

	public void setObject(boolean object) {
		this.object = object;
	}

	
}