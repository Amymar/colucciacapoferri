package model;

import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;

/**
 * 
 * @author Marilena
 * The item card class
 */
public class ObjectCard {

	private int id;
	private ObjectCardType type;
	
	/**
	 * Constructor 
	 * @param id
	 * @param tipo
	 */
	public ObjectCard (int id, ObjectCardType tipo){
		this.id=id;
		this.type=tipo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ObjectCardType getType() {
		return type;
	}

	public void setType(ObjectCardType type) {
		this.type = type;
	}

	
	
	
}
