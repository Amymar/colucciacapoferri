package model;

import it.polimi.ingsw.colucciacapoferri.controller.Round;
import it.polimi.ingsw.colucciacapoferri.controller.Enumerations.*;
import it.polimi.ingsw.colucciacapoferri.model.boxes.Box;
import it.polimi.ingsw.colucciacapoferri.model.boxes.BoxSingleton;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Marilena
 * player character card
 */
public class CharacterCard {
	/**
	 * distinguish each character
	 */
	private String name;
	/**
	 * character race (Alien or Human)
	 */
	private Race race;
	/**
	 * current position of the character on the game board 
	 */
	private BoxSingleton position;
	/**
	 * 
	 */
	List <Round> oldRounds;
	List<ObjectCard> objects;
	boolean eaten;
	
	/**
	 * Constructor
	 * @param nome
	 * @param race
	 * @param posizione
	 */
	public CharacterCard(String nome, Race race, BoxSingleton posizione){
		this.name=nome;
		this.race=race;
		this.position=posizione;
		this.oldRounds= new ArrayList <Round>();
		this.objects= new ArrayList <ObjectCard>();
		this.eaten=false;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Race getRace() {
		return race;
	}


	public void setRace(Race race) {
		this.race = race;
	}


	public BoxSingleton getPosition() {
		return position;
	}


	public void setPosition(Box position) {
		this.position = position;
	}


	public List<Round> getOldRounds() {
		return oldRounds;
	}


	public void setOldRounds(List<Round> oldRounds) {
		this.oldRounds = oldRounds;
	}


	public List<ObjectCard> getObjects() {
		return objects;
	}


	public void setObjects(List<ObjectCard> objects) {
		this.objects = objects;
	}


	public boolean isEaten() {
		return eaten;
	}


	public void setEaten(boolean eaten) {
		this.eaten = eaten;
	}
	
	
}
